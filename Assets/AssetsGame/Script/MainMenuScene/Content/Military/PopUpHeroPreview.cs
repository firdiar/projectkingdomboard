﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpHeroPreview : MonoBehaviour
{
    [SerializeField] Animation anim;
    [SerializeField] GameObject heroholder;
    [SerializeField] GameObject descHolder;

    [Header("UI")]
    [SerializeField] Image heroIdle;
    [SerializeField] Text heroName;
    [SerializeField] Text heroClass;
    [SerializeField] Text heroName2;
    [SerializeField] Text heroDesc;

    bool currentHero = true;
    bool isAnimating = false;

    public void animationFinish()
    {
        isAnimating = false;
        currentHero = !currentHero;
    }

    public void init(HeroesDataScriptableObject data) {
        heroIdle.sprite = data.idle;
        heroName.text = data.heroName;
        heroName2.text = data.heroName;
        heroDesc.text = data.desc;
        currentHero = true;
        isAnimating = false;
        switch (data.heroesClass) {
            case HeroesClass.Infantry:
                heroClass.text = "( Infantry )";
                break;
            case HeroesClass.Archer:
                heroClass.text = "( Archer )";
                break;
            case HeroesClass.Cavalary:
                heroClass.text = "( Cavalary )";
                break;
            case HeroesClass.Magician:
                heroClass.text = "( Magician )";
                break;
        }

        heroholder.SetActive(true);
        heroholder.transform.rotation = Quaternion.identity;
        descHolder.SetActive(false);

        this.gameObject.SetActive(true);
    }

    public void flipDesc()
    {
        if (!currentHero || isAnimating)
            return;

        anim.Play("FlipDesc");
        isAnimating = true;
    }

    public void flipHero() {
        if (currentHero || isAnimating)
            return;

        

        anim.Play("FlipPreview");
        isAnimating = true;
    }

    public void closePopUp() {

        this.gameObject.SetActive(false);
    }
}
