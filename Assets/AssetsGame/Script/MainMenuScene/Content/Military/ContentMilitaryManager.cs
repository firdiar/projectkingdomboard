﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentMilitaryManager : ContentHandler
{

    Dictionary<string  , HeroesDataScriptableObject> userHeroes = new Dictionary<string, HeroesDataScriptableObject>();

    [Header("Img Asset")]
    [SerializeField] Sprite ActiveTab = null;
    [SerializeField] Sprite inActiveTab = null;

    [Header("Tabs")]
    [SerializeField] GameObject tabFormations = null;
    [SerializeField] Image btnFormations = null;
    [SerializeField] FormationMilitaryHandler formationHandler = null;

    [SerializeField] GameObject tabDeputy = null;
    [SerializeField] Image btnDeputy = null;

    [SerializeField] GameObject tabHeroes = null;
    [SerializeField] Image btnHeroes = null;
    [SerializeField] HeroesMilitaryHandler heroesHandler = null;

    [SerializeField] GameObject tabTroops = null;
    [SerializeField] Image btnTroops = null;

    public void dummy() {

    }

    public void setActiveFormation() {
        tabFormations.SetActive(true);
        tabHeroes.SetActive(false);
        tabTroops.SetActive(false);
        tabDeputy.SetActive(false);

        btnFormations.sprite = ActiveTab;
        btnHeroes.sprite = inActiveTab;
        btnTroops.sprite = inActiveTab;
        btnDeputy.sprite = inActiveTab;

    }

    public void setActiveDeputy()
    {
        tabDeputy.SetActive(true);
        tabFormations.SetActive(false);
        tabHeroes.SetActive(false);
        tabTroops.SetActive(false);

        btnDeputy.sprite = ActiveTab;
        btnFormations.sprite = inActiveTab;
        btnHeroes.sprite = inActiveTab;
        btnTroops.sprite = inActiveTab;

    }

    public void setActiveHeroes()
    {
        tabFormations.SetActive(false);
        tabHeroes.SetActive(true);
        tabTroops.SetActive(false);
        tabDeputy.SetActive(false);

        btnFormations.sprite = inActiveTab;
        btnHeroes.sprite = ActiveTab;
        btnTroops.sprite = inActiveTab;
        btnDeputy.sprite = inActiveTab;
    }
    public void setActiveTroops()
    {
        tabFormations.SetActive(false);
        tabHeroes.SetActive(false);
        tabTroops.SetActive(true);
        tabDeputy.SetActive(false);

        btnFormations.sprite = inActiveTab;
        btnHeroes.sprite = inActiveTab;
        btnTroops.sprite = ActiveTab;
        btnDeputy.sprite = inActiveTab;
    }

    public override void Start()
    {
        base.Start();
        reCallStart();
    }

    public override void reCallStart() {
        getAllUserHeroes();
        //Debug.Log("Re Called");
    }

    void getAllUserHeroes()
    {
        userHeroes.Clear();
        foreach (HeroesData h in Account.CurrentAccountData.heroes.Archer)
        {
            HeroesDataScriptableObject hdso = RoyaleHeroesLibs.getHeroesData(h.name);
            if (hdso != null)
            {
                userHeroes.Add(hdso.heroName, hdso);
                //Debug.Log(hdso.heroName);
            }
        }
        foreach (HeroesData h in Account.CurrentAccountData.heroes.Infantry)
        {
            HeroesDataScriptableObject hdso = RoyaleHeroesLibs.getHeroesData(h.name);
            if (hdso != null)
            {
                userHeroes.Add(hdso.heroName, hdso);
                //Debug.Log(hdso.heroName);
            }
        }
        foreach (HeroesData h in Account.CurrentAccountData.heroes.Magician)
        {
            HeroesDataScriptableObject hdso = RoyaleHeroesLibs.getHeroesData(h.name);
            if (hdso != null)
            {
                userHeroes.Add(hdso.heroName, hdso);
                //Debug.Log(hdso.heroName);
            }
        }
        foreach (HeroesData h in Account.CurrentAccountData.heroes.Cavalary)
        {
            HeroesDataScriptableObject hdso = RoyaleHeroesLibs.getHeroesData(h.name);
            if (hdso != null)
            {
                userHeroes.Add(hdso.heroName, hdso);
                //Debug.Log(hdso.heroName);
            }
        }

        formationHandler.initFormationTab(userHeroes);
        heroesHandler.initHeroesTab(userHeroes);
    }
}
