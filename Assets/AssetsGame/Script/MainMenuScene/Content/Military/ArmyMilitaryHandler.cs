﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmyMilitaryHandler : MonoBehaviour
{
    //[Header("Data")]
    ArmyDataHolderScriptableObject dataArmy;

    ArmyDataScriptableObject[] currentArmy;
    int currentType = 0;

    [Header("UI")]
    [SerializeField]ScorllViewSnap scrollView=null;
    [SerializeField] Text armyTypeName = null;
    [Header("UI - Status")]
    [SerializeField] Text hp = null;
    [SerializeField] Text phyAtk = null;
    [SerializeField] Text phyDef = null;
    [SerializeField] Text magAtk = null;
    [SerializeField] Text magDef = null;
    [SerializeField] Text marchSpeed = null;
    [SerializeField] Text atkSpeed = null;
    [SerializeField] Text cntrRate = null;
    [SerializeField] Text range = null;
    [Header("UI - Requirement")]
    [SerializeField] Text food = null;
    [SerializeField] Text wood = null;
    [SerializeField] Text stone = null;
    [Header("UI - Desc")]
    [SerializeField] Text description = null;
    // Start is called before the first frame update
    void Start()
    {
        dataArmy = Resources.Load<ArmyDataHolderScriptableObject>("ArmyData/ArmyDataHolder");
        scrollView.onSelectedNewItemDelegate = newItem;
        
    }

    

    void newItem(int idx) {
        //Debug.Log(idx);
        switch (idx) {
            case 0:
                currentArmy = dataArmy.infantry;
                break;
            case 1:
                currentArmy = dataArmy.archer;
                break;
            case 2:
                currentArmy = dataArmy.cavalary;
                break;
            case 3:
                currentArmy = dataArmy.magician;
                break;

        }
        currentType = 0;

        showArmyData(currentArmy[currentType]);
    }

    public void showArmyData(ArmyDataScriptableObject data) {
        armyTypeName.text = data.armyName;

        hp.text = data.status.health.ToString();
        phyAtk.text = data.status.phyAttack.ToString();
        phyDef.text = data.status.phyDeffense.ToString();
        magAtk.text = data.status.magAttack.ToString();
        magDef.text = data.status.magDeffense.ToString();
        marchSpeed.text = data.status.movementSpeed.ToString();
        atkSpeed.text = data.status.attackSpeed.ToString() + " /s";
        cntrRate.text = data.status.counterAttackRate.ToString() + "%";
        range.text = data.status.range.ToString();

        food.text = data.requirement.food.ToString();
        wood.text = data.requirement.wood.ToString();
        stone.text = data.requirement.stone.ToString();

        description.text = data.description;
    }

    public void nextTypeArmy() {
        currentType = (currentType + 1)%currentArmy.Length;
        showArmyData(currentArmy[currentType]);
    }
    public void prevTypeArmy() {
        currentType = RoyaleHeroesLibs.mod( (currentType - 1) , currentArmy.Length );
        
        showArmyData(currentArmy[currentType]);
    }
}
