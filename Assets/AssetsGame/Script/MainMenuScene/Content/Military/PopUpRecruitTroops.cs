﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpRecruitTroops : MonoBehaviour
{
    //[Header("Data")]
    ArmyDataHolderScriptableObject armyData = null;
    HeroesDataScriptableObject heroData;
    int heroLevel = 1;
    int maxTroop = 0;
    int maxCost = 0;
    RoyaleHeroesLibs.Wrapper<int> actualCost = new RoyaleHeroesLibs.Wrapper<int>(0);
    RoyaleHeroesLibs.Wrapper<int> valueOnGroup = new RoyaleHeroesLibs.Wrapper<int>(0);

    Vector2 position = Vector2.zero;

    List<UnitRecruit> units = new List<UnitRecruit>();


    bool _isInStatus = false;
    bool isInStatus {
        get {
            return _isInStatus;
        }
        set {
            _isInStatus = value;
            stateName.text = (!_isInStatus ? "Status" : "Recruit");
            status.SetActive(_isInStatus);
            recruit.SetActive(!_isInStatus);
            if (isInStatus) {
                updateStatusAtribute();
            }
        }
    }

    
    public delegate void OnFinishRecruitArmyDelegate(bool isConfirmed , BlockData data , Vector2 position);
    public OnFinishRecruitArmyDelegate onFinishRecruitArmy;

    

    Coroutine moveCoroutine;
    bool _isInStatusAtr = true;
    bool isInStatusAtr
    {
        get
        {
            return _isInStatusAtr;
        }
        set
        {
            if (!isMoveCoroutineFinish) {
                return;
            }
            _isInStatusAtr = value;

            //setAnchorAndPivotStatus(_isInStatusAtr);
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }
            isMoveCoroutineFinish = false;

            statusSwipeLeftBtn.interactable = !_isInStatusAtr;
            statusSwipeRightBtn.interactable = _isInStatusAtr;

            moveCoroutine = StartCoroutine(moveContentStatus());
        }
    }

    bool isMoveCoroutineFinish = true;

    Vector2 pivotLeft = new Vector2(0, 0.5f);
    Vector2 pivotRight = new Vector2(1, 0.5f);

    [Header("Prefabs")]
    [SerializeField] UnitRecruit armyRecruitSliderPrefabs = null;

    [Header("UI")]
    [SerializeField] Image heroBorder = null;
    [SerializeField] Image heroIcon = null;
    [SerializeField] Text heroName = null;
    [SerializeField] Text heroLevelAndClass = null;
    [SerializeField] Image maximumArmy = null;

    [SerializeField] Text groupName = null;
    [SerializeField] Text stateName = null;
    [SerializeField] Text totalTroop = null;
    [SerializeField] Text totalResource = null;
    [SerializeField] Text totalCost = null;
    [SerializeField] Text availableCostText = null;

    [SerializeField] RectTransform tittleSwapStatus = null;
    [SerializeField] RectTransform contentSwapStatus = null;
    [SerializeField] Button statusSwipeRightBtn = null;
    [SerializeField] Button statusSwipeLeftBtn = null;

    [Header("UI - Status")]
    [SerializeField] Text hp = null;
    [SerializeField] Text phyAtk = null;
    [SerializeField] Text phyDef = null;
    [SerializeField] Text magAtk = null;
    [SerializeField] Text magDef = null;
    [SerializeField] Text marchSpeed = null;
    [SerializeField] Text atkSpeed = null;
    [SerializeField] Text cntrRate = null;
    [SerializeField] Text range = null;

    [SerializeField] Text food = null;
    [SerializeField] Text wood = null;
    [SerializeField] Text stone = null;

    [Header("Holder")]
    [SerializeField] Transform recruitSliderHolder = null;

    [Header("Tab")]
    [SerializeField] GameObject recruit = null;
    [SerializeField] GameObject status = null;

    void updateStatusAtribute() {
        CharacterStatus stats = new CharacterStatus();
        int totalUnit = 0;

        ResourcesMaterial material = new ResourcesMaterial();
        foreach (UnitRecruit unit in units)
        {
            stats += unit.getArmyStatus();
            totalUnit += unit.getCountUnit();

            material.food += unit.getResourceCost(UnitRecruit.ResourceClass.Food);
            material.wood += unit.getResourceCost(UnitRecruit.ResourceClass.Wood);
            material.stone += unit.getResourceCost(UnitRecruit.ResourceClass.Stone);
        }

        stats += (heroData.status * 2000); //Mathf.Max(Mathf.CeilToInt(totalUnit * 0.05f), 1) );

        stats /= (totalUnit+2000)* 1f;
        Debug.Log(material.food);

        hp.text = RoyaleHeroesLibs.intToStringK(stats.health);
        phyAtk.text = RoyaleHeroesLibs.intToStringK(stats.phyAttack);
        phyDef.text = RoyaleHeroesLibs.intToStringK(stats.phyDeffense);
        magAtk.text = RoyaleHeroesLibs.intToStringK(stats.magAttack);
        magDef.text = RoyaleHeroesLibs.intToStringK(stats.magDeffense);
        marchSpeed.text = stats.movementSpeed.ToString();
        atkSpeed.text = ( Mathf.Floor(stats.attackSpeed*100)/100 ).ToString();
        cntrRate.text = (Mathf.Floor(stats.counterAttackRate * 100)).ToString()+"%";
        range.text = stats.range.ToString();


        food.text = RoyaleHeroesLibs.intToStringK(material.food);
        wood.text = RoyaleHeroesLibs.intToStringK(material.wood);
        stone.text = RoyaleHeroesLibs.intToStringK(material.stone);


    }
    

    IEnumerator moveContentStatus() {
        float progress = 0;
        
        Vector2 targetTitle = tittleSwapStatus.localPosition;
        targetTitle.x = -targetTitle.x;


        Vector2 targetContent = contentSwapStatus.localPosition;
        targetContent.x = -targetContent.x;


        while (progress < 1) {
            progress = Mathf.Min( 0.08f + progress , 1);

            tittleSwapStatus.localPosition = Vector2.Lerp(tittleSwapStatus.localPosition, targetTitle, progress);
            contentSwapStatus.localPosition = Vector2.Lerp(contentSwapStatus.localPosition, targetContent, progress);

            yield return new WaitForSeconds(0.02f);
        }

        isMoveCoroutineFinish = true;
        yield return null;
    }



    private void Start()
    {
        //InitPopUp(RoyaleHeroesLibs.getHeroesData("Emiya Shiro"), 4);
        
    }

    public void EndEdit(bool isConfirmed)
    {
        if (valueOnGroup < 100 && isConfirmed) {
            RoyaleHeroesToast.ShowToast("Minimum army is 100",2);
            return;
        }

        BlockData b = new BlockData();
        b.hero = heroData.name;
        b.heroClass = heroData.heroesClass;

        int total = 0;
        foreach (UnitRecruit unit in units)
        {
            int c = unit.getCountUnit();
            if ( c > 0) {
                b.army.Add(unit.getNameUnit(), c);
                total += c;
            }
        }

        //Account.CurrentAccountData.getHeroes(heroData.name , )
        //maxTroop
        if (total > maxTroop) {
            RoyaleHeroesToast.ShowToast("Total troop is over capacity", 2);
            return;
        }

        gameObject.SetActive(false);
        onFinishRecruitArmy(isConfirmed, b , position);
    }

    public void InitPopUp(HeroesDataScriptableObject heroData , int level , Vector2 position , int leftCost , BlockData data = null)
    {
        if(armyData == null)
            armyData = Resources.Load<ArmyDataHolderScriptableObject>("ArmyData/ArmyDataHolder");

        maxCost = leftCost;
        actualCost.Value = 0;
        this.heroData = heroData;
        heroLevel = level;
        maxTroop = heroData.status.maxTroops(level);
        valueOnGroup.Value = 0;
        this.position = position;

        heroIcon.sprite = heroData.icon;
        heroBorder.sprite = RoyaleHeroesLibs.getLevelBorder(level);

        clearUnits();
        switch (heroData.heroesClass) {
            case HeroesClass.Infantry:
                initSlider(armyData.infantry , data);
                break;
            case HeroesClass.Archer:
                initSlider(armyData.archer, data);
                break;
            case HeroesClass.Cavalary:
                initSlider(armyData.cavalary, data);
                break;
            case HeroesClass.Magician:
                initSlider(armyData.magician, data);
                break;
        }

        initHeroText();

        isInStatus = false;
        //updateStateTab();

    }

    void initHeroText() {
        heroName.text = heroData.heroName;
        string classHerostr = "";
        switch (heroData.heroesClass)
        {
            case HeroesClass.Infantry:
                classHerostr = "Infantry";
                break;
            case HeroesClass.Archer:
                classHerostr = "Archer";
                break;
            case HeroesClass.Cavalary:
                classHerostr = "Cavalary";
                break;
            case HeroesClass.Magician:
                classHerostr = "Magician";
                break;
        }
        heroLevelAndClass.text = "Lv " + heroLevel.ToString() + " ( "+classHerostr+" )";

        

        updateValueOnGroup();
        



    }
    
    public void changeState() {
        isInStatus = !isInStatus;
        //updateStateTab();
    }

    public void changeStateStatus()
    {
        isInStatusAtr = !isInStatusAtr;
        //updateStateTab();
    }


    void updateValueOnGroup()
    {

        int totalUnit = 0;
        int totalResource = 0;
        foreach (UnitRecruit unit in units) {
            totalUnit += unit.getCountUnit();
            totalResource += unit.getResourceCost();
            //Debug.Log(unit.getNameUnit()+" - "+unit.getResourceCost() + " - " + totalResource);
        }
        
        valueOnGroup.Value = totalUnit;
        actualCost.Value = totalUnit;
        //Debug.Log(valueOnGroup.Value+" - "+total);

        float limit = Mathf.InverseLerp(0, maxTroop, (maxTroop - valueOnGroup.Value));
        maximumArmy.fillAmount = limit;

        totalTroop.text = "Troop "+ RoyaleHeroesLibs.intToStringK( totalUnit);
        this.totalResource.text = "Res "+ RoyaleHeroesLibs.intToStringK(totalResource);
        
        this.totalCost.text = "Total Cost : "+ ( actualCost ).ToString();
        this.availableCostText.text = "Available Cost : " + (maxCost - actualCost.Value);

        groupName.text = heroData.nick + "'s " + RoyaleHeroesLibs.getGroupName(valueOnGroup.Value);

        groupName.GetComponentInParent<HorizontalLayoutGroup>().spacing += 0.02f;
    }

    void clearUnits() {
        //units.Clear();
        foreach (UnitRecruit unit in units) {
            Destroy(unit.gameObject);
        }
        units.Clear();
    }

    void initSlider(ArmyDataScriptableObject[] armyData , BlockData dataBlock) {
        
        foreach (ArmyDataScriptableObject data in armyData) {
            UnitRecruit temp = Instantiate(armyRecruitSliderPrefabs, recruitSliderHolder);
            if (dataBlock != null && dataBlock.army.ContainsKey(data.armyName)) {
                temp.initUnit(data, maxTroop, maxCost, valueOnGroup, actualCost, updateValueOnGroup , dataBlock.army[data.armyName] );
            }
            else {
                temp.initUnit(data, maxTroop, maxCost, valueOnGroup, actualCost, updateValueOnGroup);
            }
            units.Add(temp);
        }
    }





    
}
