﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeputyListItem : MonoBehaviour
{

    public DeputyMilitaryHandler parent = null;
    public DeputyHero data = null;

    //string _heroName = "";
    public string heroName {
        get { return nameOfHero.text; }
        set { nameOfHero.text = value; }
    }
    public string heroesClass
    {
        set { classOfHero.text =  "Hero Class				: " + value; }
    }
    public string heroesBattle
    {
        set { battleOfHero.text = "Battle as Deputy			: " + value; }
    }
    [SerializeField] Text nameOfHero = null;
    [SerializeField] Text classOfHero = null;
    [SerializeField] Text battleOfHero = null;
    [SerializeField] Image iconOfHero = null;

    [SerializeField] Color blue;
    [SerializeField] Color red;

    [SerializeField] GameObject setCurrentDeputyBtn;
    [SerializeField] GameObject unsetCurrentDeputyBtn;

    [SerializeField] GameObject promoted = null;
    [SerializeField] GameObject unpromoted = null;


    public void setIcon(Sprite icon) {
        iconOfHero.sprite = icon;
    }

    public void setPromotedStart()
    {
        promoted.SetActive(true);
        unpromoted.SetActive(false);
    }

    public void setPromoted() {

        MainMenuManager.showPopUpMessage("Confirmation", "Are you sure to promote "+heroName+" to Deputy ?\nPromotion cost is 30.000 Gold", true, (bool res) => {
            if (res)
            {
                //buyItems(data);
                if (Account.CurrentAccountData.resources.gold >= 30000)
                {

                    Account.addResource(-30000, ResourcesMaterialType.Gold);
                    MainMenuManager.mainmenu.UpdateHeader();
                    setPromotedStart();
                    parent.saveNewBrain(this, heroName);

                    RoyaleHeroesToast.ShowToast("Promotion success", 2);
                }
                else {
                    RoyaleHeroesToast.ShowToast("Not enough gold", 2);
                }

                
            }
            else
            {
                RoyaleHeroesToast.ShowToast("Promotion canceled", 2);
            }
        });

        
    }

    public void setCurrentDeputyPure() {
        setCurrentDeputyBtn.SetActive(false);
        unsetCurrentDeputyBtn.SetActive(true);
    }

    public void setCurrentDeputy()
    {
        

        MainMenuManager.showPopUpMessage("Confirmation", "Are you sure to set " + heroName + " as Deputy ?\nPreparation cost is 5.000 Gold", true, (bool res) => {
            if (res)
            {
                //buyItems(data);
                if (Account.CurrentAccountData.resources.gold >= 5000)
                {

                    Account.addResource(-5000, ResourcesMaterialType.Gold);
                    MainMenuManager.mainmenu.UpdateHeader();
                    setCurrentDeputyBtn.SetActive(false);
                    unsetCurrentDeputyBtn.SetActive(true);
                    parent.setDeputyHero(this);

                    RoyaleHeroesToast.ShowToast("New Deputy Commander!", 2);
                }
                else
                {
                    RoyaleHeroesToast.ShowToast("Not enough gold", 2);
                }


            }
            else
            {
                RoyaleHeroesToast.ShowToast("New Deputy canceled", 2);
            }
        });

    }

    public void unsetCurrentDeputyPure()
    {
        setCurrentDeputyBtn.SetActive(true);
        unsetCurrentDeputyBtn.SetActive(false);
    }

    public void unsetCurrentDeputy()
    {
        

        MainMenuManager.showPopUpMessage("Confirmation", "Are you sure to unset Deputy " + heroName + " ?", true, (bool res) => {
            if (res)
            {
                setCurrentDeputyBtn.SetActive(true);
                unsetCurrentDeputyBtn.SetActive(false);
                parent.unsetDeputyHero(this);

                RoyaleHeroesToast.ShowToast("Deputy Demoted", 2);

            }
        });
    }

}
