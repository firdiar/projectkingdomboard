﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeputyMilitaryHandler : MonoBehaviour
{

    List<DeputyHero> heroes = null;

    List<DeputyListItem> listItem = new List<DeputyListItem>();

    [SerializeField]DeputyListItem prefabsItem = null;
    [SerializeField] Transform contentHolder = null;

    public void Start() {
        loadDeputy();

        loadItem();

        InitTab();

    }

    void InitTab()
    {
        foreach (DeputyHero dh in heroes)
        {
            DeputyListItem dli = getHeroInList(dh.heroName);
            dli.data = dh;
            dli.setPromotedStart();
            if (dh.isCurrentDeputy)
            {
                dli.transform.SetAsFirstSibling();
                dli.setCurrentDeputyPure();
            }

        }
    }



    public DeputyHero getCurrentDeputy() {
        foreach (DeputyHero d in heroes) {
            if (d.isCurrentDeputy) {
                return d;
            }
        }
            
        return null;
    }

    public DeputyListItem getCurrentDeputyList()
    {
        foreach (DeputyListItem dli in listItem)
        {
            if (dli.data != null && dli.data.isCurrentDeputy)
            {
                return dli;
            }
        }

        return null;
    }

    public void setDeputyHero(DeputyListItem dli)
    {
        DeputyListItem currentDeputy = getCurrentDeputyList();
        if (currentDeputy != null) {
            currentDeputy.unsetCurrentDeputyPure();
            currentDeputy.data.isCurrentDeputy = false;
        }

        dli.data.isCurrentDeputy = true;
        dli.transform.SetAsFirstSibling();


        saveDeputy();
    }

    public void unsetDeputyHero(DeputyListItem dli)
    {
        dli.data.isCurrentDeputy = false;

        saveDeputy();
    }

    DeputyHero getHeroInList2(string name)
    {
        if (name == "")
        {
            return null;
        }

        foreach (DeputyHero dli in heroes)
        {
            if (dli.heroName == name)
            {
                return dli;

            }
        }
        return null;
    }

    DeputyListItem getHeroInList(string name) {
        if (name == "")
        {
            return null;
        }

        foreach (DeputyListItem dli in listItem)
        {
            if (dli.heroName == name)
            {
                return dli;
               
            }
        }
        return null;
    }

    public void setHeroFirstSibling(DeputyListItem dli) {
        if(dli != null)
        {
            dli.transform.SetAsFirstSibling();
        }
    }

    void saveDeputy()
    {
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(heroes);

        string path = "deputyData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        RoyaleHeroesLibs.saveFileToName(path, json);
    }

    void loadItem() {
        foreach (HeroesData hd in Account.CurrentAccountData.heroes.Infantry) {
            CreateItem(hd , "Infantry");
        }
        foreach (HeroesData hd in Account.CurrentAccountData.heroes.Archer)
        {
            CreateItem(hd, "Archer");
        }
        foreach (HeroesData hd in Account.CurrentAccountData.heroes.Cavalary)
        {
            CreateItem(hd, "Cavalary");
        }
        foreach (HeroesData hd in Account.CurrentAccountData.heroes.Magician)
        {
            CreateItem(hd, "Magician");
        }

    }

    void CreateItem(HeroesData hd ,  string heroClass) {

        DeputyListItem item = Instantiate<DeputyListItem>(prefabsItem, contentHolder);
        item.heroName = hd.name;
        item.heroesClass = heroClass;
        HeroesDataScriptableObject hdso = RoyaleHeroesLibs.getHeroesData(hd.name);
        item.setIcon( hdso.icon );
        item.parent = this;
        listItem.Add(item);

    }

    void loadDeputy()
    {

        string path = "deputyData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        string json = RoyaleHeroesLibs.loadFileFromName(path);
        if (json != null)
        {
            heroes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DeputyHero>>(json);
        }
        else {
            heroes = new List<DeputyHero>();
        }

    }

    public void saveNewBrain(DeputyListItem dli , string heroName) //promote new deputy
    {
        DeputyHero dhero = new DeputyHero(heroName);

        heroes.Add(dhero);

        dli.data = dhero;

        saveDeputy();

        /*
        RoyaleHeroesReinforcementLearningModel model = new RoyaleHeroesReinforcementLearningModel();

        string json = Newtonsoft.Json.JsonConvert.SerializeObject(model);

        string path = "brainData-"+heroName+"-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        RoyaleHeroesLibs.saveFileToName(path, json);
        */
    }
}
/*
public class RoyaleHeroesReinforcementLearningModel
{
    public int trainCount = 0;
    //public float[,] weights = null;
}
*/

public class DeputyHero {
    public bool isCurrentDeputy = false;
    public string heroName = "";
    public int battleCount = 0;

    public DeputyHero(string heroName) {
        this.heroName = heroName;
    }
}
