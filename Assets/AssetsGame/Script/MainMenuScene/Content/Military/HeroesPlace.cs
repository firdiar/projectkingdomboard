﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HeroesPlace : MonoBehaviour
{

    public string heroName;

    string _position;
    public string position {
        get {
            return _position;
        }
        set {
            _position = value;
            positionEmpty.text = _position;
        }
    }

    public Vector2 coordinate = Vector2.zero;
    
    Sprite _icon;
    public Sprite icon
    {
        get
        {
            return _icon;
        }
        set
        {
            _icon = value;
            iconHeroes.sprite = _icon;

            activeTab.SetActive(_icon != null);
            inActiveTab.SetActive(_icon == null);
        }
    }

    Sprite _border;
    public Sprite border
    {
        get
        {
            return _border;
        }
        set
        {
            _border = value;
            borderHeroes.sprite = _border;
        }
    }

    public delegate void OnPickedDelegate(HeroesPlace item);
    public OnPickedDelegate onPicked;
    
    public OnPickedDelegate onHold;
    public OnPickedDelegate onClick;

    public void runEvent()
    {
        onPicked(this);
    }

    public void inactivateButtonPut() {
        buttonPut.gameObject.SetActive(false);
    }
    public void activateButtonPut()
    {
        if (icon == null)
        {
            buttonPut.gameObject.SetActive(true);
        }
    }

    //Dictionary<string, int> army;

    [SerializeField] Image borderHeroes = null;
    [SerializeField] Image iconHeroes = null;
    [SerializeField] Text positionEmpty = null;
    [SerializeField] Button buttonPut = null;

    [SerializeField] GameObject activeTab = null;
    [SerializeField] GameObject inActiveTab = null;

}
