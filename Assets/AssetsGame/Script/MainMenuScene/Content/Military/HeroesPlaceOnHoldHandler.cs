﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroesPlaceOnHoldHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    [SerializeField] HeroesPlace parent;

    float timeCounter = 0;
    bool stillHold = false;
    bool clicked = false;
    Vector2 position = Vector2.zero;
    public void OnPointerDown(PointerEventData eventData)
    {
        timeCounter = 0;
        stillHold = true;
        clicked = true;
        position = eventData.position;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        stillHold = false;
        //clicked = false;
        if (clicked && timeCounter < 0.7f && eventData.position == position) {
            parent.onClick(parent);
        }
        clicked = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //stillHold = false;
        //clicked = false;
    }


    void Update()
    {
        if (stillHold)
        {
            if (timeCounter < 1f)
            {
                timeCounter += Time.deltaTime;
            }
            else
            {
                stillHold = false;
                //Handheld.Vibrate();
                Vibration.Vibrate(200);
                parent.onHold(parent);
                
            }
        }
    }
}
