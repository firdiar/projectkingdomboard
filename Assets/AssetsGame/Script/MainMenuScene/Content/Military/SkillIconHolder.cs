﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillIconHolder : MonoBehaviour
{
    [SerializeField]UnityEngine.UI.Image icon;

    HeroesSkill _data;
    public HeroesSkill data {
        get {
            return _data;
        }
        set {
            _data = value;
            icon.sprite = value.icon;
            
        }
    }
    public float scaleAddFactor;
    public Transform center;
    public float maxDist;

    public float updateScale() {
        float dist = Vector2.Distance(transform.position, center.position);
        
        float lerpValue =Mathf.InverseLerp(maxDist , 0 , Mathf.Min(dist, maxDist) );
        transform.localScale = Vector3.one * (1 + Mathf.Lerp(0, scaleAddFactor, lerpValue));

        return dist;
    }
}
