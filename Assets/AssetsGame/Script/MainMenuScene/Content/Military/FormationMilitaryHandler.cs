﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class FormationMilitaryHandler : MonoBehaviour
{

    int heroLimit = 0;
    int heroUsed = 0;
    int costMax = 0;
    int costUsed = 0;

    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    BlockData[,] lineUpform = new BlockData[13, 3];

    Dictionary<string, HeroesDataScriptableObject> userHeroes;
    List<string> heroesFree = new List<string>();

    [Header("Data")]
    [SerializeField] List<int> costLimitPerChapter = new List<int> ();
    [SerializeField] List<int> heroLimitPerChapter = new List<int>();

    [Header("Prefabs")]
    [SerializeField] HeroesPlace heroesPlace = null;
    [SerializeField] HeroesListItem heroesListItem = null;

    [Header("Holder")]
    [SerializeField] Transform contentLineUp = null;
    [SerializeField] Transform contentHeroesInfantry = null;
    [SerializeField] Transform contentHeroesCavalary = null;
    [SerializeField] Transform contentHeroesArcher = null;
    [SerializeField] Transform contentHeroesMagician = null;


    [Header("Other Preference")]
    [SerializeField] Slider sliderZoom = null;
    [SerializeField] PopUpRecruitTroops popUp = null;
    [SerializeField] Text costLimitText = null;
    [SerializeField] Text heroLimitText = null;
    [SerializeField] GameObject TutorialText = null;

    [Header("BtnPutList")]
    [SerializeField] GameObject btnPutInfantry;
    [SerializeField] GameObject btnPutArcher;
    [SerializeField] GameObject btnPutCavalary;
    [SerializeField] GameObject btnPutMagician;

    public void clearLineUpForm() {
        Debug.Log(lineUpform.Length);
        for (int i = 0; i < 13; i++)
        {
            for (int j = 3; j > 0; j--)
            {
                lineUpform[i, j - 1] = new BlockData();
            }
        }
        saveLineUp();
    }

    public void Start()
    {
        float zoomVal = PlayerPrefs.GetFloat("lineUpZoom", 1);
        sliderZoom.value = zoomVal;
        popUp.onFinishRecruitArmy = onFinishRecruitRecall;
    }

    public void ZoomLineUp(System.Single value) {
        Debug.Log("Zoom : " + Mathf.Lerp(100, 155, value));
        PlayerPrefs.SetFloat("lineUpZoom", value);
        contentLineUp.GetComponent<GridLayoutGroup>().cellSize = Vector2.one * Mathf.Lerp(100, 155, value);
    }

    void clearContent() {
        foreach (Transform t in contentLineUp) {
            Destroy(t.gameObject);
        }
        foreach (Transform t in contentHeroesInfantry)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in contentHeroesCavalary)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in contentHeroesArcher)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in contentHeroesMagician)
        {
            Destroy(t.gameObject);
        }
    }

    void checkLineUpEmpty() {
        bool found = false;
        foreach (BlockData d in lineUpform) {
            if (d.hero != "") {
                found = true;
                break;
            }
        }

        if (!found) {
            TutorialText.SetActive(true);
        }
    }

    void saveLineUp() {
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(lineUpform);

        string path = "lineUpData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json"; 

        RoyaleHeroesLibs.saveFileToName(path, json);
    }

    void loadLineUp() {

        string path = "lineUpData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json"; 

        string json = RoyaleHeroesLibs.loadFileFromName(path);
        if (json == null)
        {
            clearLineUpForm();
        }
        else {
            lineUpform = Newtonsoft.Json.JsonConvert.DeserializeObject<BlockData[,]>(json);
        }

    }

    public void initFormationTab(Dictionary<string, HeroesDataScriptableObject> userHeroes) {

        clearContent();

        heroLimit = heroLimitPerChapter[Account.CurrentAccountData.tierLevel-1];
        costMax = costLimitPerChapter[Account.CurrentAccountData.tierLevel-1];
        costUsed = 0;
        heroUsed = 0;

        loadLineUp();
        this.userHeroes = userHeroes;

        heroesFree.Clear();
        foreach (KeyValuePair<string, HeroesDataScriptableObject> entry in userHeroes)
        {
            heroesFree.Add(entry.Key);
        }

        initLineUp();
        initDeck();
        updateUIFormation();

        checkLineUpEmpty();

    }

    Transform getParentContentBasedOnClass(HeroesClass heroesClass) {
        switch (heroesClass) {
            case HeroesClass.Infantry:
                return contentHeroesInfantry;

            case HeroesClass.Archer:
                return contentHeroesArcher;

            case HeroesClass.Cavalary:
                return contentHeroesCavalary;

            case HeroesClass.Magician:
                return contentHeroesMagician;

        }
        return contentHeroesInfantry;
    }

    void updateUIFormation() {
        costLimitText.text = "Cost Used : " + costUsed.ToString() + " / " + costMax.ToString();
        heroLimitText.text = "Hero Used : " + heroUsed.ToString() + " / " + heroLimit.ToString();
    }

    void initLineUp() {

        for (int i = 0; i < 13; i++) {
            for (int j = 3; j > 0; j--) {
                string number = alphabet[i] + " " + ((j).ToString());
                BlockData data = lineUpform[i, j - 1];

                HeroesPlace temp = Instantiate(heroesPlace, contentLineUp);
                temp.position = number;
                temp.onPicked = itemPutted;
                temp.onHold = onHoldHeroOnLineUp;
                temp.onClick = onClickHeroOnLineUp;
                temp.coordinate = new Vector2(i, j - 1);

                if (data.hero != "") {

                    if (heroesFree.Contains(data.hero))
                    {
                        if ((costMax - costUsed) <= 0 || (heroLimit - heroUsed) <= 0) {
                            lineUpform[i, j - 1].reset();
                            continue;
                        }

                        heroesFree.Remove(data.hero);
                        temp.heroName = data.hero;
                        int levelHero = Account.CurrentAccountData.getHeroes(data.hero, userHeroes[data.hero].heroesClass).level;
                        temp.icon = userHeroes[data.hero].icon;
                        temp.border = RoyaleHeroesLibs.getLevelBorder(levelHero);
                        //Debug.Log();
                        heroUsed++;
                        costUsed += data.getCountArmy();
                    }
                    else {
                        lineUpform[i, j - 1].hero = "";
                        lineUpform[i, j - 1].army.Clear();
                    }
                }

            }
        }

        saveLineUp();

    }

    void onHoldHeroOnLineUp(HeroesPlace heroes) {
        if (moveItemPlace == heroes)
        {
            clearMoveItem();
            inactivatePutTriggerLineUp();
            inactivatePutTriggerList(userHeroes[heroes.heroName].heroesClass);
        }
        else
        {
            Debug.Log("Holded " + heroes.heroName);
            clearMoveItem();
            RoyaleHeroesToast.ShowToast("Moving Hero : " + heroes.heroName, 2);

            moveItemPlace = heroes;

            activatePutTriggerLineUp();
            activatePutTriggerList(userHeroes[heroes.heroName].heroesClass);
        }
    }
    void onClickHeroOnLineUp(HeroesPlace heroes)
    {
        Debug.Log("Clicked " + heroes.heroName);
        //lineUpform[(int)heroes.coordinate.x, (int)heroes.coordinate.y].getCountArmy();
        clearMoveItem();
        openPopUpRecruitArmy(heroes.heroName, heroes.coordinate);
    }

    void initDeck() {
        foreach (string name in heroesFree) {
            Transform parent = getParentContentBasedOnClass(userHeroes[name].heroesClass);
            HeroesListItem temp = Instantiate(heroesListItem, parent);
            temp.heroName = name;
            temp.icon = userHeroes[name].icon;
            int levelHero = Account.CurrentAccountData.getHeroes(name, userHeroes[name].heroesClass).level;
            temp.border = RoyaleHeroesLibs.getLevelBorder(levelHero);
            temp.onHold = listItemHolded;
        }
    }

    HeroesListItem moveItemList;
    HeroesPlace moveItemPlace;
    HeroesPlace targetItemPlace;

    public void listItemHolded(HeroesListItem item) {
        Debug.Log("Test : " + item.heroName);
        TutorialText.SetActive(false);
        if (heroUsed >= heroLimit) {
            clearMoveItem();
            inactivatePutTriggerLineUp();

            RoyaleHeroesToast.ShowToast("Sorry you hava reach maximum hero", 2);
            return;
        }

        if (moveItemList == item)
        {
            clearMoveItem();
            inactivatePutTriggerLineUp();
        }
        else
        {
            clearMoveItem();
            RoyaleHeroesToast.ShowToast("Moving Hero : " + item.heroName, 2);


            moveItemList = item;

            activatePutTriggerLineUp();
        }
    }

    public void activatePutTriggerLineUp() {
        foreach (Transform t in contentLineUp)
        {
            t.GetComponent<HeroesPlace>().activateButtonPut();
        }
    }
    public void inactivatePutTriggerLineUp()
    {
        foreach (Transform t in contentLineUp)
        {
            t.GetComponent<HeroesPlace>().inactivateButtonPut();
        }
    }
    public void activatePutTriggerList(HeroesClass hClass)
    {
        switch (hClass) {
            case HeroesClass.Infantry:
                btnPutInfantry.SetActive(true);
                break;
            case HeroesClass.Archer:
                btnPutArcher.SetActive(true);
                break;
            case HeroesClass.Magician:
                btnPutMagician.SetActive(true);
                break;
            case HeroesClass.Cavalary:
                btnPutCavalary.SetActive(true);
                break;
        }
    }
    public void inactivatePutTriggerList(HeroesClass hClass)
    {
        switch (hClass)
        {
            case HeroesClass.Infantry:
                btnPutInfantry.SetActive(false);
                break;
            case HeroesClass.Archer:
                btnPutArcher.SetActive(false);
                break;
            case HeroesClass.Magician:
                btnPutMagician.SetActive(false);
                break;
            case HeroesClass.Cavalary:
                btnPutCavalary.SetActive(false);
                break;
        }
    }


    public void itemPutted(HeroesPlace place) {
        Debug.Log("Item : " + place.heroName);
        inactivatePutTriggerLineUp();

        TutorialText.SetActive(false);

        targetItemPlace = place;
        if (moveItemList != null)// jika memindahkan list ke lineUp
        {
            putItemToLineUp(moveItemList.heroName);
            //moveItemList = null;
        }
        else if (moveItemPlace != null)// jika memindahkan lineUp ke lineUp
        {
            inactivatePutTriggerList( userHeroes[moveItemPlace.heroName].heroesClass);
            doChanges();
            //moveItemPlace = null;
        }
        else {
            Debug.LogError("Error 2 moving items are null");
        }
        

    }
    public void itemPutted()
    {
        
        inactivatePutTriggerLineUp();

        if (moveItemPlace != null)// jika memindahkan lineUp ke lineUp
        {
            inactivatePutTriggerList(userHeroes[moveItemPlace.heroName].heroesClass);
            Transform parent = null;
            switch (userHeroes[moveItemPlace.heroName].heroesClass) {
                case HeroesClass.Infantry:
                    parent = contentHeroesInfantry;
                    break;
                case HeroesClass.Archer:
                    parent = contentHeroesArcher;
                    break;
                case HeroesClass.Cavalary:
                    parent = contentHeroesCavalary;
                    break;
                case HeroesClass.Magician:
                    parent = contentHeroesMagician;
                    break;
            }
            HeroesListItem item = Instantiate(heroesListItem, parent);
            item.heroName = moveItemPlace.heroName;
            item.icon = moveItemPlace.icon;
            item.border = moveItemPlace.border;
            item.onHold = listItemHolded;

            moveItemPlace.heroName = "";
            moveItemPlace.icon = null;

            costUsed -= lineUpform[(int)moveItemPlace.coordinate.x, (int)moveItemPlace.coordinate.y].getCountArmy();
            heroUsed--;

            lineUpform[(int)moveItemPlace.coordinate.x, (int)moveItemPlace.coordinate.y].reset();

            saveLineUp();

            clearMoveItem();
            checkLineUpEmpty();
        }
        else
        {
            Debug.LogError("Error moving items place are null");
        }

    }



    void putItemToLineUp(string heroName) {

        //HeroesDataScriptableObject hero = userHeroes[heroName];
        openPopUpRecruitArmy(heroName, targetItemPlace.coordinate);

        

    }


    void openPopUpRecruitArmy(string heroName, Vector2 coordinate) {
        popUp.gameObject.SetActive(true);
        int levelHero = Account.CurrentAccountData.getHeroes(heroName, userHeroes[heroName].heroesClass).level;
        int avbCost = costMax - (costUsed - lineUpform[(int)coordinate.x, (int)coordinate.y].getCountArmy());
        if (lineUpform[(int)coordinate.x, (int)coordinate.y].hero != "")
        {
            popUp.InitPopUp(RoyaleHeroesLibs.getHeroesData(heroName), levelHero, coordinate, avbCost, lineUpform[(int)coordinate.x, (int)coordinate.y]);
        }
        else {
            popUp.InitPopUp(RoyaleHeroesLibs.getHeroesData(heroName), levelHero, coordinate, avbCost);
        }
        
        
    }

    void onFinishRecruitRecall(bool isConfirmed, BlockData data, Vector2 coordinate) {
        //popUp.onFinishRecruitArmy -= onFinishRecruitRecall;

        if (isConfirmed)
        {
            costUsed -= lineUpform[(int)coordinate.x, (int)coordinate.y].getCountArmy();
            lineUpform[(int)coordinate.x, (int)coordinate.y] = data;
            costUsed += lineUpform[(int)coordinate.x, (int)coordinate.y].getCountArmy();
            doChanges();
            
        }
        else {
            clearMoveItem();
            checkLineUpEmpty();
        }

    }
    void clearMoveItem()
    {
        if (moveItemPlace != null && moveItemPlace.heroName != "") {
            inactivatePutTriggerList(userHeroes[moveItemPlace.heroName].heroesClass);
            inactivatePutTriggerLineUp();
        }
        if (moveItemList != null) {
            inactivatePutTriggerLineUp();
        }
        moveItemList = null;
        moveItemPlace = null;
        targetItemPlace = null;

        

        updateUIFormation();
    }
    void doChanges() {

        

        if (moveItemList != null)
        {
            int levelHero = Account.CurrentAccountData.getHeroes(moveItemList.heroName, userHeroes[moveItemList.heroName].heroesClass).level;
            targetItemPlace.heroName = moveItemList.heroName;
            targetItemPlace.icon = moveItemList.icon;
            targetItemPlace.border = RoyaleHeroesLibs.getLevelBorder(levelHero);
            heroUsed++;
            Destroy(moveItemList.gameObject);
        }
        else if (moveItemPlace != null)
        {
            int levelHero = Account.CurrentAccountData.getHeroes(moveItemPlace.heroName, userHeroes[moveItemPlace.heroName].heroesClass).level;
            targetItemPlace.heroName = moveItemPlace.heroName;
            targetItemPlace.icon = moveItemPlace.icon;
            targetItemPlace.border = RoyaleHeroesLibs.getLevelBorder(levelHero);
            lineUpform[(int)targetItemPlace.coordinate.x, (int)targetItemPlace.coordinate.y].copy(lineUpform[(int)moveItemPlace.coordinate.x, (int)moveItemPlace.coordinate.y]);


            moveItemPlace.heroName = "";
            moveItemPlace.icon = null;
            lineUpform[(int)moveItemPlace.coordinate.x, (int)moveItemPlace.coordinate.y].reset();
        }
        //checkLineUpEmpty();
        clearMoveItem();
        saveLineUp();
    }


}


public class BlockData {
    public string hero = "";
    public bool autmovement = true;
    public HeroesClass heroClass = HeroesClass.Infantry;
    public Dictionary<string, int> army = new Dictionary<string, int>();

    public void reset() {
        hero = "";
        army.Clear();
    }
    public void copy(BlockData data) {
        reset();
        hero = data.hero;
        heroClass = data.heroClass;
        foreach (KeyValuePair<string, int> v in data.army) {
            army.Add(v.Key, v.Value);
        }
    }
    public int getCountArmy() {
        int cost = 0;
        if (army == null) {
            return 0;
        }
        foreach (KeyValuePair<string, int> v in army)
        {
            cost += v.Value;
        }
        return cost;
    }

    public CharacterStatus getTotalStatus() {
        CharacterStatus stats = new CharacterStatus();
        int totalUnit = 0;

        HeroesDataScriptableObject heroData = RoyaleHeroesLibs.getHeroesData(hero);

        ArmyDataHolderScriptableObject data = Resources.Load<ArmyDataHolderScriptableObject>("ArmyData/ArmyDataHolder");
        ArmyDataScriptableObject[] armyData = null;
        switch (heroClass)
        {
            case HeroesClass.Infantry:
                armyData = data.infantry;
                break;
            case HeroesClass.Archer:
                armyData = data.archer;
                break;
            case HeroesClass.Cavalary:
                armyData = data.cavalary;
                break;
            case HeroesClass.Magician:
                armyData = data.magician;
                break;
        }

        for (int i = 0; i < armyData.Length; i++)
        {
            foreach (KeyValuePair<string, int> v in army)
            {
                if (v.Key == armyData[i].armyName)
                {
                    stats += armyData[i].status * v.Value;
                    totalUnit += v.Value;
                    break;
                }
            }
        }

        stats += (heroData.status * 2000);//Mathf.Max(Mathf.CeilToInt(totalUnit * 0.05f), 1) );


        stats /= (totalUnit + 2000) * 1f;
        

        return stats;

    }

    public ResourcesMaterial getTotalResources()
    {
        ArmyDataHolderScriptableObject data = Resources.Load<ArmyDataHolderScriptableObject>("ArmyData/ArmyDataHolder");
        ResourcesMaterial res = new ResourcesMaterial();
        if (army == null)
        {
            return res;
        }

        ArmyDataScriptableObject[] armyData = null;
        switch (heroClass)
        {
            case HeroesClass.Infantry:
                armyData = data.infantry;
                break;
            case HeroesClass.Archer:
                armyData = data.archer;
                break;
            case HeroesClass.Cavalary:
                armyData = data.cavalary;
                break;
            case HeroesClass.Magician:
                armyData = data.magician;
                break;
        }


        for (int i = 0; i < armyData.Length; i++)
        {
            foreach (KeyValuePair<string, int> v in army)
            {
                if (v.Key == armyData[i].armyName)
                {
                    res.food = armyData[i].requirement.food * v.Value;
                    res.wood = armyData[i].requirement.wood * v.Value;
                    res.stone = armyData[i].requirement.stone * v.Value;

                    break;
                }
            }
        }
        
        return res;
    }
}

