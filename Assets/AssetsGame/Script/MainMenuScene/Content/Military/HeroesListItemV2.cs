﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class HeroesListItemV2 : MonoBehaviour
{

    [SerializeField] Image iconImage;
    [SerializeField] Image borderImage;
    [SerializeField] Button btnItem;
    [SerializeField] Text itemName;

    string _name;
    public string heroName {
        get {
            return _name;
        }
        set {
            _name = value;
            itemName.text = _name;
            //int level = Account.CurrentAccountData.getHeroes(_name)
            //borderImage.sprite = RoyaleHeroesLibs.getLevelBorder()
        }
    }

    public Sprite icon {
        set {
            if(iconImage != null)
                iconImage.sprite = value;
        }
    }
    public Sprite border
    {
        set
        {
            if(borderImage!= null)
                borderImage.sprite = value;
        }
    }

    public UnityAction onClick {
        set {
            if(btnItem!=null)
                btnItem.onClick.AddListener(value);
        }
    }
    
}
