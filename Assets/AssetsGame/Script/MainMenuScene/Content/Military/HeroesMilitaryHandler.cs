﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroesMilitaryHandler : MonoBehaviour
{

    [Header("Data")]
    [SerializeField] ExperienceData expData;
    [SerializeField] ExperienceData cardData;
    [SerializeField] Sprite activeTab;
    [SerializeField] Sprite inactiveTab;

    [Header("Pop Up")]
    [SerializeField] PopUpHeroPreview preview;

    [Header("UI Object")]
    [SerializeField] VerticalLayoutGroup contenHolder;
    [SerializeField] Transform holderInfantry;
    [SerializeField] Transform holderArcher;
    [SerializeField] Transform holderCavalary;
    [SerializeField] Transform holderMagician;
    [SerializeField] Transform holderSkill;
    [SerializeField] Transform centerSkill;


    [Header("UI HeroHeader")]
    [SerializeField] Image heroBorder;
    [SerializeField] Image heroIcon;
    [SerializeField] Text heroName;
    [SerializeField] Text heroLevelAndClass;

    [SerializeField] Image xpAmount;
    [SerializeField] Text xpTextBlack;
    [SerializeField] Text xpTextWhite;

    [SerializeField] Image cardAmount;
    [SerializeField] Text cardTextBlack;
    [SerializeField] Text cardTextWhite;

    [SerializeField] Image tabSkill;
    [SerializeField] Image tabStatus;

    [Header("UI - Status")]
    [SerializeField] Text hp = null;
    [SerializeField] Text phyAtk = null;
    [SerializeField] Text phyDef = null;
    [SerializeField] Text magAtk = null;
    [SerializeField] Text magDef = null;
    [SerializeField] Text marchSpeed = null;
    [SerializeField] Text atkSpeed = null;
    [SerializeField] Text cntrRate = null;
    [SerializeField] Text range = null;

    [Header("UI - Skill")]
    [SerializeField] Text nameSkill = null;
    [SerializeField] Text descSkill = null;
    [SerializeField] ScorllViewSnap scrollView = null;

    [Header("Prefabs")]
    [SerializeField] HeroesListItemV2 listItem;
    [SerializeField] SkillIconHolder listSkill;



    Dictionary<string, HeroesDataScriptableObject> userHeroes;

    public void initHeroesTab(Dictionary<string, HeroesDataScriptableObject> userHeroes)
    {
        clearContent();
        this.userHeroes = userHeroes;
        string nameRandom = "";
        foreach (KeyValuePair<string, HeroesDataScriptableObject> entry in userHeroes)
        {
            HeroesListItemV2 temp = Instantiate(listItem, getParentHolder(entry.Value.heroesClass));
            temp.heroName = entry.Value.heroName;
            temp.icon = entry.Value.icon;
            int levelHero = Account.CurrentAccountData.getHeroes(entry.Value.heroName, userHeroes[entry.Value.heroName].heroesClass).level;
            temp.border = RoyaleHeroesLibs.getLevelBorder(levelHero);
            temp.onClick = () => { showHeroesStatus(entry.Value.heroName); };

            if ((Random.RandomRange(0f, 1f) < 0.2f) || nameRandom == "") {
                nameRandom = entry.Value.heroName;
            }
        }

        showHeroesStatus(nameRandom);
        scrollView.Init();
    }

    public Transform getParentHolder(HeroesClass hClass) {
        switch (hClass)
        {
            case HeroesClass.Infantry:

                return holderInfantry;
            case HeroesClass.Archer:

                return holderArcher;
            case HeroesClass.Cavalary:

                return holderCavalary;
            case HeroesClass.Magician:

                return holderMagician;


        }
        return holderInfantry;
    }

    void clearContent()
    {

        foreach (Transform t in holderInfantry)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in holderCavalary)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in holderArcher)
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in holderMagician)
        {
            Destroy(t.gameObject);
        }
        contenHolder.spacing += 0.00001f;
    }
    string currentHero = "";
    int currentHeroLevel = 0;
    public void showHeroesStatus(string nameHero) {
        //Debug.Log(nameHero);
        currentHero = nameHero;
        HeroesDataScriptableObject dataHero = userHeroes[nameHero];

        hp.text = dataHero.status.health.ToString();
        phyAtk.text = dataHero.status.phyAttack.ToString();
        phyDef.text = dataHero.status.phyDeffense.ToString();
        magAtk.text = dataHero.status.magAttack.ToString();
        magDef.text = dataHero.status.magDeffense.ToString();
        marchSpeed.text = dataHero.status.movementSpeed.ToString();
        atkSpeed.text = dataHero.status.attackSpeed.ToString() + " /s";
        cntrRate.text = (Mathf.Floor(dataHero.status.counterAttackRate * 1000f) / 10f).ToString() + "%";
        range.text = dataHero.status.range.ToString();


        HeroesData dataHeroUser = Account.CurrentAccountData.getHeroes(nameHero, userHeroes[nameHero].heroesClass);

        int levelHero = dataHeroUser.level;
        currentHeroLevel = levelHero;
        heroBorder.sprite = RoyaleHeroesLibs.getLevelBorder(levelHero);
        heroName.text = nameHero;
        heroIcon.sprite = dataHero.icon;
        heroLevelAndClass.text = "Lv " + levelHero.ToString() + " ( " + dataHero.heroesClass.ToString() + " )";

        int targetXP = expData.listLevel[dataHeroUser.level - 1].experienceTarget;
        
        xpAmount.fillAmount = Mathf.Min(1, Mathf.InverseLerp(0, targetXP, Mathf.Min(dataHeroUser.xp, targetXP)));
        xpTextBlack.text = dataHeroUser.xp.ToString() + " / " + targetXP;
        xpTextWhite.text = dataHeroUser.xp.ToString() + " / " + targetXP;

        int targetCard = cardData.listLevel[dataHeroUser.level - 1].experienceTarget;

        cardAmount.fillAmount = Mathf.Min(1, Mathf.InverseLerp(0, targetCard, Mathf.Min(dataHeroUser.card, targetCard)));
        //Debug.Log(cardAmount.fillAmount +" -  - "+ dataHeroUser.card);
        cardTextBlack.text = dataHeroUser.card.ToString() + " / " + targetCard;
        cardTextWhite.text = dataHeroUser.card.ToString() + " / " + targetCard;

        foreach (SkillIconHolder skill in skills)
        {
            Destroy(skill.gameObject);
        }

        int prevSkillCount = skills.Count;

        skills.Clear();
        foreach (HeroesSkill skill in dataHero.skill) {
            SkillIconHolder holder = Instantiate(listSkill, holderSkill);
            holder.data = skill;
            holder.center = centerSkill;
            //holder.updateScale();
            skills.Add(holder);
        }

        if (dataHero.skill.Length == 0) {
            nameSkill.text = "";
            descSkill.text = "Heroes have no skills";
        }

        //scrollView.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
        if (prevSkillCount != dataHero.skill.Length)
        {
            scrollView.Init();
        }
        scrollView.GetComponent<ScrollRect>().verticalNormalizedPosition = 0.5f;
        scrollView.setDefaultPosition();
        
        //moveScrollView();

    }
    List<SkillIconHolder> skills = new List<SkillIconHolder>();

    string previewingSkill = "";

    

    public void previewDesc() {

        preview.init(RoyaleHeroesLibs.getHeroesData(currentHero));
    }

    public void moveScrollView() {

        if (skills.Count == 0)
            return;

        SkillIconHolder best = null;
        float min = 0;
        int idx = 0;
        for (int i= 0; i < skills.Count; i++) {
            float temp = skills[i].updateScale();
            if (temp < min || best == null) {
                best = skills[i];
                min = temp;
                idx = i;
            }
        }
        //Debug.Log(previewingSkill +" - "+ best.data.name);
        if (previewingSkill != best.data.name) {
            previewingSkill = best.data.name;
            // + " ( " + (best.data.isActiveSkill ? "Active" : "Passive") + " )"
            nameSkill.text = best.data.name +( currentHeroLevel > idx ? " ( Unlocked )" : " ( Unlock at Level "+(idx+1)+" )");
            descSkill.text = best.data.description;
        }
        

    }

    public void openStatus() {
        tabStatus.sprite = activeTab;
        tabSkill.sprite = inactiveTab;
    }
    public void openSkill()
    {
        tabStatus.sprite = inactiveTab;
        tabSkill.sprite = activeTab;
    }

}
