﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRecruit : MonoBehaviour
{
    ArmyDataScriptableObject armyData;
    public int valueMax = 10;
    public float costMax = 0;
    RoyaleHeroesLibs.Wrapper<int> valueCost = new RoyaleHeroesLibs.Wrapper<int>(0);
    RoyaleHeroesLibs.Wrapper<int> valueOnGroup = new RoyaleHeroesLibs.Wrapper<int>(2);

    int unitCount;
    [SerializeField] UnityEngine.UI.Text textUnitName = null;
    [SerializeField] UnityEngine.UI.Text textUnitCount = null;
    [SerializeField] UnityEngine.UI.Slider slider = null;
    [SerializeField] UnityEngine.UI.InputField textInput = null;

    public delegate void OnValueChangeDelegate();
    public OnValueChangeDelegate onValueChange;

    public void initUnit(ArmyDataScriptableObject armyData , int maxTroop ,float maxCost, RoyaleHeroesLibs.Wrapper<int> reffInt, RoyaleHeroesLibs.Wrapper<int> reffCost,  OnValueChangeDelegate eventDelegate , int defaultUnit = 0) {

        onValueChange = eventDelegate;
        

        this.armyData = armyData;
        valueMax = maxTroop;
        costMax = maxCost;


        unitCount = defaultUnit;
        slider.value = Mathf.InverseLerp(0, maxTroop, defaultUnit);


        //Debug.Log(name);
        valueCost = reffCost;
        valueOnGroup = reffInt;

        

        textUnitName.text = armyData.armyName;
        textUnitCount.text = unitCount.ToString();
    }
    //bool after = false;
    //float tempData = 0;
    public void ValueOnChange(System.Single val) {

        int offset = valueMax - valueOnGroup.Value;
        //float left = Mathf.InverseLerp(0, valueMax, offset);

        
        int prevUnitCount = unitCount;
        int nextUnitCount = (int)Mathf.Round(val * valueMax);

        float offsetCost = costMax - valueCost.Value;
        //Debug.Log(offset + " - " + offsetCost + " - "+nextUnitCount);
        if ((offsetCost < 0 && prevUnitCount < nextUnitCount) || ((nextUnitCount + valueCost.Value  - prevUnitCount) > costMax))
        {
            //after = true;
            int other = (int)Mathf.Max((valueCost.Value) - prevUnitCount , 0);
            //Debug.Log("here1");
            //tempData = Mathf.InverseLerp(0, valueMax, (costMax * 1000 - other));
            slider.value = Mathf.InverseLerp(0, valueMax, (costMax - other));
            nextUnitCount = (int)Mathf.Max((costMax - other) , 0);
        }
        else if ( (offset < 0 && prevUnitCount < nextUnitCount) || ((nextUnitCount+ valueOnGroup.Value-prevUnitCount) > valueMax) )
        {
            int other = valueOnGroup.Value - prevUnitCount;
            ///Debug.Log("here2");
            slider.value = Mathf.InverseLerp(0, valueMax, valueMax - other);
            nextUnitCount = valueMax - other;
        }
        //Debug.Log( nextUnitCount);
        unitCount = nextUnitCount;
        textUnitCount.text = unitCount.ToString();
        if (prevUnitCount != nextUnitCount)
        {
            onValueChange();
        }
        

        
        
        //onValueChange();

    }
    
    public void finishEditCount(UnityEngine.UI.InputField input) {
        Debug.Log("finish Edited");
        int value = int.Parse(input.text);

        float norm = Mathf.InverseLerp(0, valueMax, Mathf.Clamp(value, 0, valueMax));

        slider.value = norm;

    }
    public void matchTextInput() {
        textInput.text = unitCount.ToString();
    }
    
    public int getCountUnit() {
        return unitCount;
    }
    public string getNameUnit()
    {
        return armyData.armyName;
    }
    public int getResourceCost() {
        
        return (armyData.requirement.food + armyData.requirement.gold + armyData.requirement.wood + armyData.requirement.stone)*unitCount;
    }
    public int getResourceCost(ResourceClass rssClass)
    {
        switch (rssClass) {
            case ResourceClass.Food:
                return armyData.requirement.food * unitCount;
            case ResourceClass.Wood:
                return armyData.requirement.wood * unitCount;
            case ResourceClass.Stone:
                return armyData.requirement.stone * unitCount;
            default:
                return 0;
        }
        //return (armyData.requirement.food + armyData.requirement.gold + armyData.requirement.wood + armyData.requirement.stone) * unitCount;
    }

    public CharacterStatus getArmyStatus() {

        return armyData.status * unitCount;
    }

    public enum ResourceClass {
        Food , Wood , Stone , Gold
    }
    
    
}


