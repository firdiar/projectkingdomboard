﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HeroesListItem : MonoBehaviour , IPointerDownHandler , IPointerUpHandler //, IDragHandler
{
    string _name;
    public string heroName
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
            nameHeroes.text = _name;
        }
    }

    Sprite _icon;
    public Sprite icon
    {
        get
        {
            return _icon;
        }
        set
        {
            _icon = value;
            iconHeroes.sprite = _icon;
        }
    }

    Sprite _border;
    public Sprite border
    {
        get
        {
            return _border;
        }
        set
        {
            _border = value;
            borderHeroes.sprite = _border;
            
        }
    }

    [SerializeField] Image borderHeroes = null;
    [SerializeField] Image iconHeroes = null;
    [SerializeField] Text nameHeroes = null;


    public delegate void OnHoldDelegate(HeroesListItem item);
    public OnHoldDelegate onHold;

    void runEvent() {
        //Handheld.Vibrate();
        Vibration.Vibrate(200);
        onHold(this);
    }


    float timeCounter = 0;
    bool stillHold = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        timeCounter = 0;
        stillHold = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        stillHold = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //stillHold = false;
    }

    void Update()
    {
        if (stillHold) {
            if (timeCounter < 1f) {
                timeCounter += Time.deltaTime;
            }
            else{
                stillHold = false;
                runEvent();
            }
        }
    }
}
