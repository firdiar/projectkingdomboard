﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListTogleShower : MonoBehaviour {

    [SerializeField] VerticalLayoutGroup contentHolder = null;
    [SerializeField] GameObject CheckmarkIndicator = null;
    [SerializeField] GameObject togleContent = null;

    [SerializeField] GameObject placeholder = null;

    bool isOpen = false;

    public void OpenOrClose() {
        isOpen = !isOpen;

        togleContent.SetActive(isOpen);
        CheckmarkIndicator.SetActive(isOpen);

        contentHolder.spacing += 0.0002f * (isOpen ? 1 : -1);

        updatePlaceHolder();
        

    }
    public void updatePlaceHolder() {
        //Debug.Log(togleContent.transform.childCount+" - "+isOpen+" - "+this.name);

        //togleContent.GetComponent<VerticalLayoutGroup>().spacing += 0.002f;
        contentHolder.spacing += 0.0002f * (isOpen ? 1 : -1);
        if (isOpen)
        {
            
            placeholder.SetActive(togleContent.transform.childCount == 1);

        }
    }

}
