﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using Firebase.Database;

public class AddFriendsPopUpHandler : MonoBehaviour {

    [SerializeField] InputField inputSearch = null;
    [SerializeField] Transform contentHolder = null;
    [SerializeField] GameObject prefabSearchItem = null;

    VerticalLayoutGroup layoutGroup;

    ContentFriendsManager friendManager;

    List<FriendUserData> searchData = new List<FriendUserData>();

    public void Start()
    {
        layoutGroup = contentHolder.GetComponent<VerticalLayoutGroup>();
    }

    public void InitializePopUp(ContentFriendsManager fm )
    {
        Debug.Log("initialized");
        this.friendManager = fm;
        clearAll();
    }


    bool isReload = false;
    int prevCount = 0;

    public void SearchPlayer() {
        string pnick = inputSearch.text;
        if (pnick == "") {
            RoyaleHeroesToast.ShowToast("Please input player nickname", 2);
            return;
        }
        prevCount = 0;
        clearAll();

        //inputSearch.text = "";
        Debug.Log("Searching "+ pnick);
        // searching 
        DatabaseReference reff = FirebaseDatabase.DefaultInstance.RootReference.Child("account");
        reff.OrderByChild("nickName").EqualTo(pnick).GetValueAsync().ContinueWith(task => {//StartAt

            if (task.IsFaulted)
            {
                Debug.Log("Search failed : ");

            }
            else if (task.IsCompleted)
            {
                Debug.Log("Search finish");
                DataSnapshot snap = task.Result;
                
                if (snap.ChildrenCount == 0) { // handling not found

                    reff.OrderByChild("nickName").StartAt(pnick).GetValueAsync().ContinueWith(task2 => {
                        if (task2.IsFaulted)
                        {
                            Debug.Log("Search failed : ");

                        }
                        else if (task2.IsCompleted)
                        {
                            DataSnapshot snap2 = task2.Result;
                            foreach (DataSnapshot p in snap2.Children)
                            {

                                Debug.Log("Added : " + p.Key);

                                if (p.Key == FirebaseAuth.DefaultInstance.CurrentUser.UserId || friendManager.isExistInFriendList(p.Key))
                                {
                                    Debug.Log("Kontinue");
                                    continue;
                                }

                                IDictionary dictionary = (IDictionary)p.Value;
                                //Debug.Log("temp1");
                                FriendUserData fdata = new FriendUserData();
                                fdata.uid = p.Key;
                                fdata.level = int.Parse(dictionary["level"].ToString());
                                fdata.nickName = dictionary["nickName"].ToString();
                                //Debug.Log("temp2");
                                string conn = dictionary["connected"].ToString();
                                //Debug.Log(conn);
                                if (conn == "false")
                                {

                                    fdata.connected = false;
                                }
                                else
                                {

                                    fdata.connected = true;
                                }
                                searchData.Add(fdata);
                            }

                        }
                    });

                }
                
                foreach (DataSnapshot p in snap.Children) {

                    Debug.Log("Added : " + p.Key);

                    if (p.Key == FirebaseAuth.DefaultInstance.CurrentUser.UserId || friendManager.isExistInFriendList(p.Key)) {
                        Debug.Log("Kontinue");
                        continue;
                    }
                        
                    IDictionary dictionary = (IDictionary)p.Value;
                    //Debug.Log("temp1");
                    FriendUserData fdata = new FriendUserData();
                    fdata.uid = p.Key;
                    fdata.level = int.Parse(dictionary["level"].ToString());
                    fdata.nickName = dictionary["nickName"].ToString();
                    //Debug.Log("temp2");
                    string conn = dictionary["connected"].ToString();
                    //Debug.Log(conn);
                    if (conn == "false")
                    {
                        
                        fdata.connected = false;
                    }
                    else
                    {

                        fdata.connected = true;
                        /*
                        //Debug.Log("temp23");
                        System.DateTime date = System.DateTime.Parse(conn , RoyaleHeroesLibs.getCultureInfo());
                        //Debug.Log("temp24");
                        int res = (int)Mathf.Floor((float)(System.DateTime.UtcNow - date).TotalSeconds);
                        //Debug.Log("temp25");
                        if (res <= 300)
                        {
                            fdata.connected = true;
                        }
                        else
                        {
                            fdata.connected = false;
                        }
                        */
                        //Debug.Log("temp29");
                    }
                    //Debug.Log("temp3");
                    searchData.Add(fdata);
                    //Debug.Log("Data : "+searchData.Count);
                }
                isReload = true;
            }

        });

        
    }

    int updateContent = 0;

    private void FixedUpdate()
    {
        //Debug.Log(searchData.Count);
        if (prevCount != searchData.Count || isReload) {
            Debug.Log("Loop");
            isReload = false;
            prevCount = searchData.Count;
            //clearContent();
            Debug.Log("Rendering : " + searchData.Count);
            for (int i = 0; i < searchData.Count; i++) {
                renderContent(i);
            }
            updateContent = 10;
        }
        if (updateContent != 0) {
            layoutGroup.spacing = updateContent;
            updateContent = Mathf.Max(updateContent - 1, 0);
        }
    }

    public void SendFriendRequest(FriendUserData fdata) {
        if (friendManager.isExistInFriendList(fdata.uid)) {
            RoyaleHeroesToast.ShowToast("You already sent request to this player",2);
            return;
        }

        
        friendManager.AddFriends(fdata.uid);
        RoyaleHeroesToast.ShowToast("Request sended successfuly", 2);
        searchData.Remove(fdata);
        fdata.content = null;

    }

    public void ClosePopUp() {
        //this.transform.parent.gameObject.SetActive(false);
        clearAll();
        this.transform.gameObject.SetActive(false);
    }

    void clearAll() {
        clearData();
        clearContent();
    }

    void clearContent() {
        foreach (Transform item in contentHolder)
        {
            Destroy(item.gameObject);
        }
    }
    void clearData() {
        foreach (FriendUserData item in searchData)
        {
            if(item.content != null)
                Destroy(item.content.gameObject);
        }
        searchData.Clear();
    }

    

    void renderContent(int id) {
        searchData[id].content = Instantiate(prefabSearchItem, contentHolder);
        searchData[id].content.GetComponent<GeneralContentFriendButtonHolder<AddFriendsPopUpHandler>>().InitializeButton(searchData[id], this);

    }

}
