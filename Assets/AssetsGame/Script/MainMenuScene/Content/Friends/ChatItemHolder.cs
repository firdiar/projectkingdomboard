﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatItemHolder : MonoBehaviour {

    [SerializeField]Text text = null;

    public void setText(string val) {
        text.text = val;
        GetComponent<HorizontalLayoutGroup>().spacing = 0.001f;
        transform.GetChild(1).GetComponent<VerticalLayoutGroup>().spacing = 0.001f;
    }





}
