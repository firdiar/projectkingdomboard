﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneralContentFriendButtonHolder<T> : MonoBehaviour {

    [Header("Friends Tag")]
    [SerializeField] protected Text nickName;
    [SerializeField] protected Text status;
    [SerializeField] protected RawImage statusIndicator;
    [SerializeField] protected Color online;
    [SerializeField] protected Color offline;

    protected FriendUserData userData;
    protected T parent;

    public virtual void ReRenderButton() {
        InitializeButton(userData , parent);
    }

    public virtual void InitializeButton(FriendUserData fdata, T parent)
    {
        this.parent = parent;
        
        userData = fdata;

        nickName.text = fdata.nickName + " (Lv " + fdata.level + ")";

        if (fdata.connected)
        {
            statusIndicator.color = online;
            status.text = "Online";
        }
        else
        {
            statusIndicator.color = offline;
            status.text = "Offline";
        }

        this.gameObject.SetActive(true);
    }
}
