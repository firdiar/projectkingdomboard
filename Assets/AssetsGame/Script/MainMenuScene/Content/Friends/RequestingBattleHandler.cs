﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;

public class RequestingBattleHandler : MonoBehaviour {

    [SerializeField] ContentFriendsManager manager = null;
    [SerializeField]LoadingCircleScript loading = null;
    [SerializeField] UnityEngine.UI.Text status = null;
    [SerializeField] UnityEngine.UI.Text nickNameText = null;

    FriendBattleRequest _target;
    public FriendBattleRequest target
    {
        get { return _target; }
        set
        {
            _target = value;
            answered = false;
            accepted = false;
            time = 7;
            manager.isRequesting = true;
            nickNameText.text = _target.nickName;

            dbReffMe = FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId+"/"+_target.uid);
            dbReffHim = FirebaseDatabase.DefaultInstance.GetReference("/realtime/"  + _target.uid + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId );
            dbReffMe.ChildChanged += OnValueChanged;
            loading.InitializeLoading((int)time + 3, ClosePopUp);
            status.text = "Waiting";
        }
    }


    DatabaseReference dbReffMe = null;
    DatabaseReference dbReffHim = null;

    float time = 5;
    bool answered = false;
    bool accepted = false;

    void OnValueChanged(object sender, ChildChangedEventArgs args) {
        
        Debug.Log("Value Changed " );
        string status = args.Snapshot.Value.ToString();
        if (status == "rejected") {
            this.status.text = "Friend Refuse Battle Offer";
        }
        else {
            accepted = true;
            this.status.text = "Friend Accept Battle Offer";
            manager.gotoBattleScene(target.roomID);
        }
        answered = true;
    }

    void ClosePopUp() {
        Debug.Log("Close Pop : "+answered);
        if (!answered || !accepted)
        {

            if (dbReffHim != null)
            {
                dbReffHim.RemoveValueAsync();
            }
            if (dbReffMe != null)
            {
                dbReffMe.RemoveValueAsync();
            }

            //Destroy(gameObject);
            gameObject.SetActive(false);
            manager.isRequesting = false;
        }

    }

    public void SetAnswer(string ans) {
        answered = true;
        status.text = ans;
    }

    private void Update()
    {
        if (!answered) {
            if (time > 0) {
                time -= Time.deltaTime;
                if (time <= 0) {
                    answered = true;
                    status.text = "No Answer";
                }
            } 
                
            
        }
    }



}
