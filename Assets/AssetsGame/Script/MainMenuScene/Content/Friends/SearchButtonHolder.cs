﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchButtonHolder : GeneralContentFriendButtonHolder<AddFriendsPopUpHandler>
{
    public void sendRequest() {
        if (parent == null) {
            return;
        }

        parent.SendFriendRequest(userData);
        Destroy(this.gameObject);
    }
    
    
}
