﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendingButtonHolder : GeneralContentFriendButtonHolder<ContentFriendsManager>
{

    public void CancelPendingStatus() {
        parent.CancelFriendRequest(userData);
    }

}
