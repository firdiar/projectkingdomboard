﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;

public class BattleRequestComingHandler : MonoBehaviour {

    public ContentFriendsManager manager;
    [SerializeField] LoadingCircleScript loading = null;
    [SerializeField] UnityEngine.UI.Text nickNameText = null;

    FriendBattleRequest _target;
    public FriendBattleRequest target
    {
        get { return _target; }
        set
        {
            _target = value;
            nickNameText.text = _target.nickName;

            dbReffMe = FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/" + _target.uid);
            dbReffHim = FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + _target.uid + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId);

            loading.InitializeLoading((int)5, ClosePopUp);

        }
    }

    
    DatabaseReference dbReffMe = null;
    DatabaseReference dbReffHim = null;

    bool accept = false;
    bool actioned = false;

    void ClosePopUp()
    {
        //Debug.Log("Close Pop : " + answered);
        if (!accept && !actioned)
        {
            actioned = true;
            if (dbReffHim != null)
            {
                dbReffHim.RemoveValueAsync();
            }
            if (dbReffMe != null)
            {
                dbReffMe.RemoveValueAsync();
            }

            
        }
        else if (!actioned) {
            actioned = true;
            Debug.Log("Move Battle Scene");
            manager.gotoBattleScene(target.roomID);
        }
        Destroy(gameObject);

    }

    public void AcceptRequest() {
        dbReffMe.Child("status").SetValueAsync("accepted");
        dbReffHim.Child("status").SetValueAsync("accepted");
        accept = true;
        ClosePopUp();
    }
    public void RefuseRequest()
    {
        dbReffMe.Child("status").SetValueAsync("rejected");
        dbReffHim.Child("status").SetValueAsync("rejected");
        accept = false;
        ClosePopUp();
    }

}
