﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using Firebase.Database;


public class ChatPageHandler : MonoBehaviour {

    [Header("UI")]
    public Text nickName;
    public RectTransform contentChatHolder;
    public InputField inputField;
    [Header("Prefabs")]
    public GameObject prefabChatFriends;
    public GameObject prefabChatMe;



    List<ChatValue> chatList = new List<ChatValue>();
    DatabaseReference reffChat = null;
    bool newTextAvailable = false;

    public void InitializePage(string nickName , string chatID)
    {
        

        this.nickName.text = nickName;
        ClearChatContent();
        chatList.Clear();
        Debug.Log(chatID);
        reffChat = FirebaseDatabase.DefaultInstance.GetReference("/message/" + chatID);

        AddListener();

        //loadChatData(chatID);
        
        

    }

    void HandleNewMessageRealtime(object sender, ChildChangedEventArgs args) {
        DataSnapshot snap = args.Snapshot;

        IDictionary dictionary = (IDictionary)snap.Value;

        System.DateTime time = System.DateTime.Parse(dictionary["time"].ToString());

        if ((System.DateTime.UtcNow - time).TotalDays > 5) {
            Clean(snap.Key);
        }

        ChatValue chat = new ChatValue();
        chat.msg = dictionary["msg"].ToString();
        chat.sender = dictionary["sender"].ToString();
        

        chat.time = time;
        chatList.Add(chat);
        newTextAvailable = true;
    }

    void InsertChatText() {
        chatList.Sort(delegate (ChatValue c1, ChatValue c2) { return c1.time.CompareTo(c2.time); });
        ChatItemHolder temp;
        foreach (ChatValue chat in chatList) {
            Debug.Log(chat.sender+"   "+ FirebaseAuth.DefaultInstance.CurrentUser.UserId);
            if (chat.sender == FirebaseAuth.DefaultInstance.CurrentUser.UserId)
            {
                temp = Instantiate(prefabChatMe, contentChatHolder).GetComponent<ChatItemHolder>();
            }
            else {
                temp = Instantiate(prefabChatFriends, contentChatHolder).GetComponent<ChatItemHolder>();
            }
            //temp.transform.SetAsLastSibling();
            temp.setText(chat.msg);
        }
        chatList.Clear();
    }

    public void AddListener() {
        if (reffChat == null)
            return;

        reffChat.ChildAdded += HandleNewMessageRealtime;
    }

    public void DeleteListener() {
        if (reffChat == null)
            return;

        reffChat.ChildAdded -= HandleNewMessageRealtime;
    }

    void ClearChatContent() {
        
        /*int childCount = contentChatHolder.childCount;
        Debug.Log(childCount);
        for (int i = 0; i < childCount; i++) {
            Destroy(contentChatHolder.GetChild(0).gameObject);
        }*/

        foreach (Transform child in contentChatHolder.transform) {
            Destroy(child.gameObject);
        }
    }

    void Clean(string key) {
        reffChat.Child(key).RemoveValueAsync();
    }

    public void Send() {

        ChatValue chat = new ChatValue();
        chat.msg = inputField.text;
        chat.sender = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        chat.time = System.DateTime.UtcNow;
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(chat);
        reffChat.Push().SetRawJsonValueAsync(json);

        inputField.text = "";

    }
	
	// Update is called once per frame
	void Update () {
        if (newTextAvailable) {
            newTextAvailable = false;
            InsertChatText();
        }
	}
}

public class ChatValue {
    
    public string msg;
    public string sender;
    public System.DateTime time;

}
