﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FriendButtonHolder : GeneralContentFriendButtonHolder<ContentFriendsManager>
{

    [Header("Friends Option Holder")]
    [SerializeField] RectTransform buttonOpen = null;
    [SerializeField] RectTransform optionHolder = null;
    [SerializeField] Button btnBattle = null;

    bool isMenuOpened = false;
    Coroutine currentCoroutine = null;

    public override void InitializeButton(FriendUserData fdata, ContentFriendsManager parent)
    {

        base.InitializeButton(fdata , parent);
        btnBattle.interactable = fdata.connected;
        //Debug.Log("Connected : "+ fdata.connected);

    }


    public override void ReRenderButton()
    {
        InitializeButton(userData, parent);

    }

    public void OpenAndCloseMenu() {

        if (currentCoroutine != null) {
            StopCoroutine(currentCoroutine);
        }

        if (!isMenuOpened)
        {
            currentCoroutine = StartCoroutine(Open());
        }
        else {
            currentCoroutine = StartCoroutine(Close());
        }
        isMenuOpened = !isMenuOpened;

    }

    IEnumerator Open() {


        Vector2 tempVector = new Vector2(optionHolder.sizeDelta.x, optionHolder.sizeDelta.y);
        Vector3 rotation = new Vector3(buttonOpen.localRotation.x, buttonOpen.localRotation.y, buttonOpen.localRotation.z);

        EventSystem.current.SetSelectedGameObject(null);

        while (optionHolder.rect.height < 110) {
            tempVector.y = Mathf.Clamp(optionHolder.rect.height+10, 0, 110);
            optionHolder.sizeDelta = tempVector;

            rotation.z =  (tempVector.y / (110 * 1.0f)) * 180;

            //Debug.Log(rotation.z);
            buttonOpen.localRotation = Quaternion.Euler(rotation);

            yield return new WaitForSeconds(0.01f);
        }


        yield return null;
    }

    IEnumerator Close() {

        Vector2 tempVector = new Vector2(optionHolder.sizeDelta.x, optionHolder.sizeDelta.y);
        Vector3 rotation = new Vector3(buttonOpen.localRotation.x , buttonOpen.localRotation.y , buttonOpen.localRotation.z);

        EventSystem.current.SetSelectedGameObject(null);

        while (optionHolder.rect.height > 0)
        {
            tempVector.y = Mathf.Clamp(optionHolder.rect.height - 10, 0, 110);
            optionHolder.sizeDelta = tempVector;


            rotation.z = (tempVector.y / (110 * 1.0f)) * 180;
            buttonOpen.localRotation = Quaternion.Euler( rotation );

            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }

    public void Delete() {
        if (parent == null) {
            Debug.Log("cant find manager");
            return;
        }
        parent.DeleteFriends(userData);
    }
    public void Battle()
    {
        if (parent == null)
        {
            Debug.Log("cant find manager");
            return;
        }
        //parent.DeleteFriends(userData);
        parent.Battle(userData);
    }
    public void Chat()
    {
        if (parent == null)
        {
            Debug.Log("cant find manager");
            return;
        }
        parent.OpenChat(userData);
    }

}
