﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestButtonHolder : GeneralContentFriendButtonHolder<ContentFriendsManager>
{

    public void AcceptFriendRequest() {
        parent.AcceptFriendRequest(userData);
    }

    public void DeclineFriendRequest()
    {
        parent.DeclineFriendRequest(userData);
    }

}
