﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Photon.Pun;
using Photon.Realtime;

public class ContentFriendsManager : ContentHandler
{

    [SerializeField] ChatPageHandler chatPage = null;

    [SerializeField] UnityEngine.UI.VerticalLayoutGroup content = null;

    [Header("Shower Button")]
    [SerializeField] ListTogleShower pendingBtn = null;
    [SerializeField] ListTogleShower requestBtn = null;
    [SerializeField] ListTogleShower friendListBtn = null;

    [Header("Holder")]
    [SerializeField] UnityEngine.UI.Toggle autoToggle = null;
    [SerializeField] GameObject pendingHolder = null;
    [SerializeField] GameObject requestHolder = null;
    [SerializeField] GameObject friendListHolder = null;

    [Header("Pop Screen")]
    [SerializeField] GameObject popScreen = null;
    [SerializeField] AddFriendsPopUpHandler addFriendsContent = null;
    [SerializeField] RequestingBattleHandler requestingBattle = null;
    [SerializeField] BattleRequestComingHandler battleComing = null;


    [Header("Prefabs")]
    [SerializeField] GameObject prefabsPending = null;
    [SerializeField] GameObject prefabsRequest = null;
    [SerializeField] GameObject prefabsFriends = null;



    List<FriendUserData> idFriendList = new List<FriendUserData>(); //ListFriend

    List<int> spawnList = new List<int>();
    Queue<int> destroyList = new Queue<int>();

    int isLoadingFinish = 0;
    public bool isRequesting { get; set; }

    DatabaseReference dbReffRealtime = null;

    public void dummy() {

    }
    public void init()
    {
        //base.Start();
        //GetChat();

        //Debug.Log("Start");

        dbReffRealtime = FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId);
        dbReffRealtime.ChildAdded += handleChildAdded;
        InvokeRepeating("ReloadFriends" , 0 , 3);

        autoToggle.isOn = (PlayerPrefs.GetInt("AUTO_REJECT") == 1 ? true : false);


    }
    void handleChildAdded(object sender, ChildChangedEventArgs args)
    {
        //Debug.Log("Child added");
        DataSnapshot snap = args.Snapshot;
        Debug.Log(snap.Key);

        IDictionary dictionary = (IDictionary)snap.Value;
        FriendBattleRequest temp = new FriendBattleRequest(dictionary["uid"].ToString() , dictionary["status"].ToString() , dictionary["nickName"].ToString(), dictionary["roomID"].ToString(), dictionary["time"].ToString());
        double deltaTime = (System.DateTime.UtcNow - temp.time).TotalSeconds;
        //Debug.Log("Time delta : "+deltaTime+" / "+temp.time+" # " + System.DateTime.UtcNow);
        if ( deltaTime > 5) {

            FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/" + temp.uid).RemoveValueAsync();
            FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + temp.uid + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId).RemoveValueAsync();

            return;
        }

        if (dictionary["status"].ToString() == "sent") {
            OpenRequestingBattle(temp);
        }
        else
        {
            if (isRequesting || autoToggle.isOn)
            {
                Debug.Log("Battle Request Auto Reject");
                FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/" + temp.uid+"/status").SetValueAsync("rejected");
                FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + temp.uid + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/status").SetValueAsync("rejected");
            }
            else {
                Debug.Log("Battle Request Recived");
                GotBattleRequest(temp);
            }
            
        }
        
    }

    public void AutoRejectChange() {
        //Debug.Log("Test : "+ autoToggle.isOn);
        PlayerPrefs.SetInt("AUTO_REJECT" , (autoToggle.isOn?1:0) );
    }

    public void GotBattleRequest(FriendBattleRequest fbr) {

        if (popScreen == null) {
            return;
        }

        BattleRequestComingHandler obj = Instantiate(battleComing.gameObject , popScreen.transform ).GetComponent<BattleRequestComingHandler>();
        obj.manager = this;
        obj.target = fbr;
    }

    public void OpenAddFriends() {
        //popScreen.SetActive(true);
        
        addFriendsContent.gameObject.SetActive(true);
        addFriendsContent.InitializePopUp(this);

    }

    public void ReloadFriends() {
        //Debug.Log("Reloading Friends : "+isLoadingFinish);

        if (isLoadingFinish != 0) {
            return;
        }

        GetFriendListID();
    }

    public bool isExistInFriendList(string uid)
    {
        foreach (FriendUserData fdata in idFriendList)
        {
            if (fdata.uid == uid)
            {
                return true;
            }
        }
        return false;
    }

    /*public void GetChat() {
        
        DatabaseReference reff = FirebaseDatabase.DefaultInstance.GetReference("/message"); //l7oxMWRvxTSDBLgg2dLpSQAowxc2
        Debug.Log(FirebaseAuth.DefaultInstance.CurrentUser.UserId);
        reff.OrderByChild("user").EqualTo("<123123123><firdi>").GetValueAsync().ContinueWith(task => {
            Debug.Log("task recived ...");
            if (task.IsFaulted)
            {
                Debug.Log("data fault");
                
            }
            else if (task.IsCompleted)
            {
                Debug.Log("data complete");
                DataSnapshot snapshot = task.Result;

                if (snapshot.Value == null)
                {
                    Debug.Log("New Chat Detected");

                }
                else
                {
                    
                    foreach (DataSnapshot snap in snapshot.Children)
                    {
                        IDictionary chat = (IDictionary)snap.Value;

                        //chat.Keys.ToString();
                        Debug.Log(chat.Keys.Count);

                        Debug.Log(chat["val"].ToString());
                    }
                        

                }
                    
            }
        });
    }*/

    public void AddFriends(string UID) {
        DatabaseReference reffMe = FirebaseDatabase.DefaultInstance.GetReference("/friendship/"+ FirebaseAuth.DefaultInstance.CurrentUser.UserId+"/"+UID);
        DatabaseReference reffHim = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + UID + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId);

        string chatID = RoyaleHeroesLibs.RandomString(22);

        FriendStatus friendMe = new FriendStatus("sent" , chatID);
        FriendStatus friendHim = new FriendStatus("recived", chatID);

        string jsonMe = Newtonsoft.Json.JsonConvert.SerializeObject(friendMe);
        string jsonHim = Newtonsoft.Json.JsonConvert.SerializeObject(friendHim);

        reffMe.SetRawJsonValueAsync(jsonMe).ContinueWith(task => {
            
            if (task.IsFaulted)
            {
                Debug.Log("Friend Request fault");

            }
            else if (task.IsCompleted)
            {
                Debug.Log("Friend Request Success");
                ReloadFriends();
            }

        });
        reffHim.SetRawJsonValueAsync(jsonHim);

    }

    public void AcceptFriends(string UID)
    {
        DatabaseReference reffMe = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/" + UID+"/status");
        DatabaseReference reffHim = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + UID + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId+"/status");

        reffMe.SetValueAsync("accepted").ContinueWith(task => {

            if (task.IsFaulted)
            {
                Debug.Log("Friend Request fault");

            }
            else if (task.IsCompleted)
            {
                Debug.Log("Friend Request Success");
                ReloadFriends();
            }

        });
        reffHim.SetValueAsync("accepted");

    }

    public void CancelFriends(string UID)
    {
        DatabaseReference reffMe = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/" + UID );
        DatabaseReference reffHim = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + UID + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId );

        for (int i = 0; i < idFriendList.Count; i++) {
            if (idFriendList[i].uid == UID) {
                //destroyList.Enqueue(i);
                destroyFromList(i);
                contentHolderUpdate();
                idFriendList.RemoveAt(i);
                break;
            }
        }

        reffMe.RemoveValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                Debug.Log("Cancel friend fault");
            }
            else if (task.IsCompleted)
            {
                Debug.Log("Cancel friend Success");
                ReloadFriends();
            }
        });
        reffHim.RemoveValueAsync();

    }

    public void GetFriendListID() {
        isLoadingFinish = -1;

        //idFriendList.Clear();
        DatabaseReference reff  = FirebaseDatabase.DefaultInstance.GetReference("/friendship/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId);
        reff.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                Debug.Log("get friendlist failed");
            }
            else if (task.IsCompleted)
            {
                //Debug.Log("get friendlist Success");
                DataSnapshot snapshot = task.Result;
                //if (snapshot.ChildrenCount < idFriendList.Count) {
                // idFriendList.Clear();
                //destroyList.Enqueue(i);
                //}

                //List<string> idExist = new List<string>();

                foreach (DataSnapshot snap in snapshot.Children)
                {
                    bool isExist = false;

                    IDictionary dictionary = (IDictionary)snap.Value;
                    //idExist.Add(snap.Key);
                    for (int i = 0; i < idFriendList.Count; i++)
                    {
                        if (idFriendList[i].uid == snap.Key)
                        {
                            isExist = true;
                            //Debug.Log("Exist : " + idFriendList[i].uid);
                            if (dictionary["status"].ToString() != idFriendList[i].status)
                            {
                                idFriendList[i].status = dictionary["status"].ToString();
                                destroyList.Enqueue(i);
                            }

                            break;
                        }
                    }

                    if (!isExist)
                    {
                        //Debug.Log("Data Not Exist : "+ snap.Key);
                        FriendUserData fdata = new FriendUserData();
                        fdata.uid = snap.Key;

                        
                        fdata.chatID = dictionary["chatID"].ToString();
                        fdata.status = dictionary["status"].ToString();
                        idFriendList.Add(fdata);
                    }
                    
                }

                /*
                for (int i = 0; i < idFriendList.Count; i++)
                {
                    bool found = false;
                    foreach (string key in idExist) {
                        if (key == idFriendList[i].uid) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        destroyList.Enqueue(i);
                    }
                }*/

                isLoadingFinish = idFriendList.Count;
                //Debug.Log("friendlist : "+idFriendList.Count);
                for (int i = 0; i < idFriendList.Count; i++) {
                    GetFriendListData(i);
                }

            }


        });

    }

    void GetFriendListData(int counter) {
        //Debug.Log("Get Friend Data : "+counter);
        FirebaseDatabase.DefaultInstance.GetReference("/account/" + idFriendList[counter].uid).GetValueAsync().ContinueWith(task =>
        {
            
            //Debug.Log("Get Friend Data2 : "+ isLoadingFinish);
            if (task.IsFaulted)
            {
                Debug.Log("get friendlist failed");
                isLoadingFinish = Mathf.Max(isLoadingFinish - 1, 0);
            }
            else if (task.IsCompleted)
            {
                //Debug.Log("Get Friend Data complete");

                DataSnapshot snapshot = task.Result;
                IDictionary nick = (IDictionary)snapshot.Value;
                isLoadingFinish = Mathf.Max(isLoadingFinish - 1, 0);
                //Debug.Log("Get Friend Data3 : " + counter);

                idFriendList[counter].nickName = nick["nickName"].ToString();
                idFriendList[counter].level = int.Parse(nick["level"].ToString());
                idFriendList[counter].tierLevel = int.Parse(nick["tierLevel"].ToString());

               

                string conn = nick["connected"].ToString();
                
                if (conn == "false")
                {
                    idFriendList[counter].connected = false;
                }
                else
                {
                    System.DateTime date = System.DateTime.Parse(conn , RoyaleHeroesLibs.getCultureInfo());
                    double res = (System.DateTime.UtcNow - date).TotalSeconds;
                    //Debug.Log("asdasdasdads : "+res+" - "+ System.DateTime.UtcNow + " - "+date);
                    if (res <= 300)
                    {
                        idFriendList[counter].connected = true;
                    }
                    else
                    {
                        idFriendList[counter].connected = false;
                    }

                }
                //Debug.Log("Get Friend Data : " + idFriendList[counter].nickName+" Connected : "+ idFriendList[counter].connected);

                //insertInList(counter);
                spawnList.Add(counter);
                //Debug.Log(counter +" - "+ idFriendList[counter].nickName);
            }

        });
    }

    void contentHolderUpdate() {

        pendingBtn.updatePlaceHolder();
        requestBtn.updatePlaceHolder();
        friendListBtn.updatePlaceHolder();


    }

    private void Update()
    {


        if (destroyList.Count > 0)
        {

            //spawnList.pop
            
            int idx = destroyList.Dequeue();
            Debug.Log("Destoying : " + idx);
            destroyFromList(idx);
            contentHolderUpdate();

        }

        if (spawnList.Count > 0) {

            //spawnList.pop
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(spawnList);
            int idx = spawnList[0];
            spawnList.RemoveAt(0);

            //Debug.Log("Spawn : " + idx);
            //Debug.Log("data : " + json);
            insertInList(idx);
            contentHolderUpdate();

        }

    }

    void destroyFromList(int id) {
        //Debug.Log(idFriendList[id].content == null);
        if (idFriendList[id].content == null)
        {
            return;
        }
        //Debug.Log(idFriendList[id].content == null);
        
        Destroy(idFriendList[id].content.gameObject);

        //Debug.Log(idFriendList[id].content == null);

        idFriendList[id].content = null;
        //Debug.Log(idFriendList[id].content == null);

        //idFriendList.RemoveAt(id);

    }

    void insertInList(int id) {

        //Debug.Log("inserting : "+idFriendList[id].uid+" - "+id);

        if (idFriendList[id].content != null) {

            idFriendList[id].content.GetComponent<GeneralContentFriendButtonHolder<ContentFriendsManager>>().ReRenderButton();
            return;

        }

        

        if (idFriendList[id].status == "recived"){
            
            idFriendList[id].content = Instantiate(prefabsRequest, requestHolder.transform);
            idFriendList[id].content.GetComponent<RequestButtonHolder>().InitializeButton(idFriendList[id], this);
        }
        else if (idFriendList[id].status == "sent"){

            idFriendList[id].content = Instantiate(prefabsPending, pendingHolder.transform);
            idFriendList[id].content.GetComponent<PendingButtonHolder>().InitializeButton(idFriendList[id], this);
        }
        else {

            idFriendList[id].content = Instantiate(prefabsFriends, friendListHolder.transform);
            idFriendList[id].content.GetComponent<FriendButtonHolder>().InitializeButton(idFriendList[id], this);
        }
        content.spacing += 0.001f * (id % 2 == 0 ? -1 : 1);
        
        

    }
    void insertInList(FriendUserData fdata)
    {

        if (fdata.content != null)
        {

            fdata.content.GetComponent<GeneralContentFriendButtonHolder<ContentFriendsManager>>().ReRenderButton();
            return;

        }


        if (fdata.status == "recived")
        {

            fdata.content = Instantiate(prefabsRequest, requestHolder.transform);
            fdata.content.GetComponent<RequestButtonHolder>().InitializeButton(fdata, this);
        }
        else if (fdata.status == "sent")
        {

            fdata.content = Instantiate(prefabsPending, pendingHolder.transform);
            fdata.content.GetComponent<PendingButtonHolder>().InitializeButton(fdata, this);
        }
        else
        {

            fdata.content = Instantiate(prefabsFriends, friendListHolder.transform);
            fdata.content.GetComponent<FriendButtonHolder>().InitializeButton(fdata, this);
        }
        
        content.spacing += 0.001f;

        fdata.content.GetComponent<GeneralContentFriendButtonHolder<ContentFriendsManager>>().InitializeButton(fdata, this);

    }


    public void DeleteFriends(FriendUserData fdata) {
        Debug.Log("Friends Deleted");
        CancelFriends(fdata.uid);
    }

    public void OpenChat(FriendUserData fdata) {
        Debug.Log("Chat Opened");

        chatPage.InitializePage(fdata.nickName , fdata.chatID);

        openChatPage();

        //chatPage;


    }

    void openChatPage() {
        chatPage.transform.SetAsFirstSibling();
    }
    public void CloseChatPage()
    {
        chatPage.transform.SetAsLastSibling();
    }

    public void Battle(FriendUserData fdata)
    {
        Debug.Log("Battle Asked");
        string roomID = RoyaleHeroesLibs.RandomString(22);

        FriendBattleRequest req = new FriendBattleRequest(fdata.uid , "sent" , fdata.nickName , roomID , System.DateTime.UtcNow);
        //Debug.Log(req.time);
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(req);

        FriendBattleRequest req2 = new FriendBattleRequest(FirebaseAuth.DefaultInstance.CurrentUser.UserId, "recived", Account.CurrentAccountData.nickName, roomID, System.DateTime.UtcNow);
        string json2 = Newtonsoft.Json.JsonConvert.SerializeObject(req2);


        dbReffRealtime.Child(fdata.uid).SetRawJsonValueAsync(json);
        FirebaseDatabase.DefaultInstance.GetReference("/realtime/" + fdata.uid + "/" + FirebaseAuth.DefaultInstance.CurrentUser.UserId).SetRawJsonValueAsync(json2);
    }

    public void OpenRequestingBattle(FriendBattleRequest fbr) {


        requestingBattle.target = fbr;
        requestingBattle.gameObject.SetActive(true);
    }

    public void gotoBattleScene(string roomID) {
        Debug.Log("Move To Battle Scene : "+roomID);

        RoomOptions option = new RoomOptions();
        
        option.MaxPlayers = 2;
        PhotonNetwork.JoinOrCreateRoom(roomID , option , TypedLobby.Default);

    }

    public void CancelFriendRequest(FriendUserData fdata)
    {
        Debug.Log("Cancel Friend Request");
        CancelFriends(fdata.uid);
        //ReloadFriends();
    }
    public void AcceptFriendRequest(FriendUserData fdata)
    {
        Debug.Log("Accept Friend Request");
        AcceptFriends(fdata.uid);
        //ReloadFriends();
    }
    public void DeclineFriendRequest(FriendUserData fdata)
    {
        Debug.Log("Decline Friend Request");
        CancelFriends(fdata.uid);
        
    }


}

public class MessageObject {
    string sender;
    string msg;
}


//status : sent , recived , accepted
public class FriendStatus {
    public string status;
    public string chatID;

    public FriendStatus(string status , string chatID) {
        this.status = status;
        this.chatID = chatID;
    }
}

public class FriendUserData{
    public string uid;
    public string nickName;
    public int level;
    public int tierLevel;
    public bool connected;
    public string status;
    public string chatID;
    public GameObject content;
}

public class FriendBattleRequest {
    public string uid;
    public string status;
    public string nickName;
    public string roomID;
    public System.DateTime time;
    public FriendBattleRequest(string uid , string status , string nickName , string roomID , string time)
    {
        this.uid = uid;
        this.status = status;
        this.roomID = roomID;
        this.nickName = nickName;
        this.time = System.DateTime.Parse(time).ToUniversalTime();
    }
    public FriendBattleRequest(string uid, string status, string nickName, string roomID, System.DateTime time)
    {
        this.uid = uid;
        this.status = status;
        this.roomID = roomID;
        this.nickName = nickName;
        this.time = time;
    }
}
