﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LoadingCircleScript : MonoBehaviour {

    float _setCount;
    public float setCount {
        get { return _setCount; }
        set {
            textCounter.text = Mathf.Ceil(value).ToString();
            _setCount = value;
        }
    }

   

    [SerializeField] GameObject prefabLoadingPart = null;
    [SerializeField] Text textCounter = null;
    [SerializeField] int countCircle = 0;

    [SerializeField]UnityEvent eventAfterFinish = new UnityEvent();

    List<Image> circler = new List<Image>();
    void Start()
    {
        float degreePerItem =  360/(countCircle * 1.0f);

        for (int i = 0; i < countCircle; i++) {
            GameObject obj = Instantiate(prefabLoadingPart, transform);
            obj.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, i * degreePerItem);

            Image temp = obj.transform.GetChild(0).GetComponent<Image>();
            circler.Add(temp);
            circler[i].color = new Color(circler[i].color.r, circler[i].color.g, circler[i].color.b, 0f);
        }
        
    }

    bool isReversed = false;

    public void InitializeLoading(int Counter , UnityAction after = null) {
        if (countCircle == circler.Count)
        {
            for (int i = 0; i < countCircle; i++)
            {
                circler[i].color = new Color(circler[i].color.r, circler[i].color.g, circler[i].color.b, 0f);
            }
        }
        if (after != null) {
            eventAfterFinish.AddListener(after);
        }
        
        setCount = Counter;

    }
    public void InitializeLoadingReverse()
    {
        if (countCircle == circler.Count)
        {
            for (int i = 0; i < countCircle; i++)
            {
                circler[i].color = new Color(circler[i].color.r, circler[i].color.g, circler[i].color.b, 0f);
            }
        }
        isReversed = true;
        setCount = 0.1f;
    }

    public void StopLoading() {
        StopLoading(0);
    }
    public void StopLoading(float time)
    {
        Debug.Log("Stopping");
        setCount = time;
        isReversed = false;
        after = true;
    }

    bool after = false;
    // Update is called once per frame
    void FixedUpdate () {

        if (setCount > 0)
        {
            after = true;
            if (isReversed)
            {
                setCount += Time.deltaTime;
            }
            else {
                setCount -= Time.deltaTime;
            }
            

            float percent = setCount - Mathf.Floor(setCount);

            int idx = (int)Mathf.Floor(percent / (1.0f / countCircle));
            idx = (idx + 1) % circler.Count;
            Color c = circler[idx].color;

            c.a = 1f;

            circler[idx].color = c;


            for (int i = 0; i < countCircle; i++)
            {
                Color t = circler[i].color;

                t.a = Mathf.Max(t.a - 0.03f, 0);

                circler[i].color = t;
            }

        }
        else if(after) {
            after = false;
            for (int i = 0; i < countCircle; i++)
            {
                Color t = circler[i].color;

                t.a = Mathf.Max(t.a - 0.03f, 0);
                if (t.a > 0) {
                    after = true;
                }
                circler[i].color = t;
            }
            if (after == false) {
                eventAfterFinish.Invoke();
            }
        }


	}
}
