﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentResourcesManager : ContentHandler
{


    [Header("Production Data")]
    [SerializeField] List<int> limitProduction = new List<int>();
    [SerializeField] int speedProductionPerWorker = 10;
    [Header("UI")]
    [SerializeField] ResourceGroupUIProduction foodGroup = null;
    [SerializeField] ResourceGroupUIProduction woodGroup = null;
    [SerializeField] ResourceGroupUIProduction stoneGroup = null;
    [SerializeField] Text idleWorkerText = null;



    //Worker
    int idleWorker;
    int foodWorker;
    int woodWorker;
    int stoneWorker;

    //Resource Gathered
    int _resourceFood;
    int _resourceWood;
    int _resourceStone;

    int resourceFood {
        get {
            return _resourceFood;
        }
        set {
            _resourceFood = Mathf.Clamp( value , 0 , limitProduction[Account.CurrentAccountData.level - 1] );
            float percentage = Mathf.Clamp(_resourceFood / (limitProduction[Account.CurrentAccountData.level - 1] * 1.0f), 0, 1);
            foodGroup.fillPercentage.fillAmount = percentage;
            if (percentage == 1 && !foodGroup.particle.isPlaying) {
                foodGroup.particle.Play();
            }
        }
    }

    int resourceWood
    {
        get { return _resourceWood; }
        set
        {
            _resourceWood = Mathf.Clamp(value, 0, limitProduction[Account.CurrentAccountData.level - 1]);

            float percentage = Mathf.Clamp(_resourceWood / (limitProduction[Account.CurrentAccountData.level - 1] * 1.0f), 0, 1);
            woodGroup.fillPercentage.fillAmount = percentage;
            if (percentage == 1 && !woodGroup.particle.isPlaying)
            {
                woodGroup.particle.Play();
            }
        }
    }

    int resourceStone {
        get { return _resourceStone; }
        set {
            _resourceStone = Mathf.Clamp(value, 0, limitProduction[Account.CurrentAccountData.level - 1]);

            float percentage = Mathf.Clamp( _resourceStone / (limitProduction[Account.CurrentAccountData.level - 1] * 1.0f), 0, 1);
            stoneGroup.fillPercentage.fillAmount = percentage;
            if (percentage == 1 && !stoneGroup.particle.isPlaying)
            {
                stoneGroup.particle.Play();
            }
        }
    }

    public void setResourceGathered() {

        

        ResourcesProductionData resProduction = Account.CurrentAccountData.production;

        int total = (int)Mathf.Floor((float)(System.DateTime.Now - resProduction.lastCheckpoint).TotalSeconds);
        Debug.Log("Total second : "+total);
        int collectedFood = total * speedProductionPerWorker * resProduction.foodWork;
        int collectedWood = total * speedProductionPerWorker * resProduction.woodWork;
        int collectedStone = total * speedProductionPerWorker * resProduction.stoneWork;
        
        resourceFood = Mathf.Clamp(collectedFood + resProduction.resFood, 0, limitProduction[Account.CurrentAccountData.level - 1]);
        resourceWood = Mathf.Clamp(collectedWood + resProduction.resWood, 0, limitProduction[Account.CurrentAccountData.level - 1]);
        resourceStone = Mathf.Clamp(collectedStone + resProduction.resStone, 0, limitProduction[Account.CurrentAccountData.level - 1]);

        idleWorker = resProduction.idleWork;
        foodWorker = resProduction.foodWork;
        woodWorker = resProduction.woodWork;
        stoneWorker = resProduction.woodWork;

        setIdleWorker(resProduction.idleWork);
        setFoodWorker(resProduction.foodWork);
        setWoodWorker(resProduction.woodWork);
        setStoneWorker(resProduction.stoneWork);

        setLimitProduction();

        InvokeRepeating("ClockUpdate", 0, 1);
    }

    void setFoodWorker(int worker) {
        foodWorker = worker;
        foodGroup.workerCount.text = foodWorker.ToString();
        foodGroup.productionPerHour.text = RoyaleHeroesLibs.intToStringK(worker * speedProductionPerWorker * 3600) + " / h";
        saveUpdateResource();
    }
    void setWoodWorker(int worker) {
        woodWorker = worker;
        woodGroup.workerCount.text = woodWorker.ToString();
        woodGroup.productionPerHour.text = RoyaleHeroesLibs.intToStringK(worker * speedProductionPerWorker * 3600) + " / h";
        saveUpdateResource();
    }
    void setStoneWorker(int worker)
    {
        stoneWorker = worker;
        stoneGroup.workerCount.text = stoneWorker.ToString();
        stoneGroup.productionPerHour.text = RoyaleHeroesLibs.intToStringK(worker * speedProductionPerWorker * 3600) + " / h";
        saveUpdateResource();
    }
    void setIdleWorker(int worker)
    {
        idleWorker = worker;
        idleWorkerText.text = idleWorker.ToString() +" / "+ (Account.CurrentAccountData.level+2).ToString();
    }

    public void editFoodWorker(bool isAdd) {
        if (isAdd) { 
            if (idleWorker <= 0)
            {
                RoyaleHeroesToast.ShowToast("No Worker Available", 1);
                return;
            }
            setIdleWorker(idleWorker - 1);
            setFoodWorker(foodWorker + 1);
            
        }else{
                if (foodWorker <= 0)
                {
                    RoyaleHeroesToast.ShowToast("No more worker available in Food production", 1);
                    return;
                }
                setIdleWorker(idleWorker + 1);
                setFoodWorker(foodWorker - 1);
                
        }
    }
    public void editWoodWorker(bool isAdd)
    {
        if (isAdd)
        {
            if (idleWorker <= 0)
            {
                RoyaleHeroesToast.ShowToast("No Worker Available", 1);
                return;
            }
            setIdleWorker(idleWorker - 1);
            setWoodWorker(woodWorker + 1);
        }
        else { 
                if (woodWorker <= 0)
                {
                    RoyaleHeroesToast.ShowToast("No more worker available in Wood production", 1);
                    return;
                }
                setIdleWorker(idleWorker + 1);
                setWoodWorker(woodWorker - 1);
               
        }
    }
    public void editStoneWorker(bool isAdd)
    {
        if (isAdd)
        {
            if (idleWorker <= 0)
            {
                RoyaleHeroesToast.ShowToast("No Worker Available", 1);
                return;
            }
            setIdleWorker(idleWorker - 1);
            setStoneWorker(stoneWorker + 1);
        }
        else { 
                if (stoneWorker <= 0)
                {
                    RoyaleHeroesToast.ShowToast("No more worker available in Stone production", 1);
                    return;
                }
                setIdleWorker(idleWorker + 1);
                setStoneWorker(stoneWorker - 1);
                
        }
    }

    public void saveUpdateResource() {
        Account acc = Account.CurrentAccountData;
        acc.production.lastCheckpoint = System.DateTime.Now;
        acc.production.resFood = resourceFood;
        acc.production.resWood = resourceWood;
        acc.production.resStone = resourceStone;

        acc.production.foodWork = foodWorker;
        acc.production.woodWork = woodWorker;
        acc.production.stoneWork = stoneWorker;
        acc.production.idleWork = idleWorker;

        RoyaleHeroesLibs.SaveAccountData();

    }

    void setLimitProduction() {
        string texLim = RoyaleHeroesLibs.intToStringK(limitProduction[Account.CurrentAccountData.level - 1]);
        foodGroup.limitProduction.text =texLim;
        woodGroup.limitProduction.text = texLim;
        stoneGroup.limitProduction.text = texLim;

    }

    public void collectFood() {

        Account.addResource(resourceFood, ResourcesMaterialType.Food);
        resourceFood = 0;
        foodGroup.particle.Stop();
        MainMenuManager.mainmenu.UpdateHeader();
        saveUpdateResource();

    }
    public void collectWood() {
        Account.addResource(resourceWood, ResourcesMaterialType.Wood);
        //Account.CurrentAccountData.resources.wood += resourceWood;
        resourceWood = 0;
        woodGroup.particle.Stop();
        MainMenuManager.mainmenu.UpdateHeader();
        saveUpdateResource();

    }
    public void collectStone() {
        Account.addResource(resourceStone, ResourcesMaterialType.Stone);
        //Account.CurrentAccountData.resources.stone += resourceStone;
        resourceStone = 0;
        stoneGroup.particle.Stop();
        MainMenuManager.mainmenu.UpdateHeader();
        saveUpdateResource();

    }

    public void ClockUpdate()
    {

        //Debug.Log(resourceFood);
        resourceFood += foodWorker * speedProductionPerWorker;
        resourceWood += woodWorker * speedProductionPerWorker;
        resourceStone += stoneWorker * speedProductionPerWorker;

    }


}

[System.Serializable]
public class ResourceGroupUIProduction {
    public Text workerCount;
    public Text limitProduction;
    public Text productionPerHour;
    public ParticleSystem particle;
    public Image fillPercentage;
}
