﻿using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ContentBattleManager : ContentHandler
{
    [SerializeField] Image poinPercentageFill = null;
    [SerializeField] Text chapterText = null;
    [SerializeField] Text titleText = null;
    [SerializeField] Text poinText = null;
    [SerializeField] Image tierSymbol = null;
    [SerializeField] List<Sprite> tierSymbols = new List<Sprite>();
    [SerializeField] List<int> tierPoin = new List<int> ();
    [SerializeField] List<string> tierTitle = new List<string>();
    int reachPoin;
    int currentPoin;

    public override void Start()
    {
        base.Start();
    }

    public void SetUpTierLevel(int tierLevel) {

        reachPoin = tierPoin[tierLevel - 1];
        UpdatePlayerPoin();
        titleText.text = tierTitle[tierLevel - 1];
        chapterText.text = "Chapter " + tierLevel;
        SetTierImage(tierLevel);

    }

    public float getChAmount (){
        return poinPercentageFill.fillAmount;
    }

    public void SetUpCurrentPoin(int poin) {
        currentPoin = poin;
        UpdatePlayerPoin();
    }

    void UpdatePlayerPoin()
    {
        poinText.text = currentPoin.ToString() + " / " + reachPoin.ToString();
        poinPercentageFill.fillAmount = Mathf.Clamp( currentPoin / (reachPoin * (1.0f)) , 0 , 1 );
    }

    void SetTierImage(int tierLevel) {
        tierSymbol.sprite = tierSymbols[tierLevel - 1];
    }

    bool checkLineUp()
    {

        string path = "lineUpData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        string json = RoyaleHeroesLibs.loadFileFromName(path);
        if (json == null)
        {
            MainMenuManager.showPopUpMessage("Match making invalid", "Sorry you have to set your formation in tab formation before start playing", false);
            return false;
        }
        else
        {
            BlockData[,] lineUpform = Newtonsoft.Json.JsonConvert.DeserializeObject<BlockData[,]>(json);
            int totalArmy = 0;
            ResourcesMaterial requirement = new ResourcesMaterial();
            foreach (BlockData data in lineUpform) {
                totalArmy += data.getCountArmy();
                requirement += data.getTotalResources();
            }

            if (requirement.food > Account.CurrentAccountData.resources.food)
            {
                MainMenuManager.showPopUpMessage("Match making invalid", "Sorry you dont have enough Food, collect your Food or please re-line up you formation", false);
                return false;
            }
            else if (requirement.wood > Account.CurrentAccountData.resources.wood)
            {
                MainMenuManager.showPopUpMessage("Match making invalid", "Sorry you dont have enough Wood, collect your Wood or please re-line up you formation", false);
                return false;
            }
            else if (requirement.stone > Account.CurrentAccountData.resources.stone) {
                MainMenuManager.showPopUpMessage("Match making invalid", "Sorry you dont have enough Stone, collect your Stone or please re-line up you formation", false);
                return false;
            }
            else if (totalArmy < 100) {
                MainMenuManager.showPopUpMessage("Match making invalid", "Sorry but you didn't set your formation yet  in Military menu", false);
                return false;
            }
            else if (totalArmy < Account.CurrentAccountData.tierLevel * 15000) {
                MainMenuManager.showPopUpMessage("Confirm Battle", "Your formation is less than " + Account.CurrentAccountData.tierLevel * 15000 + " army, are you sure to continue?", true, (bool confirm) => {
                    if (confirm)
                    {
                        PhotonNetwork.JoinLobby();
                    }
                });
                return false;
            }
            else
            {
                return true;
            }
            

        }

        

    }

    public void startMatchMaking() {

        bool confirm  = checkLineUp();
        if (confirm) {
            PhotonNetwork.JoinLobby();
        }
        
    }
   

}
