﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfferHolderItem : MonoBehaviour
{
    ContentTavernManager.GachaData data;

    [Header("prefabs")]
    [SerializeField] HeroesListItemV2 prefabHero = null;

    [Header("UI")]
    [SerializeField] Text title = null;
    [SerializeField] Text description = null;
    [SerializeField] Text price = null;
    [SerializeField] Transform contentListHeroHolder = null;
    [SerializeField] ScorllViewSnap scrollViewHero = null;
    [SerializeField] Button btnBuy = null;
    public RoyaleHeroesNestedScrollHorizontal horizontalNested;

    List<string> heroesName = new List<string>();

    string desc = "";

    public void addListener(UnityEngine.Events.UnityAction action) {
        btnBuy.onClick.AddListener(action);
    }

    public void initOffer(ContentTavernManager.GachaData data) {

        this.data = data;

        title.text = data.title;

        desc = data.desc;

        price.text = data.price.ToString();

        scrollViewHero.onSelectedNewItemDelegate = newItem;

        foreach (ContentTavernManager.GachaData.GachaProbability prob in data.heroes)
        {
            loadListHeroes(prob.heroName);
        }

        //setDesc(RoyaleHeroesLibs.getHeroesData(heroesName[0]).desc);
        newItem(0);
    }

    public void newItem(int idx)
    {
        Debug.Log(heroesName[idx]);

        setDesc(heroesName[idx], RoyaleHeroesLibs.getHeroesData(heroesName[idx]).desc);

    }

    public void setDesc(string heroName , string val) {
        description.text = "Description\n" + desc +"\n\n\nHeroes Data : "+heroName+"\n"+ val;
    }

    // Update is called once per frame
    void loadListHeroes(string name)
    {

        heroesName.Add(name);

        HeroesListItemV2 temp = Instantiate(prefabHero, contentListHeroHolder);
        temp.heroName = name;
        temp.icon = RoyaleHeroesLibs.getHeroesData(name).icon;
    }
}
