﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpGachaHandler : MonoBehaviour
{
    ContentTavernManager.GachaData data;

    [SerializeField] Text heroName=null;
    [SerializeField] Text className = null;
    [SerializeField] Image heroImage = null;
    [SerializeField] Animator animator = null;

    [SerializeField] AudioSource backSound = null;

    float targetVolume = -1;
    // Start is called before the first frame update
    public void Init(ContentTavernManager.GachaData data)
    {
        this.data = data;
        HeroesDataScriptableObject reward = getReward();
        Debug.Log("You got "+reward.heroName);

        resetView();
        setCard(reward);
        giveReward(reward);
        //playAnimation();
        animPLayed = false;

        targetVolume = 0.1f;
        
    }

    public void targetVolumeBack()
    {
        targetVolume = 0.5f;
    }
    void SetVolume() {
        if (targetVolume > backSound.volume) {
            backSound.volume = Mathf.Min(backSound.volume + 0.005f, targetVolume);
        }
        else {
            backSound.volume = Mathf.Max(backSound.volume - 0.01f, targetVolume);
        }
        
    }

    public void startAnimGacha() {

        animator.SetTrigger("start");
    }

    HeroesDataScriptableObject getReward() {


        string name = "";
        float result = Random.Range(0f, 1f);
        for (int i = 0; i < data.heroes.Count; i++) {
            result = result - data.heroes[i].prob;
            if (result <= 0) {
                name = data.heroes[i].heroName;
                break;
            }
        }

        return RoyaleHeroesLibs.getHeroesData(name);
    }
    void resetView() {

    }
    void setCard(HeroesDataScriptableObject reward) {
        heroName.text = reward.heroName;
        string temp = "";
        switch (reward.heroesClass) {
            case HeroesClass.Infantry:
                temp = "Infantry";
                break;
            case HeroesClass.Archer:
                temp = "Archer";
                break;
            case HeroesClass.Cavalary:
                temp = "Cavalary";
                break;
            case HeroesClass.Magician:
                temp = "Magician";
                break;
        }
        className.text = "( " + temp + " )";

        heroImage.sprite = reward.idle;

    }
    void giveReward(HeroesDataScriptableObject reward) {
        Account.addHeroesCard(reward.heroName , reward.heroesClass);
        RoyaleHeroesLibs.SaveAccountData();
    }
    void playAnimation() {
        animPLayed = true;
        animator.Play("Gacha");

        //targetVolume = 5;
    }
    bool animPLayed = false;
    public void close() {
        if (animator.GetBool("isFinish"))
        {
            backSound.volume = 0.5f;
            this.gameObject.SetActive(false);
        }
        else if(!animPLayed){
            playAnimation();
        }
    }

    public void Update()
    {
        if (targetVolume != -1 && targetVolume != backSound.volume) {
            SetVolume();
            if (targetVolume == backSound.volume)
            {
                targetVolume = -1;
            }
        }
    }

}
