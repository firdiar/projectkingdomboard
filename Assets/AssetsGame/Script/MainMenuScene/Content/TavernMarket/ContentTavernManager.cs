﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;

public class ContentTavernManager : ContentHandler
{
    //List<GachaData> gData = new List<GachaData>();
    DatabaseReference dbReffTavern;

    [SerializeField] OfferHolderItem offerPrefabs;
    [SerializeField] Transform contentHolder;
    [SerializeField] UnityEngine.UI.ScrollRect rectScroll;
    [SerializeField] ScorllViewSnap scrollView;
    [SerializeField] PopUpGachaHandler popUp;
    

    public void init() {
        clearConten();
        FirebaseStatusConnectUpdate();
        
    }

   

    void clearConten() {
        foreach (Transform child in contentHolder) {
            Destroy(child.gameObject);
        }
    }
    void loadContent(GachaData gd) {
        
        OfferHolderItem offer = Instantiate(offerPrefabs, contentHolder);
        offer.initOffer(gd);
        offer.horizontalNested.mOutterScroll = rectScroll;
        offer.horizontalNested.mOutterSnap = scrollView;
        offer.addListener(() => { onBuy(gd); });
        
    }



    void FirebaseStatusConnectUpdate()
    {

        dbReffTavern = FirebaseDatabase.DefaultInstance.GetReference("/tavern/");
        dbReffTavern.ChildAdded += handleChildAdded;
    }

    void handleChildAdded(object sender, ChildChangedEventArgs args)
    {
        //Debug.Log("Child added");
        DataSnapshot snap = args.Snapshot;

        IDictionary dictionary = (IDictionary)snap.Value;
        //Debug.Log(dictionary["title"].ToString());
        GachaData temp = new GachaData();
        temp.title = dictionary["title"].ToString();
        temp.price = int.Parse(dictionary["price"].ToString());
        temp.desc = dictionary["desc"].ToString();

        foreach (DataSnapshot gp in snap.Child("heroes").Children) {
            IDictionary dict = (IDictionary)gp.Value;
            temp.addHeroes(dict["name"].ToString(), float.Parse(dict["prob"].ToString()));
        }
       // gData.Add(temp);

        loadContent(temp);
        if (scrollView != null)
        {
            scrollView.Init();
        }
        

    }
    public void onBuy(GachaData data) {
        Debug.Log(data.title);

        if (Account.CurrentAccountData.resources.gold < data.price) {
            RoyaleHeroesToast.ShowToast("Sorry not enough gold", 2);
            return;
        }

        MainMenuManager.showPopUpMessage("Confirmation", "Are you sure wanna buy this item ?", true , (bool res)=>{
            if (res)
            {
                buyItems(data);
            }
            else {
                RoyaleHeroesToast.ShowToast("Purchase canceled",2);
            }
        });

        
    }

    public void reInit() {
        scrollView.Init();
    }

    public void buyItems(GachaData data) {
        Debug.Log(Account.CurrentAccountData.resources.gold);
        Account.addResource(-data.price, ResourcesMaterialType.Gold);
        MainMenuManager.mainmenu.UpdateHeader();


        Debug.Log(Account.CurrentAccountData.resources.gold);
        popUp.gameObject.SetActive(true);
        popUp.Init(data);
    }

    public class GachaData
    {

        public class GachaProbability
        {
            public string heroName;
            public float prob;
            public GachaProbability(string name, float prob) {
                heroName = name;
                this.prob = prob;
            }
        }

        public void addHeroes(string name, float prob) {
             heroes.Add(new GachaProbability(name, prob));
        }

        public List<GachaProbability> heroes = new List<GachaProbability>();
        public string title;
        public int price;
        public string desc;


    }
}
