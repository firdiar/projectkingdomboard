﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviourPunCallbacks
{

    public static MainMenuManager mainmenu;

    Account userData;

    [Header("Network")]
    [SerializeField]PhotonView pv;

    [Header("Contents Manager")]
    [SerializeField] ContentTavernManager tavernManager = null;
    [SerializeField] ContentMilitaryManager militaryManager = null;
    [SerializeField] ContentBattleManager battleManager = null;
    [SerializeField] ContentResourcesManager resourcesManager = null;
    [SerializeField] ContentFriendsManager friendsManager = null;
    [SerializeField] ProfileManager profileManager = null;

    [Header("Header Manager")]
    [SerializeField] HeaderManager headerManager = null;

    [Header("Pop Up")]
    [SerializeField] MessagePopUp message = null;
    [SerializeField] GameObject matchMaking = null;
    [SerializeField] LoadingCircleScript matchMakingLoading = null;
    [SerializeField] Text matchMakingTip = null;
    [SerializeField] Text matchMakingStat = null;
    [SerializeField] List<string> tip = new List<string>();

    ExperienceData userLevelData;

    void Start()
    {

        RoyaleHeroesLoading.hideLayerLoading();

        if (MainMenuManager.mainmenu != null)
        {
            Destroy(mainmenu);
        }
        MainMenuManager.mainmenu = this;

        if (PlayerPrefs.GetInt("isNewPlayer" , 0 ) == 0){
            PlayerPrefs.SetInt("isNewPlayer", 1);
            MainMenuManager.showPopUpMessage("Welcome", "Welcome new commander to Royale Heroes\nOnly you can help indonesia to become independence, Get rid the aggresor!!", false);
        }

        InvokeRepeating("FirebaseStatusConnectUpdate" , 0 , 270);

        
        

        RoyaleHeroesToast.ShowToast("Welcome To Royale Heroes", 2);
        userData = Account.CurrentAccountData;
        userLevelData = Resources.Load<ExperienceData>("Experience/PlayerLevel");

        InitializeManager();
        //PhotonNetwork.
        
        Debug.Log("Player connected : "+PhotonNetwork.IsConnected);
        if (!PhotonNetwork.IsConnected)
        {
            Debug.Log("Reconnecting");
            PhotonNetwork.ConnectUsingSettings();
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            MainMenuManager.showPopUpMessage("Quit", "Are you sure want to quit from game?", true , (bool res)=> {
                if (res) {
                    Application.Quit();
                }
            } );
        }
        
    }

    public void backToLoginScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoginScene");
    }

    void moveToGameScene() {
        
        pv.RPC("moveSync", RpcTarget.All);

        //UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");

    }

    [PunRPC]
    public void masterMove() {

        Debug.Log("Moving Scene!");

        UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
    }

    [PunRPC]
    public void moveSync() {

        Debug.Log("Moving Sync");

        if (!PhotonNetwork.IsMasterClient)
        {
            RoyaleHeroesLoading.openLayerLoading(() => {
                pv.RPC("masterMove", RpcTarget.MasterClient);
            });
        }
        else {
            
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                RoyaleHeroesLoading.openLayerLoading( ()=> masterMove());
            }
            else
            {
#endif
                RoyaleHeroesLoading.openLayerLoading();
#if UNITY_EDITOR
            }
#endif
        }
    }


    public void LeaveRoom() {
        CancelInvoke("moveToGameScene");
        if(PhotonNetwork.InRoom)
            PhotonNetwork.LeaveRoom();
    }
    public void LeaveLobby()
    {
        if(PhotonNetwork.InLobby)
            PhotonNetwork.LeaveLobby();
    }
    #region MonoBehaviourPunCallbacks Callbacks

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reason {0}", cause);
        MainMenuManager.showPopUpMessage("Disconnected", "Sorry you're disconnected from server because " + cause, false, (bool answer)=> { backToLoginScene(); });
    }

    public void logout() {

        //Debug.Log("Trying to log out : "+ Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId);

        //Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();

        Debug.Log("Trying to log out : " + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId);

        RoyaleHeroesLoading.openLayerLoading(() => {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LogoutScene");
        });

    }


   public override void OnJoinedLobby()
   {
        base.OnJoinedLobby();
        Debug.Log("match making please wait");
        RoyaleHeroesToast.ShowToast("Find enemy please wait",2);

        matchMaking.SetActive(true);
        matchMakingLoading.InitializeLoadingReverse();
        matchMakingTip.text = "Tips : " + tip[Random.RandomRange(0, tip.Count)];
        matchMakingStat.text = "Please Wait";

   }
    public override void OnLeftLobby()
    {
        base.OnLeftLobby();

        Debug.Log("match making canceled");
        if (Account.CurrentAccountData.tierLevel == 1)
        {
            CancelInvoke("moveToGameScene");
        }
    }

    public override void OnCreatedRoom()
   {
        base.OnCreatedRoom();
        Debug.Log("You create a room");
        if (!PhotonNetwork.CurrentRoom.Name.Contains("|")) {

            //return;
        }

        //if (Account.CurrentAccountData.tierLevel == 1)
        //{
            //Invoke("LeaveRoom", 5); //karna test ntar diganti ya
            //Invoke("moveToGameScene", 10 );
        //}
       PhotonNetwork.AutomaticallySyncScene = true;
   }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("player entered room");
        //if (PhotonNetwork.IsMasterClient) {
        moveToGameScene();
        //}
        
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("you joined room : "+PhotonNetwork.CurrentRoom.PlayerCount);
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
   {
       base.OnRoomListUpdate(roomList);
        Debug.Log("Room List Update");
        for(int i = 0; i < roomList.Count; i++) { 
            string[] split = roomList[i].Name.Split('|');
            if (split[0] == Account.CurrentAccountData.tierLevel.ToString()) {
                PhotonNetwork.JoinRoom(roomList[i].Name);
                return;
            }
        }
           

       
        Debug.Log("No room found");
        RoomOptions option = new RoomOptions();
        option.MaxPlayers = 2;
        string uniqID = RoyaleHeroesLibs.RandomString(10);
        PhotonNetwork.CreateRoom(Account.CurrentAccountData.tierLevel+"|"+uniqID, option);
    }

    #endregion

    public static void showPopUpMessage(string caption, string content, bool isConfirmCancelType, MessagePopUp.OnFinishDelegate eventDelegate = null) {
        Debug.Log("pop up message called");
        if (mainmenu.message == null)
            return;

        mainmenu.message.gameObject.SetActive(true);
        mainmenu.message.initMessage(caption,content,isConfirmCancelType,eventDelegate);
    }

    

    void FirebaseStatusConnectUpdate() {

        string path = "/account/" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + "/connected";

        
        Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(path).SetValueAsync(System.DateTime.UtcNow.ToString(RoyaleHeroesLibs.getCultureInfo()));

    }

    void InitializeManager() {
        //Initialize Header Manager
        UpdateHeader();

        //Initialize Battle Manager
        UpdateBattle();

        //Initialize Resource Manager
        UpdateResources();


        // dummy
        militaryManager.dummy();

        //initialize friendsManager
        friendsManager.init();

        //initialize tavernManager
        tavernManager.init();
    }

    public void UpdateHeader()
    {
        headerManager.setNickName(userData.nickName);
        headerManager.setLevelUpExp(userLevelData.listLevel[userData.level - 1].experienceTarget);
        headerManager.setCurrentExp(userData.exp);
        headerManager.setLevelUser(userData.level);
        headerManager.setFood(userData.resources.food);
        headerManager.setWood(userData.resources.wood);
        headerManager.setStone(userData.resources.stone);
        headerManager.setGold(userData.resources.gold);
    }

    public void UpdateBattle()
    {
        battleManager.SetUpTierLevel(userData.tierLevel);
        battleManager.SetUpCurrentPoin(userData.tierPoin);
    }

    public void showUserProfile() {
        profileManager.init(headerManager.getXpAmount(), battleManager.getChAmount());
        profileManager.popIn();
    }

    public void UpdateResources()
    {
        resourcesManager.setResourceGathered();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        Debug.Log("Application Paused");
    }



}
