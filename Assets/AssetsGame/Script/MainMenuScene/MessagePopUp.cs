﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessagePopUp : MonoBehaviour
{
    [SerializeField] Text caption;
    [SerializeField] Text content;
    [SerializeField] GameObject confirmCancelHolder;
    [SerializeField] GameObject okHolder;

    public delegate void OnFinishDelegate(bool isOk);
    public OnFinishDelegate onFinish = null;

    public void initMessage(string caption , string content , bool isConfirmCancelType , OnFinishDelegate eventDelegate = null) {

        this.caption.text = caption;
        this.content.text = content;
        confirmCancelHolder.SetActive(isConfirmCancelType);
        okHolder.SetActive(!isConfirmCancelType);
        onFinish = eventDelegate;

    }

    public void runEvent(bool isOk) {
        if (onFinish != null)
        {
            onFinish(isOk);
        }

    }


}
