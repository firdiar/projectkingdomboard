﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPageHandler : MonoBehaviour
{

    [Header("UI")]
    [SerializeField] ScorllViewSnap scrollView = null;
    [SerializeField] Text position = null;
    [SerializeField] Text title = null;
    [SerializeField] Text dialogBox = null;
    [SerializeField] Text speakerName = null;
    [SerializeField] Image speakerImage = null;

    [Header("Content")]
    [SerializeField] Transform contentParent = null;
    [SerializeField] Image prefabs = null;

    TutorialSectionScriptableObject currentSection = null;

    int pointer = 0;
    int currentIndex = 0;
    bool isDialogFinish = true;
    Coroutine runningDialogue = null;

    private void Start()
    {
        scrollView.onSelectedNewItemDelegate = pageChanged;
    }

    public void init(string itemName) {
        currentSection = Resources.Load<TutorialSectionScriptableObject>("Tutorial/" + itemName); //+ itemName);d

        title.text = currentSection.title;
        speakerName.text = currentSection.speakerName;
        speakerImage.sprite = currentSection.speakerImage;
        position.text = "1 / " + totalItem().ToString();

        foreach (Transform t in contentParent) {
            Destroy(t.gameObject);
        }

        foreach (ImageAndDialog iad in currentSection.imageAndDialogue) {
            Image temp = Instantiate(prefabs, contentParent);
            temp.sprite = iad.image;
        }

        Invoke("scrollViewInit", 0.2f);

        pointer = 0;
        currentIndex = 0;
        startDialogue();
    }

    public void scrollViewInit()
    {
        scrollView.Init();
    }

    public void pageChanged(int index) {

        position.text = (index+1) + " / " + totalItem();
        pointer = 0;
        currentIndex = index;
        startDialogue();

    }
    public void nextOrFinishDialog() {

        if (currentIndex == totalItem() - 1 && pointer == currentSection.imageAndDialogue[currentIndex].dialogue.Count) {
            return;
        }

        if (isDialogFinish)
        {
            pointer++;
            if (pointer == currentSection.imageAndDialogue[currentIndex].dialogue.Count)
            {
                next();
            }
            else {
                startDialogue();
            }
        }
        else {
            if (runningDialogue != null)
            {
                StopCoroutine(runningDialogue);
            }
            isDialogFinish = true;
            dialogBox.text = getDialogue(currentIndex, pointer);
        }
    }

    void startDialogue() {
        if (runningDialogue != null)
        {
            StopCoroutine(runningDialogue);
        }
        string dialog = getDialogue(currentIndex, pointer);
        Debug.Log("Next Text : " + dialog +" - "+ currentIndex);
        runningDialogue = StartCoroutine(runDialog(dialog));
    }

    string getDialogue(int index , int pointer) {
        return currentSection.imageAndDialogue[index].dialogue[pointer];
    }

    IEnumerator runDialog(string dialogue) {
        isDialogFinish = false;
        int progress = 0;
        while (progress != dialogue.Length) {
            progress = Mathf.Min(progress+1 , dialogue.Length);
            //string temp = 
            dialogBox.text = dialogue.Substring(0, progress);
            //Debug.Log("Dialog : " + temp);
            yield return new WaitForSeconds(0.1f);
        }

        isDialogFinish = true;
        yield return null;
    }


    int currentPosition() {
        return scrollView.getCurrentPosition();
    }

    int totalItem() {
        if (currentSection == null) {
            return -10;
        }
        return currentSection.imageAndDialogue.Count;
    }

    public void next() {
        int temp = currentPosition()+1;
        Debug.Log(temp+" - "+ totalItem());
        if (temp >= 0 && temp < totalItem()) {
            scrollView.setPosition(temp);
            //position.text = temp + " / " + totalItem();
        }
        
    }
    public void prev() {
        int temp = currentPosition()-1;
        if (temp >= 0 && temp < totalItem())
        {
            scrollView.setPosition(temp);
            //position.text = temp + " / " + totalItem();
        }
    }
}
