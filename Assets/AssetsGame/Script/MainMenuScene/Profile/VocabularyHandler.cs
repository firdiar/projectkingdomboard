﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VocabularyHandler : MonoBehaviour
{
    [Header("Data")]
    [SerializeField]TextAsset dataJson = null;

    [Header("UI")]
    [SerializeField] Transform Content = null;
    [SerializeField] ItemExplanationHandler prefabs = null;
    [SerializeField] Text titleText = null;
    [SerializeField] Text descriptionText = null;

    [Header("Other")]
    [SerializeField] Animation anim;
    [SerializeField] GameObject choose;
    [SerializeField] GameObject explan;

    bool isShowing = false;
    
    // Start is called before the first frame update
    void Start()
    {




        VocabDataGroup[] data = Newtonsoft.Json.JsonConvert.DeserializeObject<VocabDataGroup[]>(dataJson.text);

        foreach (VocabDataGroup vdg in data) {
            //Debug.Log(vdg.heading);
            ItemExplanationHandler temp = Instantiate(prefabs, Content);
            temp.heading = vdg.heading;
            foreach (VocabData vd in vdg.data) {
                temp.addChild(vd.tittle, () => showDesc(vd.tittle, vd.explanation));
            }
        }
    }

    public void showDesc( string title , string exp)
    {
        descriptionText.text = exp;
        titleText.text = title;
        //anim.Play("VocabularyExplanation");
        explan.SetActive(true);
        choose.SetActive(false);

        isShowing = true;
    }

    public void back() {
        if (isShowing)
        {
            explan.SetActive(false);
            choose.SetActive(true);
            isShowing = false;
        }
        else {
            anim.Play("VocabularyExplanationToProfile");
        }
    }


}

public class VocabDataGroup {

    public string heading = "";

    public List<VocabData> data = new List<VocabData>();

}

public class VocabData {
    public string tittle;
    public string explanation;
}
