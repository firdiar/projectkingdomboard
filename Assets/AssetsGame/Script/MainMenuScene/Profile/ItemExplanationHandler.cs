﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemExplanationHandler : MonoBehaviour
{
    public string heading {
        get {
            return headingText.text;
        }
        set {
            headingText.text = value;
        }
    }
    [SerializeField] VerticalLayoutGroup vertContent = null;
    [SerializeField] Text headingText = null;
    [SerializeField] Button childContent = null;
    [SerializeField] VerticalLayoutGroup vert = null;

    int minBase = -120;
    float min = -240;
    int max = 10;

    bool isShowing = false;

    public void addChild(string title, UnityEngine.Events.UnityAction action) {

        Button temp = Instantiate(childContent, vertContent.transform);
        temp.transform.GetChild(0).GetComponent<Text>().text = title;
        temp.onClick.AddListener(action);
        if (vertContent.transform.childCount > 1)
        {
            min = (120 + (120.0f / (vertContent.transform.childCount - 1)))*-1;
        }
        vertContent.spacing = min;
    }

    Coroutine task = null;

    public void showAndHide() {
        isShowing = !isShowing;
        float target = min;//-240;
        if (isShowing)
        {
            target = max;
        }

        if (task != null) {
            StopCoroutine(task);
        }

        task = StartCoroutine(showAndHideContent(target));
        /*for (int i = 1; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(isShowing);
        }*/

        //Invoke("changeSpacing", 0.02f);
        

    }

    IEnumerator showAndHideContent(float target) {
        
        int childCount = vertContent.transform.childCount;
        float speed = 0.03f / childCount; //Mathf.Abs(vertContent.spacing - target) * 0.0001f;
        float progress = 0;
        while (vertContent.spacing != target) {
            progress += speed;//0.01f;
            vertContent.spacing = Mathf.Lerp(vertContent.spacing, target, progress);
            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }

    public void changeSpacing() {
        vert.spacing += 0.001f;
    }

}
