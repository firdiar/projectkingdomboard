﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHandler : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] TextAsset dataJson = null;

    [Header("Content")]
    [SerializeField]GameObject chooseContent = null;
    [SerializeField]GameObject mainContent = null;
    [SerializeField] TutorialPageHandler pageTutorial = null;

    [Header("UI Choose")]
    [SerializeField] Transform contentParent = null;
    [SerializeField] ItemExplanationHandler prefabs = null;

    [Header("Other")]
    [SerializeField] Animation anim = null;
    

    private void Start()
    {
        TutorialDataGroup[] data = Newtonsoft.Json.JsonConvert.DeserializeObject<TutorialDataGroup[]>(dataJson.text);

        foreach (TutorialDataGroup tdg in data)
        {
            //Debug.Log(vdg.heading);
            ItemExplanationHandler temp = Instantiate(prefabs, contentParent);
            temp.heading = tdg.heading;
            foreach (TutorialData td in tdg.data)
            {
                temp.addChild(td.tittle, () => explainTutorial(td.tittle, td.itemName));
            }
        }

    }

    public void backToProfile() {
        anim.Play("TutorialsToProfile");
    }

    public void explainTutorial(string tittle, string itemName) {
        chooseContent.SetActive(false);
        mainContent.SetActive(true);

        pageTutorial.init(itemName);

    }

    public void backToChoose()
    {
        chooseContent.SetActive(true);
        mainContent.SetActive(false);
    }


}

public class TutorialDataGroup
{

    public string heading = "";

    public List<TutorialData> data = new List<TutorialData>();

}

public class TutorialData
{
    public string tittle;
    public string itemName;
}

