﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileManager : MonoBehaviour
{

    [SerializeField]Animation anim = null;
    [SerializeField] Animation animPageChanger = null;

    [Header("UI")]
    [SerializeField] Text username = null;
    [SerializeField] Text lv = null;
    [SerializeField] Text xpLv = null;
    [SerializeField] Image xp = null;
    [SerializeField] Text chLv = null;
    [SerializeField] Image ch = null;

    [Header("Content")]
    [SerializeField] Transform mainProfile = null;
    [SerializeField] Transform vocabularyExp = null;

    public void popIn() {
        anim.Play("PopIn");
    }
    public void popOut() {
        anim.Play("PopOut");
    }

    public void init(float xpAmount , float chAmount) {
        username.text = Account.CurrentAccountData.nickName;
        lv.text = Account.CurrentAccountData.level.ToString();

        xpLv.text = Account.CurrentAccountData.level.ToString();
        xp.fillAmount = xpAmount;
        chLv.text = Account.CurrentAccountData.tierLevel.ToString();
        ch.fillAmount = chAmount;
    }

    public void showVocabularyExp() {
        animPageChanger.Play("VocabularyExplanation");
    }

    public void showTutorialExp()
    {
        animPageChanger.Play("Tutorials");
    }

}
