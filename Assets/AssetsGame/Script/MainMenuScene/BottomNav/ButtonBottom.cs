﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBottom : MonoBehaviour {

    private static ButtonBottom _currentActive;
    public static ButtonBottom currentActive {
        get
        {
            return _currentActive;
        }
        set {
            if (_currentActive != null) {

                if (_currentActive.buttonIndex < value.buttonIndex)
                {
                    _currentActive.content.SetLeft();
                    value.content.SetMain(0);
                }
                else
                {
                    _currentActive.content.SetRight();
                    value.content.SetMain(1);
                }

                _currentActive.DeactiveButton();

            }

            _currentActive = value;
            _currentActive.ActivateButton();

            //RoyaleHeroesToast.ShowToast("Welcome To Royale Heroes", 2);
        }
    }
    private static ButtonHolder _holder;
    public static ButtonHolder holder {
        get {
            return _holder;
        }
        set {
            _holder = value;
            if (currentActive != null) {
                _holder.target = currentActive;
            }
            
        }
    }


    public bool defaultActive = false;
    public int buttonIndex;
    public ContentHandler content;

    
    [SerializeField] Sprite spriteActive = null;
    [SerializeField] Sprite spriteUnactive = null;
    [SerializeField][Range(1,2)] float scalingFactor =1;
    [SerializeField] float delay = 0;


    Image img = null;
    RectTransform rectTransnform = null;
    Vector2 defaultSize = Vector2.zero;
    float currentScale = 1;


    // Use this for initialization
    void Start () {

        rectTransnform = this.GetComponent<RectTransform>();

        defaultSize.x = rectTransnform.sizeDelta.x;
        defaultSize.y = rectTransnform.sizeDelta.y;

        img = this.GetComponent<Image>();

        if (defaultActive) {
            setToCurrentButton();
        }
	}

    public void setToCurrentButton() {
        if (ButtonBottom.holder != null)
        {
            if (ButtonBottom.holder.target == this)
            {
                return;
            }
            
        }
        
        currentActive = this;
    }

    Coroutine activeCoroutine;

    void stopActiveCoroutine() {
        running = false;
        if (activeCoroutine != null) {
            running = false;
            StopCoroutine(activeCoroutine);
        }
    }


    public void ActivateButton() {
        if (ButtonBottom.holder != null) {
            
            ButtonBottom.holder.target = this;
        }
        
        stopActiveCoroutine();
        currentScale = 1f;
        activeCoroutine = StartCoroutine(scale(scalingFactor , true));
    }

    public void DeactiveButton() {
        stopActiveCoroutine();
        activeCoroutine = StartCoroutine(scale(1 , false));
    }

    private void Update()
    {
        if (running)
        {
            //Debug.Log("Running!");
            if (targetScale > currentScale)
            {
                currentScale = Mathf.Min(currentScale + 0.02f, targetScale);
            }
            else
            {
                currentScale = Mathf.Max(currentScale - 0.02f, targetScale);
            }

            rectTransnform.sizeDelta = defaultSize * currentScale;

            float progress = (currentScale - tempScale) / (targetScale - tempScale);

            content.MoveContent(progress);
            if (progress == 1) {
                running = false;
            }
        }

    }
    bool running = false;
    float targetScale = 0;
    float tempScale = 0;
    IEnumerator scale(float scale , bool isActive) {

        

        if (isActive)
        {
            img.sprite = spriteActive;
        }
        else
        {
            img.sprite = spriteUnactive;
        }

        tempScale = currentScale;
        targetScale = scale;
        running = true;
        /*
        while (scale != currentScale) {

            if (scale > currentScale)
            {
                currentScale = Mathf.Min(currentScale + 0.02f,scale);
            }
            else
            {
                currentScale = Mathf.Max(currentScale - 0.02f, scale);
            }

            rectTransnform.sizeDelta = defaultSize * currentScale;

            float progress =  (currentScale-tempScale)/(scale-tempScale);

            content.MoveContent(progress);

            yield return null;
            yield return new WaitForSeconds(delay);
        }
        */
        yield return null;
        activeCoroutine = null;


    }
	
}
