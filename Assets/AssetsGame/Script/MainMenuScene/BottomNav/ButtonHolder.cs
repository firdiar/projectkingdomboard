﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHolder : MonoBehaviour {

    private ButtonBottom _target;
    public ButtonBottom target {
        get { return _target;  }
        set {
            _target = value;
            stopActiveCoroutine();
            activeCoroutine =  StartCoroutine( Move() );
        }
    }

    Coroutine activeCoroutine;

    void stopActiveCoroutine()
    {

        if (activeCoroutine != null)
        {
            StopCoroutine(activeCoroutine);
        }
    }

    // Use this for initialization
    void Start () {
        ButtonBottom.holder = this;
	}

    IEnumerator Move()
    {
        float lerp = 0;
        while (this.transform.position != target.transform.position) {
            lerp = Mathf.Min(lerp + 0.1f, 1);
            this.transform.position = Vector3.Lerp(this.transform.position, target.transform.position, lerp);
            yield return new WaitForSeconds(0.01f);
        }

        //target = null;

        //yield return null;
    }
}
