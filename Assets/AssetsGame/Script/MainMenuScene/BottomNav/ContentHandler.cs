﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentHandler : MonoBehaviour
{


    [System.Serializable]enum ContentPosition { Main , Left , Right }
    public bool test;
    [SerializeField]ContentPosition current;
    ContentPosition target;
    Vector2 startPosition;
    Vector2 finishPosition;
    RectTransform rectTransform;

    public virtual void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public virtual void reCallStart() {
    }

    public void SetMain (int origin) {

        switch (origin) {
            case 0:
                current = ContentPosition.Right;
                break;
            case 1:
                current = ContentPosition.Left;
                break;
        }
        reCallStart();
        target = ContentPosition.Main;

        SetStartAndFinishPosition();
        

    }
    public void SetLeft() {
        target = ContentPosition.Left;
        SetStartAndFinishPosition();
    }
    public void SetRight() {
        target = ContentPosition.Right;
        SetStartAndFinishPosition();
    }

    void SetStartAndFinishPosition()
    {
        startPosition = TranslatePosition(current);
        finishPosition = TranslatePosition(target);
        
        rectTransform.localPosition = startPosition;

        //this.gameObject.SetActive(false);
        //rectTransform.localPosition = startPosition;
        //this.gameObject.SetActive(true);
    }
    
    Vector2 TranslatePosition(ContentPosition position) {
        Vector2 vector = Vector2.zero;
        switch (position)
        {
            case ContentPosition.Main:
                vector = Vector2.zero;
                break;
            case ContentPosition.Left:
                vector = Vector2.left;
                break;
            case ContentPosition.Right:
                vector = Vector2.right;
                break;
        }

        return new Vector2(Screen.width*4f, 0) * vector;
    }

    public void MoveContent(float progress) {

        
        Vector2 lerpResult = Vector2.Lerp(rectTransform.localPosition, finishPosition, progress); //startPosition + (deltaPosition * progress); 

        if (lerpResult.x < 10000)
        {
            rectTransform.localPosition = lerpResult;
        }
        if (test) {
            Debug.Log(progress);
        }

        if (progress == 1) {
            current = target;
        }

    }
}
