﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderManager : MonoBehaviour {

    [Header("User Profile")]
    [SerializeField] Text nicknameText = null;
    [SerializeField] Text userExp = null;
    [SerializeField] Text userLevel = null;
    [SerializeField] Image userExpPercentage = null;

    int currentExp;
    int lvlUpExp;

    [Header("Resource Profile")]
    [SerializeField] Text food = null;
    [SerializeField] Text wood = null;
    [SerializeField] Text stone = null;
    [SerializeField] Text gold = null;

    public void setCurrentExp(int exp) {
        currentExp = exp;
        UpdatePlayerExp();
    }

    public void setLevelUpExp(int exp)
    {
        lvlUpExp = exp;
        UpdatePlayerExp();
    }

    public void setNickName(string nick) {
        nicknameText.text = nick;
    }

    void UpdatePlayerExp()
    {
        userExp.text = "Xp : "+currentExp.ToString() + " / " + lvlUpExp.ToString();
        userExpPercentage.fillAmount = Mathf.Clamp(currentExp / (lvlUpExp * (1.0f)), 0, 1);
    }

    public float getXpAmount() {
        return userExpPercentage.fillAmount;
    }

    public void setLevelUser(int level) {
        userLevel.text = level.ToString();
    }


    

    public void setFood(int val) {
        food.text = RoyaleHeroesLibs.intToStringK(val);
    }
    public void setWood(int val)
    {
        wood.text = RoyaleHeroesLibs.intToStringK(val);
    }
    public void setStone(int val)
    {
        stone.text = RoyaleHeroesLibs.intToStringK(val);
    }
    public void setGold(int val)
    {
        gold.text = RoyaleHeroesLibs.intToStringK(val);
    }
}
