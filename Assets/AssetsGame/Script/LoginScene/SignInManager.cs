﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Photon.Pun;

public class SignInManager : MonoBehaviourPunCallbacks
{

    [SerializeField] float delayStartAnimation = 0.1f;
    [SerializeField] Animator perkamentLoginAnim = null;
    [SerializeField] Animator perkamentRegisterAnim = null;
    [SerializeField] Animation animationAfterSplash = null;



    // Use this for initialization
    void Start () {
        Debug.Log("Here!");
        //FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://project-kingdomboard.firebaseio.com");
        if (GameObject.Find("CanvasEkstra(Clone)") != null || GameObject.Find("CanvasEkstra") != null) {
            RoyaleHeroesLoading.hideLayerLoading();
        }
        //animationAfterSplash.Play();
        //StartCoroutine(StartAnimation());
	}
    public bool rollDownRegister= false;
    public bool rollUpRegister = false;
    public bool rollDownLogin = false;
    public bool rollUpLogin = false;
    public bool nextScene = false;
    private void Update()
    {
        if (rollDownRegister) {
            rollDownRegister = false;
            RollDownRegister();
        }
        if (rollUpRegister)
        {
            rollUpRegister = false;
            RollUpRegister();
        }
        if (rollDownLogin)
        {
            rollDownLogin = false;
            RollDownLogin();
        }
        if (rollUpLogin)
        {
            rollUpLogin = false;
            RollUpLogin();
        }

        if (nextScene) {
            nextScene = false;
           
            connectingToServer();
            
            //StartCoroutine(gotoMenuScene());
        }
        
    }

    private void connectingToServer()
    {
        Debug.Log("connecting to server");
        PhotonNetwork.ConnectUsingSettings();
    }

    public void SetAccount(Account acc) {
        if (acc == null)
        {
            rollDownRegister = true;
        } else {
            Account.CurrentAccountData = acc;
        }
        
    }

    

    public void RollUpLogin() {
        perkamentLoginAnim.Play("RollUp");
        //retriveUserAccountData();
        //StartCoroutine(gotoMenuScene());
    }
    public void RollDownLogin()
    {
        perkamentLoginAnim.Play("RollDown");
    }

    public void RollUpRegister()
    {
        perkamentRegisterAnim.Play("RollUp");
        //retriveUserAccountData();
        //StartCoroutine(gotoMenuScene());
    }
    public void RollDownRegister()
    {
        perkamentRegisterAnim.Play("RollDown");
    }

    

    IEnumerator gotoMenuScene() {
        Debug.Log("Connected");
        yield return null;
    }

    public override void OnConnectedToMaster()
    {
        
        Debug.Log("OnConnectedToMaster() was called by PUN.");
        //PhotonNetwork.JoinRandomRoom();

        PhotonNetwork.NickName = Account.CurrentAccountData.nickName;

        Debug.Log(PhotonNetwork.CurrentCluster);
        Debug.Log(PhotonNetwork.CloudRegion);

        

        RoyaleHeroesLoading.openLayerLoading(() =>
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
        });
        
    }

    IEnumerator StartAnimation() {
        yield return new WaitForSeconds(delayStartAnimation);
        RollDownLogin();
    }
    void OnApplicationPause(bool pauseStatus)
    {
        Debug.Log("Application Paused");
    }
}
