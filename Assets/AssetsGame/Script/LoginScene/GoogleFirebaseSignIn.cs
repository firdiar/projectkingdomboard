﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Google;
using UnityEngine.UI;
using Firebase.Auth;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


public class GoogleFirebaseSignIn : MonoBehaviour {
    private FirebaseAuth auth;
    private FirebaseUser FBuser;
    FirebaseDatabase database;
    [SerializeField] SignInManager sceneManager = null;
    [SerializeField] GameObject notifLengthChar = null;
    public Text Info;
    public Text Process;

    public InputField inputFieldNickname;

    bool newLogin = false;

    // Start is called before the first frame update
    void Start()
    {
        InitializeFirebase();
    }

    bool editText = false;
    string textProcess = "";
    string textdot = "";
    float time = 0;
    private void Update()
    {
        if (textProcess != "")
        {
            if (time > 1)
            {
                if (textdot.Length < 6)
                {
                    textdot += " .";
                }
                else
                {
                    textdot = "";
                }
                time = 0;
                editText = true;
            }
            else
            {
                time += Time.deltaTime;
            }
            //Debug.Log(time);
        }

        if (editText) {
            editText = false;
            Process.text = textProcess + textdot;
        }
    }

    void setProcessText(string txt) {
        textProcess = txt;
        editText = true;
        if (txt == "") {
            textdot = "";
        }
    }

    void EditorCreateAccount() {
        auth.CreateUserWithEmailAndPasswordAsync("firdi.ansyah20@gmail.com", "diva1226").ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }

    void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Process.text = "Setting up Auth";
        setProcessText("Setting up Auth");
        Info.text = "Initializing";
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://project-kingdomboard.firebaseio.com");
        database = FirebaseDatabase.DefaultInstance;
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //auth.SignOut();
        auth.StateChanged += AuthStateChanged;
        if (auth.CurrentUser == null) {
            sceneManager.rollDownLogin = true;
            setProcessText("Waiting for log in");
        }

        
        //AuthStateChanged(this, null);
    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != null)
        {
            bool signedIn = auth.CurrentUser != null;
            if (!signedIn)
            {
                Debug.Log("Signed out " + FBuser.UserId);
                //Info.text = "Sign out " + FBuser.DisplayName.ToString();
            }
            FBuser = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + FBuser.UserId);
                //Info.text = "Sign in " + FBuser.DisplayName.ToString() ;
                retriveUserAccountData();
            }
        }
        else {
            //Info.text = "Not signed in";
            //Process.text = "";
            //setProcessText("");
            
        }
    }

    public void retriveUserAccountData()
    {
        Debug.Log("retriving data ...");
        //Process.text = "Retrieving Data";
        setProcessText("Retrieving data");
        DatabaseReference reff = database.GetReference("/account/" + FBuser.UserId);

        string token = PlayerPrefs.GetString("userToken","");

        reff.GetValueAsync().ContinueWith(task => {
              Debug.Log("task recived ...");
              if (task.IsFaulted)
              {
                  // Handle the error...
                  Debug.Log("data fault");
                  //Process.text = "";
                  setProcessText("");
            }
              else if (task.IsCompleted)
              {
                  
                  DataSnapshot snapshot = task.Result;
                  if (snapshot.Value == null || snapshot.Child("nickName") ==null)
                  {
                        Debug.Log("New Player Detected");
                        sceneManager.SetAccount(null);
                        //Process.text = "";
                        setProcessText("");
                }
                  else {
                      setProcessText("Connecting to server");
                      IDictionary user = (IDictionary)snapshot.Value;
                      //Debug.Log(user.Keys.Count);
                      Debug.Log("Success Retrive User Data NickName : " + user["nickName"]);
                      Account acc = convertFromSnapshot(snapshot);
                      Debug.Log(((IDictionary)snapshot.Value)["token"].ToString());
                      Debug.Log(token);
                      // sama x newLogin => go false
                      // sama x not newLogin => go false
                      // not sama x newLogin => go false
                      // not sama x not newLogin => back true
                      if (!(token == ((IDictionary)snapshot.Value)["token"].ToString() || newLogin))
                        {
                            newLogin = false;
                            sceneManager.rollDownLogin = true;
                            FirebaseAuth.DefaultInstance.SignOut();
                            Debug.Log("Log out");
                            setProcessText("Retrive data failed");
                        }
                      else
                        {
                            Debug.Log("Log in");
                            setProcessText("Retrive data success");
                            //Debug.Log("Success Retrive User Data NickName : " + user["nickName"]);
                            acc.token = token;
                            sceneManager.SetAccount(acc);

                            //reff.Child("connected").SetValueAsync("true");

                            //Debug.Log("Success Retrive User Data NickName : " + user["nickName"]);
                            auth.StateChanged -= AuthStateChanged;
                            sceneManager.nextScene = true;
                            reff.Child("connected").OnDisconnect().SetValue("false");
                        }
                }
                //Firebase.Database.OnDisconnect onDisconnect = new Firebase.Database.OnDisconnect();
                //onDisconnect.pa
                //reff.Child("connected").OnDisconnect().SetValue("false");
                
            }
          });

    }

    int snapToInt(object obj) {
        return int.Parse(obj.ToString());
    }

    Account convertFromSnapshot(DataSnapshot snapshot) {
        
        
        Account acc = new Account();
        
        IDictionary resources = (IDictionary)snapshot.Child("resources").Value;
        acc.resources = new ResourcesMaterial();
        acc.resources.wood = int.Parse(resources["wood"].ToString());
        acc.resources.stone = int.Parse(resources["stone"].ToString()); 
        acc.resources.food = int.Parse(resources["food"].ToString());
        acc.resources.gold = int.Parse(resources["gold"].ToString());
        Debug.Log("Resource Loaded");

        
        IDictionary nick = (IDictionary)snapshot.Value;
        //acc.token = nick["token"].ToString();
        acc.nickName = nick["nickName"].ToString();
        acc.level = int.Parse( nick["level"].ToString() );
        acc.exp = int.Parse( nick["exp"].ToString() );
        acc.tierLevel = int.Parse(nick["tierLevel"].ToString());
        acc.tierPoin = int.Parse(nick["tierPoin"].ToString());
        acc.connected = true;
        acc.heroes = new Heroes();
        Debug.Log("User Data Loaded");

        DataSnapshot temp;
        
        temp = snapshot.Child("heroes/Archer");
        foreach (DataSnapshot snap in temp.Children) {
            IDictionary heroData = (IDictionary)snap.Value;
            acc.heroes.Archer.Add(new HeroesData((string)heroData["name"], snapToInt( heroData["card"] ), snapToInt( heroData["xp"] ), snapToInt(heroData["level"])));
        }
        Debug.Log("Archer Loaded");
        //Debug.Log("res");
        temp = snapshot.Child("heroes/Infantry");
        foreach (DataSnapshot snap in temp.Children)
        {
            IDictionary heroData = (IDictionary)snap.Value;
            acc.heroes.Infantry.Add(new HeroesData((string)heroData["name"], snapToInt(heroData["card"]), snapToInt(heroData["xp"]), snapToInt(heroData["level"])));
        }
        Debug.Log("Infantry Loaded");

        //Debug.Log("res");
        temp = snapshot.Child("heroes/Cavalary");
        foreach (DataSnapshot snap in temp.Children)
        {
            IDictionary heroData = (IDictionary)snap.Value;
            acc.heroes.Cavalary.Add(new HeroesData((string)heroData["name"], snapToInt(heroData["card"]), snapToInt(heroData["xp"]), snapToInt(heroData["level"])));
        }
        Debug.Log("Cavalary Loaded");
        //Debug.Log("res");

        temp = snapshot.Child("heroes/Magician");
        foreach (DataSnapshot snap in temp.Children)
        {
            IDictionary heroData = (IDictionary)snap.Value;
            acc.heroes.Magician.Add(new HeroesData((string)heroData["name"], snapToInt(heroData["card"]), snapToInt(heroData["xp"]), snapToInt(heroData["level"])));
        }
        Debug.Log("Magician Loaded");

        IDictionary production = (IDictionary)snapshot.Child("production").Value;
        acc.production = new ResourcesProductionData();
        acc.production.foodWork = int.Parse(production["foodWork"].ToString());
        acc.production.woodWork = int.Parse(production["woodWork"].ToString());
        acc.production.stoneWork = int.Parse(production["stoneWork"].ToString());
        acc.production.idleWork = int.Parse(production["idleWork"].ToString());
        
        acc.production.resFood = int.Parse(production["resFood"].ToString());
        acc.production.resWood = int.Parse(production["resWood"].ToString());
        acc.production.resStone = int.Parse(production["resStone"].ToString());
        
        string date = production["lastCheckpoint"].ToString();
        
        acc.production.lastCheckpoint = System.DateTime.Parse(date);
        Debug.Log("Production Loaded");

        return acc;
    }

    public void createNewAccountButton() {
        if (inputFieldNickname.text.Length < 6) {
            notifLengthChar.SetActive(true);
            Debug.Log("Nick Name must contains more than 6 letter");
            return;
        }
        
        SaveNewAccountData(inputFieldNickname.text);
    }

    public void SaveNewAccountData(string nick)
    {
        setProcessText("Creating new account");
        Account acc = Account.generateNewPlayerData(nick); //sceneManager.generateNewPlayerData(nick);
        
        Debug.Log("Saving new player account data");
        //Process.text = "Saving player data";
        setProcessText("Saving player data");
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(acc);
        Debug.Log(json);
        DatabaseReference reff = database.GetReference("/account/" + FBuser.UserId);
        reff.SetRawJsonValueAsync(json).ContinueWith(task =>{
            //Process.text = "";
            setProcessText("");
            Debug.Log(task.IsCompleted && !task.IsFaulted);
            if (task.IsFaulted) {
                Debug.Log("Data fault");

            }
            else if (task.IsCompleted) {
                Debug.Log("Data saved");
                //Process.text = "Finish";
                setProcessText("Connecting to server");
                sceneManager.rollUpRegister = true;
                sceneManager.nextScene = true;
                reff.Child("connected").OnDisconnect().SetValue("false");
            }
            

        });
        sceneManager.SetAccount(acc);
    }

    

    public void googleSignInButton()
    {

        setProcessText("Validating credential");
        Info.text = "Signing in";

        string token = RoyaleHeroesLibs.RandomString(32);
        PlayerPrefs.SetString("userToken", token );
        

#if UNITY_EDITOR
        auth.SignInWithEmailAndPasswordAsync("firdi.ansyah20@gmail.com", "diva1226").ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                EditorCreateAccount();
                return;
            }

            

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            setProcessText("");
            Debug.Log("token : " + token );
            //FirebaseDatabase.DefaultInstance.RootReference
            DatabaseReference reff = database.GetReference("/account/" + newUser.UserId);
            reff.Child("token").SetValueAsync(token);
            newLogin = true;
            //Info.text = "Welcome  " + FBuser.DisplayName.ToString();
            sceneManager.rollUpLogin = true;
            
        });

#elif UNITY_ANDROID

        Info.text = "Processing Loading";
        Debug.Log("Processing Loading");
        GoogleSignIn.Configuration = new GoogleSignInConfiguration
        {
            RequestIdToken = true,
            // Copy this value from the google-service.json file.
            // oauth_client with type == 3
            WebClientId = "534646306591-e44hu8kljk3avjm1iljp0edo2761h0vm.apps.googleusercontent.com"
        };
        Debug.Log("Processing Loading2");
        Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();
        Debug.Log("Processing Loading3");
        Info.text = "Sign in...";
        TaskCompletionSource<FirebaseUser> signInCompleted = new TaskCompletionSource<FirebaseUser>();
        Debug.Log("Processing Loading4");
        signIn.ContinueWith(task =>
        {
            
            Debug.Log("Processing Loading5 "+task.IsCanceled +" "+ task.IsFaulted+" "+ task.IsCompleted);
            if (task.IsCanceled)
            {
                Debug.Log("Processing Loading Cancek");

                signInCompleted.SetCanceled();
                Info.text = "canceled  1 " + FBuser.UserId.ToString();
            }
            else if (task.IsFaulted)
            {
                Debug.Log("Processing Loading Fault");
                Debug.Log(task.Exception.Message);
                Info.text = "is faulted 1 " + FBuser.UserId.ToString();
                signInCompleted.SetException(task.Exception);
            }
            else
            {
                Info.text = "Validate credential";
                Debug.Log("Processing Loading6 "+ task.Result.IdToken);
                Credential credential = GoogleAuthProvider.GetCredential(task.Result.IdToken, null);
                auth.SignInWithCredentialAsync(credential).ContinueWith(authTask =>
                {
                    setProcessText("");
                    Debug.Log("Processing Loading7");
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                        Info.text = "Canceled  " + FBuser.DisplayName.ToString();
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                        Info.text = "Credential invalid  " + FBuser.DisplayName.ToString();
                    }
                    else
                    {
                        Debug.Log("Processing Loading8");
                        signInCompleted.SetResult(authTask.Result);

                        DatabaseReference reff = database.GetReference("/account/" + FBuser.UserId);
                        reff.Child("token").SetValueAsync(token);
                        newLogin = true;
                        //Info.text = "Welcome  " + FBuser.DisplayName.ToString();
                        sceneManager.rollUpLogin = true;
                        
                        
                    }
                });
            }
        });
#endif
    }
}
