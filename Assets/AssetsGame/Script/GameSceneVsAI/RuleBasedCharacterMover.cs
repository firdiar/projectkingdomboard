﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuleBasedCharacterMover : MonoBehaviour , IMoverCharacter
{
    public Vector2 getNextMovementHero(Pion p)
    {
        //throw new System.NotImplementedException();
        return calculate(p);
    }

    // Start is called before the first frame update
    Vector2 calculate(Pion p)
    {

        List<MapTile> landmarkTile = MapGenerator.MAIN.getLandmarkPosition();

        Transform parentEnemy = MapGenerator.MAIN.getEnemyArmyParent(p.transform.parent);

        float minDist = float.PositiveInfinity;
        Vector2 pos = Vector2.one;

        foreach (Transform t in parentEnemy) {

            int actualArmy = t.GetComponent<Pion>().getActualArmy();

            if (actualArmy > p.getActualArmy()) {
                continue;
            }

            float dist = Vector2.Distance(t.position, p.transform.position);
            if ( dist < minDist) {
                minDist = dist;
                pos = t.position;
            }
        }
        foreach (MapTile m in landmarkTile) {

            if (isOwnedByMe(m , p) || (m.isOccupied && m.getCurrentPion().transform.parent == p.transform.parent )) {
                continue;
            }

            float dist = Vector2.Distance(m.transform.position, p.transform.position);
            if (dist < minDist)
            {
                minDist = dist;
                pos = m.transform.position;
            }
        }

        if (pos == Vector2.one) {
            pos = transform.parent.GetChild(Random.Range(0, transform.parent.childCount)).position;
        }

        Vector2 index = MapGenerator.MAIN.getMapTileIndexFromWorld(pos);

        if (index == p.getPos())
        {
            return new Vector2(-1, -1);
        }
        else {
            return handleTileOccupied( p , index );
        }
        
    }

    bool isMasterOwner(Pion p) {

        return p.transform.parent == MapGenerator.MAIN.armyParent.transform;
        
    }

    bool isOwnedByMe(MapTile m , Pion p) {
        if (m.owner == TileOwner.NEUTRAL) {
            return false;
        }
        if (!MapGenerator.MAIN.isOnline)
        {
            return (m.owner == TileOwner.MASTER) == (p.transform.parent == MapGenerator.MAIN.armyParent.transform);
        }
        else
        {
            return (m.owner == TileOwner.MASTER) == Photon.Pun.PhotonNetwork.IsMasterClient;
        }
    }

    Vector2 handleTileOccupied(Pion p , Vector2 target) {

        MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(target);

        if (tile == null) {
            return new Vector2(-1, -1);
        }

        if (!tile.isOccupied) {
            return target;
        }

        
        Vector2 first = p.getCurrentTile();

        int prefer = (int)((target.y - first.y) / Mathf.Max(Mathf.Abs(target.y - first.y), 1)) * -1;

        return MapGenerator.MAIN.aroundNearestNotOccupied(target, prefer);
    }

}
