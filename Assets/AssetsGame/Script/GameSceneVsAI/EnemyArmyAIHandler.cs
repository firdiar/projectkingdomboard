﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmyAIHandler : MonoBehaviour
{
    [SerializeField]MapGenerator generator;
    [SerializeField] PlayerManager enemyPlayerManager;

    StoryModeData pathData;

    void Start()
    {
        //Load Data Here


        Invoke("GenerateArmy", 0.5f);
    }

    T loadObject<T>(string path)
    {

        //string path = "deputyData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        string json = Resources.Load<TextAsset>(path).text; //RoyaleHeroesLibs.loadFileFromName(path);
        Debug.Log(json);
        if (json != null)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
        else
        {
            return default(T);
        }

    }

    void GenerateArmy() {
        
        //TextAsset lineTex = Resources.Load<TextAsset>("StoryVsAiData/line-tes.json");


        BlockData[,] lineUp = loadObject<BlockData[,]>("StoryVsAiData/line-tes");
        HeroesLevelStoryData[] levelData = loadObject<HeroesLevelStoryData[]>("StoryVsAiData/data-tes");

        enemyPlayerManager.playerPion = generator.generateEnemyArmy(lineUp , levelData);

        Debug.Log("GENERATE ENEMY ARMY!");
    }
    
}

public class StoryModeData {
    string pathFormation;
    string pathLevel;
}

public class HeroesLevelStoryData
{
    public string heroName;
    public int heroLevel;
}
