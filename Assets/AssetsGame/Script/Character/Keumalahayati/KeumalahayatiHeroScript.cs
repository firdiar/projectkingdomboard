﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class KeumalahayatiHeroScript : Pion , ILongRanged
{
    [Header("Hero Addon")]
    [SerializeField] KeumalahayatiShipScript ship;
    [SerializeField] Transform arrow;
    [SerializeField] ArrowArcherScript ArrowPrefabs;
    [SerializeField] Transform basicAttack1;
    [SerializeField] GameObject slashAfterArrow = null;
    [SerializeField] GameObject cannonProjectile = null;
    [SerializeField] Transform basicAttack2;
    [SerializeField] float arrowSpeed = 3;
    [SerializeField] int attackTimesToActiveSkill = 6;

    [SerializeField] BuffStatusScriptableObject buffPassive5 = null;

    [SerializeField] AudioClip attackBow = null;
    [SerializeField] AudioClip attackSlash = null;


    int actualAttackTimes = 0;

    Pion currentTarget = null;

    Coroutine arrowCoroutine = null;

    public override void FirstFunction()
    {
        base.FirstFunction();

        //anim.SetTrigger("skill");
    }

    public void PlayAnimShip() {
        //anim.SetBool("skill", false);
        ship.playShipAnim();
    }
    public void StopAnimShip() {
        ship.stopShipAnim();
        isAttacking = false;
        actualAttackTimes = 0;
    }

    public void cannonShooted() {
        Debug.Log("cannonShooted");

        if (!isMine) {
            return;
        }

        if (_targetAttack.Count > 0) {

            if (Random.RandomRange(0f, 1f) < 0.8f)
            {
                Pion target = _targetAttack[Random.RandomRange(0, _targetAttack.Count)];
                currentTarget = target;
                EnemyAttacked3();
            }
        }

    }

    public void EnemyAttacked3()
    {
        if (currentTarget == null)
            return;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("instantiateEffect", RpcTarget.All, true, currentTarget.transform.position + ((Vector3)Random.insideUnitCircle * 1.2f), currentTarget.pv.ViewID);
        }
        else {
            instantiateEffect(true, currentTarget.transform.position + ((Vector3)Random.insideUnitCircle * 1.2f), currentTarget);
        }
        int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.4f);
        int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.4f);

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.1f);
#if UNITY_EDITOR
        
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
            }

#endif
        }
        else
        {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
        }
        //isAttacking = false;
        currentTarget.Shake(0.1f);

    }



    public void bowShooted() {

        if (!isMine)
            return;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("ShootArrows", RpcTarget.All, getRange());
        }
        else {
            ShootArrows(getRange());
        }

    }



    [PunRPC]
    public void ShootArrows(int range) {
        ArrowArcherScript temp = Instantiate(ArrowPrefabs, arrow.position, Quaternion.Euler(0, 0, basicAttack1.eulerAngles.z + 90));
        temp.parent = this;


        float a = basicAttack1.eulerAngles.z / (180.0f / Mathf.PI);
        Vector2 target = new Vector2(-Mathf.Sin(a), Mathf.Cos(a));

        temp.target = (Vector2)transform.position + (target * 4f * (range));

        if (MapGenerator.MAIN.isOnline)
        {
            if (isMine)//jika ini pasukan kita
            {
                temp.armyEnemy = MapGenerator.MAIN.armyEnemyParent.transform;
            }
            else
            {
                temp.armyEnemy = MapGenerator.MAIN.armyParent.transform;
            }
        }
        else
        {
            if (transform.parent == MapGenerator.MAIN.armyParent.transform)//jika ini pasukan kita
            {
                temp.armyEnemy = MapGenerator.MAIN.armyEnemyParent.transform;
            }
            else
            {
                temp.armyEnemy = MapGenerator.MAIN.armyParent.transform;
            }
        }

#if UNITY_EDITOR
        if (MapGenerator.MAIN.isTestMode || (MapGenerator.MAIN.isOnline && PhotonNetwork.CurrentRoom.PlayerCount == 1))
        {
            temp.armyEnemy = MapGenerator.MAIN.armyParent.transform;
        }
#endif


        //if (arrowCoroutine != null)
        //{
        //    StopCoroutine(arrowCoroutine);
        //}
        //arrowCoroutine = StartCoroutine(moveArrow());

    }

    public void playSoundBow() {
        audios.clip = attackBow;
        audios.Play();
    }
    public void playSoundSlash()
    {
        audios.clip = attackSlash;
        audios.Play();
    }


    IEnumerator stepBack() {

        unOccupiedCurrentTile();
        Vector2 start = getCurrentTile();
        MapTile t = null;

        List<Vector2> around = new List<Vector2>();
        around.Add(start + new Vector2(0, 1));//atas
        around.Add(start + new Vector2(0, -1));//bawah
        around.Add(start + new Vector2(1, 0));//kanan
        around.Add(start + new Vector2(-1, 0));//kiri

        for (int i = 0; i < 4; i++) {

            int chose = Random.RandomRange(0, around.Count);
            Vector2 index = around[chose];

            around.RemoveAt(chose);

            t = MapGenerator.MAIN.getMapTileFromIndex(index);

            if (t != null && t.type != TileType.OBSTACLE && !t.isOccupied) {
                break;
            }
        }

        if (t == null)
        {
            yield return null;
        }
        else {
            Vector2 targetNext = t.transform.position;
            while (!isMoving && Vector2.Distance(targetNext, transform.position) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetNext, 0.5f);
                yield return new WaitForSeconds(0.02f);
            }
            yield return null;

        }

        occupiedCurrentTile();

    }
    /*
    Vector3 targetPos;
    IEnumerator moveArrow() {

        //Debug.Log("Current target : "+currentTarget==null);
        arrow.transform.localPosition = new Vector3(0, 3, 0);
        arrow.color = Color.white;
        while (currentTarget != null && Vector2.Distance(targetPos, arrow.transform.position) > 0.7f) {
            float norm = Mathf.Max(getRange() - Vector2.Distance(targetPos, arrow.transform.position), 1);
            arrow.transform.position = Vector2.MoveTowards(arrow.transform.position, targetPos, 1f * norm * arrowSpeed);

            if (currentTarget != null && Vector2.Distance(currentTarget.transform.position, arrow.transform.position) < 0.7f) {
                break;
            }

            yield return new WaitForSeconds(0.02f);
        }
        
        arrow.color = new Color(1 , 1,1 ,0);

        if (!pv.IsMine)
        {
            yield return null;
        }
        else if(currentTarget != null){

            if (Vector2.Distance(targetPos, currentTarget.transform.position) < 1f)
            {

                pv.RPC("instantiateEffect", RpcTarget.All, false, currentTarget.transform.position, currentTarget.pv.ViewID);

                EnemyAttacked();
            }
            else {
                isAttacking = false;
            }

            yield return null;
        }
        
    }*/

    [PunRPC]
    public void instantiateEffect(bool canon , Vector3 pos , int viewID) {
        PhotonView v = PhotonView.Find(viewID);
        if (v != null)
        {
            GameObject temp = Instantiate((canon ? cannonProjectile : slashAfterArrow), pos, Quaternion.Euler(0, 0, (PhotonNetwork.IsMasterClient ? 0 : 180)), PhotonView.Find(viewID).transform);
            Destroy(temp, 0.9f);
        }
    }

    public void instantiateEffect(bool canon, Vector3 pos, Pion p)
    {

        GameObject temp = Instantiate((canon ? cannonProjectile : slashAfterArrow), pos, Quaternion.Euler(0, 0, 0), p.transform);
        Destroy(temp, 0.9f);

    }


    public void EnemyAttacked() {


        //anim.SetBool("attack1", false);

        if (!isMine)
            return;

        actualAttackTimes++;
        int PhyDamage = getPhyDamage();
        int MagDamage = getMagDamage();


        Vector2 Epos =  currentTarget.getCurrentTile();
        Vector2 Mpos = getCurrentTile();
        int rangeX = (int)Mathf.Abs(Epos.x - Mpos.x);
        int rangeY = (int)Mathf.Abs(Epos.y - Mpos.y);

        if ((Mathf.Abs(rangeX) + Mathf.Abs(rangeY)) == 1 || (Mathf.Abs(rangeX) == 1 && 1 == Mathf.Abs(rangeY))) { 
            PhyDamage = Mathf.RoundToInt( 0.4f * PhyDamage );
            MagDamage = Mathf.RoundToInt(0.4f * MagDamage);
        }

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.1f);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
        }
        isAttacking = false;
        currentTarget.Shake(0.1f);

    }

    public void EnemyAttacked2()
    {
        //anim.SetBool("attack2", false);

        if (!isMine)
            return;

        actualAttackTimes++;
        int PhyDamage = getPhyDamage();
        int MagDamage = getMagDamage();

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.1f);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
        }
        //isAttacking = false;
        currentTarget.Shake(0.1f);

    }

    [PunRPC]
    public void RorateAttackHolder(float angle) {
        basicAttack1.rotation = Quaternion.Euler(0, 0, -angle);
        basicAttack2.rotation = Quaternion.Euler(0, 0, -angle);
    }

    public override void Attack(Pion p)
    {
        if (p == null)
        {
            return;
        }
        //base.Attack(p);

        currentTarget = p;
        Vector2 dirr = p.transform.position - transform.position;
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;
        //Debug.Log(angle + " - " + dirr);
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("RorateAttackHolder", RpcTarget.All, angle);
        }
        else {
            RorateAttackHolder(angle);
        }
       


        if (actualAttackTimes+bonusTimes >= attackTimesToActiveSkill) {
            actualAttackTimes = 0;
            anim.SetBool("skill", true);
        }
        else
        {
            
            Vector2 Epos = currentTarget.getCurrentTile();
            Vector2 Mpos = getCurrentTile();
            int rangeX = (int)Mathf.Abs(Epos.x - Mpos.x);
            int rangeY = (int)Mathf.Abs(Epos.y - Mpos.y);

            //passive 2
            if ( ( (rangeX + rangeY) == 1 || (rangeX == 1 && rangeY == 1) ) && levelHero >= 2)
            {
                anim.SetBool("attack2" , true);
            }
            else
            {
                //pv.RPC("setTargetPos", RpcTarget.All, currentTarget.transform.position);
                anim.SetBool("attack1", true);
            }
        }


        

        //anim.SetTrigger("attack1");
        //Debug.Log("Attacking"); ;
    }

    /*[PunRPC]
    public void setTargetPos(Vector3 pos) {
        targetPos = pos;
    }*/


    [PunRPC]
    public override void ImplementDamage(int PhyDamage, int MagDamage,int armyCount ,float shakeTime)
    {
        actualAttackTimes = 0;
        base.ImplementDamage(PhyDamage, MagDamage,armyCount, shakeTime);

        if (!isMine)
            return;

        //Passive 3
        if (levelHero >= 3 && _targetAttack.Count > 0) {

            //passive 5
            if (levelHero >= 5 && _targetAttack.Count == 1 && _targetAttack[0] != null ) {

                Vector2 pos = _targetAttack[0].getCurrentTile();
                if (isInRange((int)pos.x, (int)pos.y, 1)) {
                    bool isContain = false;
                    foreach (BuffStatusScriptableObject buff in buffHero)
                    {
                        if (buff.buffName == buffPassive5.buffName)
                        {
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain)
                    {
                        addBuff(buffPassive5);
                    }
                }
            }

            foreach (Pion p in _targetAttack)
            {
                if (p == null) {
                    return;
                }
                Vector2 pos = p.getCurrentTile();
                if (isInRange((int)pos.x, (int)pos.y, 1) && 0.1f > Random.RandomRange(0f, 1f))
                {
                    
                    StartCoroutine(stepBack());
                    break;
                }
            }
        }
        
    }
    int bonusTimes = 0;
    public override void traceEnemyAround()
    {
        if (MapGenerator.MAIN == null) {
            return;
        }
        base.traceEnemyAround();


        //Pasive 4
        if (levelHero >= 4)
        {

            bool isFinish = false;
            for (int i = -1 * 1; i <= 1; i++)
            {
                for (int j = -1 * 1; j <= 1; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        continue;
                    }

                    MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(new Vector2(i, j) + getCurrentTile());
                    if (MapGenerator.MAIN.isLake(tile))
                    {
                        bonusTimes = 2;
                        isFinish = true;
                        break;
                    }

                }
                if (isFinish)
                {
                    break;
                }
            }
            if (!isFinish)
            {
                bonusTimes = 0;
            }
        }

    }

    public void enemyHitted(Pion p)
    {
        if (!isMine)
            return;

        currentTarget = p;
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("instantiateEffect", RpcTarget.All, false, currentTarget.transform.position, currentTarget.pv.ViewID);
        }
        else {
            instantiateEffect(false, currentTarget.transform.position, currentTarget);
        }

        EnemyAttacked();
    }
}
