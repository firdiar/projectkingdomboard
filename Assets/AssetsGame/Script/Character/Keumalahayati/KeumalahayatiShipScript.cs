﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class KeumalahayatiShipScript : MonoBehaviour
{

    [SerializeField]KeumalahayatiHeroScript hero;
    [SerializeField] Animation anim;
    [SerializeField] AudioSource[] sound;

    public void Start()
    {
        sound =  GetComponents<AudioSource>();
    }

    public void cannonShooted() {
        hero.cannonShooted();



    }
    public void playShipAnim() {

        foreach (AudioSource asc in sound) {
            asc.volume = 1;
            asc.Play();
        }

        //sound.volume = 1;
        //.Play();
        //sound2.Play();
        anim.Play();
    }
    public void stopShipAnim() {
        StartCoroutine(volDown());
        anim.Stop();
    }



    IEnumerator volDown()
    {
        float progress = 0;
        float start = 1;
        while (progress < 1)
        {
            progress += 0.01f;
            float val = Mathf.Lerp(start, 0, progress);
            //sound2.volume = Mathf.Lerp(start, 0, progress);
            foreach (AudioSource asc in sound)
            {
                asc.volume = val;
                //asc.Play();
            }
            yield return new WaitForSeconds(0.05f);
        }
        foreach (AudioSource asc in sound)
        {
            asc.Stop();
        }
        yield return null;
    }



}
