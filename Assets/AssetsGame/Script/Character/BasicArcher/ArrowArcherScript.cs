﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowArcherScript : MonoBehaviour
{
    //public float speed = 1;
    public ILongRanged parent;
    public Vector2 target = Vector2.zero;
    public Transform armyEnemy;

    private void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position , target , 30*Time.deltaTime);
        //speed += Time.deltaTime;

        if (armyEnemy != null)
        {
            foreach (Transform child in armyEnemy)
            {
                //Debug.Log( child.name+" - "+Vector2.Distance(child.position, transform.position));
                if (Vector2.Distance(child.position, transform.position) < 1f)
                {
                    parent.enemyHitted(child.GetComponent<Pion>());
                    Destroy(gameObject);
                }
            }
        }

        if (Vector2.Distance(target, transform.position) < 0.2f) {
            Destroy(gameObject);
        }
    }

}
