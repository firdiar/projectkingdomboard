﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public interface ILongRanged
{
    void enemyHitted(Pion p);
}

public class BasicArcherHeroScript : Pion , ILongRanged
{
    [Header("Heroes Addon")]
    Pion currentTarget = null;
    [SerializeField] Transform basicAttackHolder = null;
    [SerializeField] Transform positionSpawnArrow = null;
    [SerializeField] GameObject slashAfter = null;
    [SerializeField] ArrowArcherScript ArrowPrefabs = null;
    [SerializeField] AudioClip attackBasic;

    [PunRPC]
    public void RotateAttackHolder(float angle)
    {
        basicAttackHolder.rotation = Quaternion.Euler(0, 0, -angle);
    }


    [PunRPC]
    public void instantiateEffect(Vector3 pos, int viewID)
    {
        PhotonView v = PhotonView.Find(viewID);
        if (v != null)
        {
            GameObject temp = Instantiate(slashAfter, pos, Quaternion.Euler(0, 0, (PhotonNetwork.IsMasterClient ? 0 : 180)), PhotonView.Find(viewID).transform);
            Destroy(temp, 0.9f);
        }
    }


    public void instantiateEffect(Vector3 pos, Pion p)
    {

        GameObject temp = Instantiate(slashAfter, pos, Quaternion.Euler(0, 0,  0 ), p.transform);
        Destroy(temp, 0.9f);
        
    }

    /*public void Start()
    {
        anim.SetBool("attack", true);
    }*/

    public void bowShooted() {

        if (!isMine)
            return;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("Shoot", RpcTarget.All, getRange());
        }
        else {
            Shoot(getRange());
        }

        //isAttacking = false;
    }

    [PunRPC]
    public void Shoot(int range) {

        audios.clip = attackBasic;
        audios.Play();

        ArrowArcherScript arrow = Instantiate(ArrowPrefabs, positionSpawnArrow.position, Quaternion.Euler(0, 0, basicAttackHolder.eulerAngles.z + 90));
        arrow.parent = this;


        float a = basicAttackHolder.eulerAngles.z / (180.0f / Mathf.PI);
        Vector2 target = new Vector2(-Mathf.Sin(a), Mathf.Cos(a));

        arrow.target = (Vector2)transform.position + (target * 4f * range);

        if (MapGenerator.MAIN.isOnline)
        {
            if (isMine)//jika ini pasukan kita
            {
                arrow.armyEnemy = MapGenerator.MAIN.armyEnemyParent.transform;
            }
            else
            {
                arrow.armyEnemy = MapGenerator.MAIN.armyParent.transform;
            }
        }
        else
        {
            if (transform.parent == MapGenerator.MAIN.armyParent.transform)//jika ini pasukan kita
            {
                arrow.armyEnemy = MapGenerator.MAIN.armyEnemyParent.transform;
            }
            else
            {
                arrow.armyEnemy = MapGenerator.MAIN.armyParent.transform;
            }
        }



#if UNITY_EDITOR
        if (MapGenerator.MAIN.isTestMode ||(MapGenerator.MAIN.isOnline && PhotonNetwork.CurrentRoom.PlayerCount == 1))
        {
            arrow.armyEnemy = MapGenerator.MAIN.armyParent.transform;
        }
#endif


    }


    public override void Attack(Pion p)
    {
        //Debug.Log("A");
        if (!canAttack())
        {
            isAttacking = false;
            Debug.Log("Cant attack player due to capturing");
            return;
        }
        //Debug.Log("B");
        if (p == null)
        {
            return;
        }

        //Debug.Log(name + " Attacking " + p.name);

        currentTarget = p;
        Vector2 dirr = p.getCurrentTile() - getCurrentTile();
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("RotateAttackHolder", RpcTarget.All, angle);
        }
        else {
            RotateAttackHolder(angle);
        }

        anim.SetBool("attack", true);

    }

    public void enemyHitted(Pion p)
    {
        //throw new System.NotImplementedException();
        if (!isMine)
            return;

        //anim.SetBool("attack", false);


        if (p == null)
        {
            return;
        }

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("instantiateEffect", RpcTarget.All, p.transform.position, p.pv.ViewID);
        }
        else {
            instantiateEffect(p.transform.position , p);
        }
        
        Vector2 delta = p.getCurrentTile() - getCurrentTile();

        int PhyDamage = Mathf.RoundToInt(getPhyDamage());
        int MagDamage = Mathf.RoundToInt(getMagDamage());

        if ((Mathf.Abs(delta.x) + Mathf.Abs(delta.y)) == 1 || (Mathf.Abs(delta.x) == 1 && 1 == Mathf.Abs(delta.y)))
        {
            PhyDamage = Mathf.RoundToInt(PhyDamage * 0.4f);
            MagDamage = Mathf.RoundToInt(MagDamage * 0.4f);
            //pv.RPC("showTex")
        }


        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            p.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.2f);

#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
            }
#endif
        }
        else {
            p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
        }
        p.Shake(0.2f);
    }
}
