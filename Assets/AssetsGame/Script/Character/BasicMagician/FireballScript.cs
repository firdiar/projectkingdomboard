﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FireballScript : MonoBehaviour
{
    public BasicMagicianHeroScript parent;
    public Pion target;
    
    

    public GameObject prefabExplosion;

    float speed = 0;
    //public bool attacking = false;

    
    public void setTarget(Pion target) {
        this.target = target;
        
    }

    
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    bool destroyed = false;

    // Update is called once per frame
    void Update()
    {
        if (parent != null)
        {

            if (target == null)
            {
                rotateAroundParent(Time.deltaTime);
            }
            else
            {
                transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
                if (Vector2.Distance(transform.position, target.transform.position) < 0.1f) {
                    
                    parent.fireballExplode(this.transform.position, this);
                    
                    parent = null;
                }
            }

        }
        else if(!destroyed){
            destroyed = true;
            GameObject obj = Instantiate(prefabExplosion, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            Destroy(gameObject,0.5f);
            Destroy(obj, 2f);
        }
    }

    void rotateAroundParent(float deltaTime) {
        
        transform.RotateAround(parent.transform.position, Vector3.forward, 270 * deltaTime);

        float dist = Vector2.Distance(parent.transform.position, transform.position);
        if (dist != 2.2f)
        {
            float distance = 2.2f;
            Vector2 target = (transform.position - parent.transform.position).normalized * distance + parent.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, target, speed * deltaTime);
        }
    }
}
