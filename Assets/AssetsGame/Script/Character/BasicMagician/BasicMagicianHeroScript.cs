﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BasicMagicianHeroScript : Pion
{
    [SerializeField] FireballScript fireballPrefabs;
    [SerializeField] int fireballLimit = 3;
    List<FireballScript> fireballs = new List<FireballScript>();

    float skillCD = 12;

    public override void Update()
    {
        base.Update();

        if (isMine)
        {
            skillCD -= Time.deltaTime;
            if (skillCD <= 0)
            {
                skill();
                skillCD = getAttackSpeed() + 2;
            }
        }

    }

    void skill() {
        if (fireballs.Count >= fireballLimit) {
            return;
        }

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("summonFireball", RpcTarget.All);
        }
        else {
            summonFireball();
        }
        
    }

    [PunRPC]
    public void summonFireball() {
        FireballScript temp = Instantiate(fireballPrefabs, transform.position + Vector3.left, Quaternion.identity);
        temp.parent = this;
        temp.setSpeed(19);
        fireballs.Add(temp);
    }

    public void fireballExplode(Vector2 pos , FireballScript fireball) {


        fireballs.Remove(fireball);

        if (!isMine)
            return;

        if (MapGenerator.MAIN == null)
            return;

        Vector2 center = MapGenerator.MAIN.getMapTileIndexFromWorld(pos);

        foreach (Transform child in MapGenerator.MAIN.armyEnemyParent) {

            Vector2 targetPos = MapGenerator.MAIN.getMapTileIndexFromWorld(child.transform.position);

            Vector2 delta = targetPos - center;

            if ((Mathf.Abs(delta.x) + Mathf.Abs(delta.y) <= 1) || (Mathf.Abs(delta.x) == 1 && Mathf.Abs(delta.y) == 1))
            {
                Pion p = child.GetComponent<Pion>();

                int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.7f);
                int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.7f);

                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    p.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.5f);
                }
                else {
                    ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.5f);
                }
                p.Shake(0.5f);
            }

        }


#if UNITY_EDITOR
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                foreach (Transform child in MapGenerator.MAIN.armyParent.transform)
                {

                    Vector2 targetPos = MapGenerator.MAIN.getMapTileIndexFromWorld(child.transform.position);

                    Vector2 delta = targetPos - center;

                    Debug.Log(delta);

                    if ((Mathf.Abs(delta.x) + Mathf.Abs(delta.y) <= 1) || (Mathf.Abs(delta.x) == 1 && Mathf.Abs(delta.y) == 1))
                    {
                        Pion p = child.GetComponent<Pion>();

                        int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.7f);
                        int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.7f);


                        p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.5f);
                        p.Shake(0.5f);
                    }

                }
            }
        }
        else if(MapGenerator.MAIN.isTestMode)
        {
            foreach (Transform child in MapGenerator.MAIN.armyParent.transform)
            {

                Vector2 targetPos = MapGenerator.MAIN.getMapTileIndexFromWorld(child.transform.position);

                Vector2 delta = targetPos - center;

                Debug.Log(delta);

                if ((Mathf.Abs(delta.x) + Mathf.Abs(delta.y) <= 1) || (Mathf.Abs(delta.x) == 1 && Mathf.Abs(delta.y) == 1))
                {
                    Pion p = child.GetComponent<Pion>();

                    int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.7f);
                    int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.7f);


                    p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.5f);
                    p.Shake(0.5f);
                }

            }
        }
#endif


    }

    public override void Attack(Pion p)
    {
        isAttacking = false;

        if (!canAttack())
        {
            Debug.Log("Cant attack player due to capturing");
            return;
        }

        if (fireballs.Count == 0 || p == null)
            return;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("initToAttack", RpcTarget.All, p.pv.ViewID);
        }
        else {
            initToAttack(p);
        }

    }

    [PunRPC]
    public void initToAttack(int id) {

        PhotonView idPion = PhotonView.Find(id);
        if (idPion == null)
        {
            return;
        }

        Pion target = idPion.GetComponent<Pion>();

        initToAttack(target);
        
    }

    void initToAttack(Pion target) {
        foreach (FireballScript f in fireballs)
        {
            f.setTarget(target);
        }
    }

}
