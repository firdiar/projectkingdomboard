﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrabuSiliwangiTigerScript : MonoBehaviour
{
    public PrabuSiliwangiHeroScript parent;
    public Pion target;
    public float speed = 2;
    public float rotationMax = 0.5f;

    public float timeBeforeDie = 2;
    float timer = 0;

    


    [SerializeField] Animation anim;

    [SerializeField]ParticleSystem head;
    [SerializeField] ParticleSystem handL;
    [SerializeField] ParticleSystem handR;
    [SerializeField] GameObject afterArrow;

    [Header("Audio")]
    [SerializeField] AudioSource audio;
    [SerializeField] AudioClip attack;
    [SerializeField] AudioClip roar;

    public void initAttack()
    {
        initAttack(target);
    }


    public void Awake()
    {
        audio.clip = roar;
        audio.PlayDelayed(2);
    }

    public void Die() {
        if (parent != null)
        {
            parent.TigerDisapear();
        }

        Destroy(gameObject);
    }

    public void Attack()
    {
        taskDone = true;

        if (parent != null)
        {
            parent.TigerAttacking();
        }
        transform.Translate(Vector2.down *1.35f, Space.Self);

        if (target != null) {
            GameObject slash = Instantiate(afterArrow, target.transform);
            Destroy(slash, 3);
        }

        audio.clip = attack;
        audio.Play();

    }

    public void Summoned()
    {
        if (parent != null)
        {
            parent.TigerSummoned();
        }

        isSummoned = true;
    }

    void setDie() {
        anim.Play("TigerDie");
    }

    void setBackToParent()
    {
        

        isComeBack = true;

        //anim.Play("TigerDie");
        if (parent != null)
        {
            parent.TigerBack();
        }
    }

    void setAttack() {
        Debug.Log("attacking");
        isAttacking = true;
        anim.Play("TigerAttack");
    }
    public void finishAttack(){
        isAttacking = false;

        target = null;
    }

    public void initParent(PrabuSiliwangiHeroScript parent) {

        Debug.Log("Parent setted");
        this.parent = parent;
    }



    public void initAttack(Pion target)
    {

        if (target == null)
        {
            setDie();
            return;
        }

        

        this.target = target;
        taskDone = false;
        isComeBack = false;
        timer = timeBeforeDie;
    }
    bool isComeBack = false;
    bool isSummoned = false;
    bool isAttacking = false;
    bool taskDone = true;

    public void Update()
    {
        //timeBeforeDie -= Time.deltaTime;

        

        if (!isSummoned)
            return;

        if (parent == null)
        {
            setDie();
            isSummoned = false;
            return;
        }

        if (isComeBack) {
            
            Vector2 dirr = this.parent.transform.position - transform.position;
            //Vector2 dirr = target.transform.position - transform.position;

            float z = (transform.rotation.eulerAngles.z > 180 ? transform.rotation.eulerAngles.z - 360 : transform.rotation.eulerAngles.z);

            float angle = (-1 * (Mathf.Atan2(dirr.x, dirr.y) * 180f / Mathf.PI)) - z;

            if (Mathf.Abs(angle + 360 * ((angle / Mathf.Abs(angle)) * -1)) < Mathf.Abs(angle))
            {
                angle = angle + 360 * ((angle / Mathf.Abs(angle)) * -1);
            }


            float rotate = z + Mathf.Clamp(angle, -rotationMax*2, rotationMax*2);//Mathf.LerpAngle(transform.rotation.z, angle, Time.deltaTime*rotationMax);

            transform.rotation = Quaternion.Euler(0, 0, rotate);
            float temp = -transform.rotation.eulerAngles.z / (180.0f / Mathf.PI);
            head.startRotation = temp;
            handL.startRotation = temp;
            handR.startRotation = -temp;



            if (!isAttacking && Vector2.Distance(this.parent.transform.position, transform.position) < 1.5f)
            {
                setDie();
                transform.Translate(Vector2.up * Time.deltaTime * 0.1f, Space.Self);
            }
            else {

                transform.Translate(Vector2.up * Time.deltaTime * speed, Space.Self);
            }
            return;
        }

        if (timer < 0) {
            setBackToParent();
        }

        if (taskDone) {
            
            timer -= Time.deltaTime;
            transform.Translate(Vector2.up * Time.deltaTime*0.2f , Space.Self);
            return;
        }
           

        if (target != null)
        {
            //transform.position = Vector2.MoveTowards(transform.position, target.transform.position, Time.deltaTime*speed);
            

            Vector2 dirr = target.transform.position - transform.position;

            float z = ( transform.rotation.eulerAngles.z > 180 ? transform.rotation.eulerAngles.z - 360 : transform.rotation.eulerAngles.z);

            float angle = (-1 * (Mathf.Atan2(dirr.x, dirr.y) * 180f / Mathf.PI)) - z ;

           // Debug.Log(dirr + " - " + z + " - " + angle);

            if (Mathf.Abs(  angle + 360 * ((angle / Mathf.Abs(angle)) * -1)) < Mathf.Abs(  angle )) {
                angle = angle + 360 * ((angle/Mathf.Abs(angle))*-1);
            }

            float rotate = 0;

            if (Vector2.Distance(target.transform.position , transform.position) < 2)
            {
                float ekstra = 4f;
                rotate = z + Mathf.Clamp(angle, -rotationMax*ekstra, rotationMax*ekstra);//Mathf.LerpAngle(transform.rotation.z, angle, Time.deltaTime*rotationMax);
            }
            else {
                rotate = z + Mathf.Clamp(angle, -rotationMax, rotationMax);
            }
            transform.rotation = Quaternion.Euler(0, 0, rotate);
            float temp = -transform.rotation.eulerAngles.z / (180.0f / Mathf.PI);
            head.startRotation = temp;
            handL.startRotation = temp;
            handR.startRotation = -temp;

            transform.Translate(Vector2.up * Time.deltaTime * speed, Space.Self);

            if (!isAttacking&&Vector2.Distance(target.transform.position, transform.position) < 2.5f && inFrontOf(target.transform.position)) {
                setAttack();
            }
            //float a = transform.rotation.z * Mathf.Deg2Rad;

            //transform.position = new Vector3(Mathf.Cos(a), Mathf.Sin(a), 0.0f).normalized * Time.deltaTime * speed;

        }
        else {
            taskDone = true;
            setDie();
        }
        
    }

    bool inFrontOf(Vector2 pos) {

        Vector3 direction = Quaternion.Euler(transform.rotation.eulerAngles) * Vector3.up;

        if (Vector2.Distance(transform.position + direction, pos) < Vector2.Distance(transform.position, pos))
        {
            return true;
        }
        else {
            return false;
        }
    }

}
