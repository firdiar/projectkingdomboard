﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PrabuSiliwangiHeroScript : Pion
{
    [Header("Hero Addon")]
    public PrabuSiliwangiTigerScript prefabsTiger;
    public PrabuSiliwangiTigerScript currentTiger;
    [SerializeField] AudioClip summonSound = null;

    public Pion currentTarget = null;
    public string pathDebuffPassive5;

    public void TigerAttacking() {
        if (!isMine)
            return;


        int MagDamage = Mathf.RoundToInt(getMagDamage() * (1+(limitAttack*0.1f)) );

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, 0, MagDamage, actualArmyCount, 0.5f);

#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(0, MagDamage, actualArmyCount, 0.5f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(0, MagDamage, actualArmyCount, 0.5f);
        }

        currentTarget.Shake(0.4f);


        //passive 3
        if (levelHero >= 3) {
            int heal = (int)(MagDamage * 0.2f);
            if (actualArmyCount == totalArmyCount && levelHero >= 4)
            {
                foreach (Pion p in MapGenerator.MAIN.armyParent.playerPion)
                {
                    if (p.actualArmyCount != p.totalArmyCount)
                    {
                        p.Heal(heal);
                        break;
                    }
                }
            }
            else {
                Heal(heal);
            }

            if (levelHero >= 5 && currentTarget != null) {
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    currentTarget.pv.RPC("addBuff", RpcTarget.Others, pathDebuffPassive5);
#if UNITY_EDITOR
                    if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                    {
                        currentTarget.addBuff(pathDebuffPassive5);
                    }
#endif
                }
                else {
                    currentTarget.addBuff(pathDebuffPassive5);
                }
            }

        }
    }

    public void TigerDisapear()
    {
        if (!isMine)
            return;

        isAttacking = false;
    }

    public void TigerBack() {

        if (!isMine)
            return;


        if (limitAttack <= 0)
            return;

        if (currentTarget == null) {
            return;
        }

        limitAttack--;


        int range = (levelHero >= 2 ? 3 : 2);
        currentTarget = traceEnemyAround(currentTarget.getCurrentTile(), range);

        if (currentTarget == null)
        {
            return;
        }

        //currentTiger.initAttack(currentTarget);
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("tigerInitAttack", RpcTarget.All, currentTarget.pv.ViewID);
        }
        else {
            tigerInitAttack(currentTarget);
        }

    }

    public Pion traceEnemyAround(Vector2 enemyPos , int range)
    {
        List<Pion> list = new List<Pion>();
        Vector2 myPos = enemyPos;
        //Debug.Log("My pos " + myPos+" range "+ getRange());
        for (int i = -1 * range; i <= range; i++)
        {
            for (int j = -1 * range; j <= range; j++)
            {
                if (i == 0 && j == 0)
                {
                    continue;
                }
                //Debug.Log("Tracking " + i + " - " + j);
                MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(new Vector2(i, j) + myPos);
                if (tile != null && tile.isOccupied)
                {
                    Pion p = tile.getCurrentPion();
                    if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                    {
#if UNITY_EDITOR
                        //IMPORTANT!!
                        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                        {//&& !p.pv.IsMine) {

                            if (p != null)
                            {
                                list.Add(p);
                            }
                        }
                        else
                        {
#endif
                            //IMPORTANT!!
                            if (p != null && !p.pv.IsMine)
                            {
                                list.Add(p);
                            }
                            //IMPORTANT!!
#if UNITY_EDITOR
                        }
#endif
                    }
                    else {
                        if (p != null && (p.transform.parent != transform.parent || MapGenerator.MAIN.isTestMode))
                        {
                            list.Add(p);
                        }
                    }

                }
            }
        }

        if (list.Count > 0) {
            return list[Random.RandomRange(0, list.Count)];
        }
        else {
            return null;
        }
        

    }

    public void TigerSummoned()
    {
        //currentTiger.initAttack();
        if (!pv.IsMine)
            return;

        Debug.Log("Tiger inited to attack");
        
    }

    int limitAttack = 0;

    [PunRPC]
    public void summonTiger(int id) {
        

        showTextSkill("Spirit Tiger!");

        PhotonView v = PhotonView.Find(id);

        if (v == null) {
            return;
        }

        currentTarget = v.GetComponent<Pion>();

        summonTiger(currentTarget);

    }


    void summonTiger(Pion p) {

        audios.clip = summonSound;
        audios.PlayDelayed(1.5f);

        currentTiger = Instantiate(prefabsTiger, transform.position, transform.rotation);
        currentTiger.initParent(this);
        currentTiger.initAttack(currentTarget);


        Vector2 dirr = currentTarget.transform.position - transform.position;
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;

        currentTiger.transform.rotation = Quaternion.Euler(0, 0, -angle + (Random.RandomRange(-1f, 1f) * 90));
    }


    [PunRPC]
    public void tigerInitAttack(int id) {
        PhotonView v = PhotonView.Find(id);

        if (v == null)
        {
            return;
        }
        currentTarget = v.GetComponent<Pion>();
        //currentTiger.initAttack(currentTarget);

        tigerInitAttack(currentTarget);
        
    }


    public void tigerInitAttack(Pion p)
    {

        currentTiger.initAttack(p);
    }

    public override void Attack(Pion p) {
        currentTarget = p;
        limitAttack = 3;
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("summonTiger", RpcTarget.All, p.pv.ViewID);
        }
        else {
            summonTiger(p);
        }
    }
}
