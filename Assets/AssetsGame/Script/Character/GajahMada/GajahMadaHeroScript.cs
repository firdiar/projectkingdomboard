﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GajahMadaHeroScript : Pion
{
    Pion currentTarget = null;
    [Header("Hero Field")]
    [SerializeField] int hitTakenToActiveSkill = 6;
    [SerializeField] Transform basicAttackHolder = null;
    [SerializeField] BuffStatusScriptableObject gajahMadaSkill;
    [SerializeField] List<BuffStatusScriptableObject> passive = new List<BuffStatusScriptableObject>();

    [SerializeField] AudioClip attackBasic;
    [SerializeField] AudioClip attackBasic3; 
    [SerializeField] AudioClip skill;

    int damageTakenTimes = 0;

    public override void FirstFunction()
    {
        base.FirstFunction();

        int loop = (5 - 1);
        passive.RemoveRange( loop , 4 - loop  );

        checkPassive();
    }

    public void checkPassive() {
        if (passive.Count == 0) {
            return;
        }

        foreach (BuffStatusScriptableObject buff in passive.ToArray())
        {
            if (buff.caseActive == BuffStatusScriptableObject.ActiveCase.UNITCOUNT_GREATHER_THAN)
            { // jika army kita lebih banyak dari seharusnya remove
                if (buff.requirement < actualArmyCount)
                {
                    addBuff(buff);
                    passive.Remove(buff);
                }
            }
            else if (buff.caseActive == BuffStatusScriptableObject.ActiveCase.UNITCOUNT_LESS_THAN)
            { // jika army kita lebih sedikit dari seharusnya remove
                if (buff.limit > actualArmyCount)
                {
                    addBuff(buff);
                    passive.Remove(buff);
                }
            }
            else if (buff.caseActive == BuffStatusScriptableObject.ActiveCase.POWER_GREATHER_THAN)
            { // jika power kita lebih besar dari seharusnya remove
                if (buff.limit < (actualArmyCount / (totalArmyCount * 1f)))
                {
                    addBuff(buff);
                    passive.Remove(buff);
                }
            }
            else if (buff.caseActive == BuffStatusScriptableObject.ActiveCase.POWER_LESS_THAN)
            { // jika power kita lebih sedikit dari seharusnya remove
                if (buff.limit > (actualArmyCount / (totalArmyCount * 1f)))
                {
                    addBuff(buff);
                    passive.Remove(buff);
                }
            }
        }

    }

    public override void Attack(Pion p)
    {
        if (!isMine)
            return;

        ///if (!canAttack())
        //{
        //    isAttacking = false;
        //    Debug.Log("Cant attack player due to capturing");
        //    return;
        //}

        if (p == null)
        {
            return;
        }

        currentTarget = p;
        Vector2 dirr = p.getCurrentTile() - getCurrentTile();
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;
        //Debug.Log(angle +" - "+ dirr);
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("RorateAttackHolder", RpcTarget.All, angle);
        }
        else {
            RorateAttackHolder(angle);
        }

        anim.SetBool("attack" , true);

    }

    public void AttackOne() {

        audios.clip = attackBasic;
        audios.Play();

        if (!isMine)
            return;

        //anim.SetBool("attack", false);


        if (currentTarget == null) {
            return;
        }
        int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.5f);
        int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.5f);


        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {

            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.1f);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
        }
        currentTarget.Shake(0.1f);
    }
    public void AttackTwo()
    {

        audios.clip = attackBasic;
        audios.Play();

        if (!isMine)
            return;

        if (currentTarget == null)
        {
            return;
        }
        int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.5f);
        int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.5f);


        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.1f);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.1f);
        }
        currentTarget.Shake(0.1f);
    }
    public void AttackThree()
    {

        audios.clip = attackBasic3;
        audios.Play();

        if (!isMine)
            return;

        if (currentTarget == null)
        {
            return;
        }
        int PhyDamage = Mathf.RoundToInt(getPhyDamage() * 0.7f);
        int MagDamage = Mathf.RoundToInt(getMagDamage() * 0.7f);

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.3f);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.3f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.3f);
        }
        currentTarget.Shake(0.3f);

        //isAttacking = false;
    }

    public void Skill() {

        

        //anim.SetBool("skill", false);
        addBuff(gajahMadaSkill);

        audios.clip = skill;
        audios.Play();
    }


    [PunRPC]
    public void RorateAttackHolder(float angle)
    {
        basicAttackHolder.rotation = Quaternion.Euler(0, 0, -angle);
    }

    [PunRPC]
    public override void ImplementDamage(int PhyDamage, int MagDamage ,int armyCount, float shakeTime)
    {

        damageTakenTimes++;

        base.ImplementDamage(PhyDamage, MagDamage ,armyCount, shakeTime);

        if (damageTakenTimes >= hitTakenToActiveSkill)
        {
            damageTakenTimes = 0;
            anim.SetBool("skill" , true);
        }

        checkPassive();

    }

}
