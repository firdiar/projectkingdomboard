﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrinceDipenogoroHeroScript : Pion
{
    public int skillReqirement = 3;
    int actualMoveToSkill;

    int bonusDamage = 0;

    

    Pion currentTarget;
    [Header("Hero")]
    [SerializeField]Transform basicAttack;
    [SerializeField]SpriteRenderer basicAttackSprite;
    [SerializeField]Transform skillAttack;
    [SerializeField] string pathDebuffPassive1;
    [SerializeField] string pathDebuffPassive2;
    [SerializeField] BuffStatusScriptableObject buffPassive5;
    

    [Header("Sound")]
    [SerializeField] AudioClip basicAttackSound = null;
    [SerializeField] AudioClip skillAttackSound = null;

    public override void FirstFunction()
    {
        base.FirstFunction();
        if (levelHero >= 4)
        {
            skillReqirement -= 1;
        }

    }

    public void basicAttackHitted() {
        if (!isMine)
            return;

        //isAttacking = false;

        if (currentTarget == null) {
            return;
        }

        int PhyDamage = getPhyDamage() + (bonusDamage*actualArmyCount);
        int MagDamage = getMagDamage();

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.2f);

#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
        }
        currentTarget.Shake(0.2f);

        if (levelHero >= 3) {
            bonusDamage += 4;
        }
    }

    public void skillAttackHitted()
    {
        if (!isMine || currentTarget == null)
            return;



        //isAttacking = false;
        int PhyDamage = Mathf.RoundToInt( getPhyDamage()*1.4f );

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("addBuff", RpcTarget.Others, pathDebuffPassive1);
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.addBuff(pathDebuffPassive1);
            }
#endif
            if (levelHero >= 2)
            {
                currentTarget.pv.RPC("addBuff", RpcTarget.Others, pathDebuffPassive2);
#if UNITY_EDITOR
                if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                {
                    currentTarget.addBuff(pathDebuffPassive2);
                }
#endif
            }

            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, 0, actualArmyCount, 0.3f);

#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, 0, actualArmyCount, 0.3f);
            }
#endif

        }
        else
        {
            currentTarget.addBuff(pathDebuffPassive1);
            if (levelHero >= 2)
            {
                currentTarget.addBuff(pathDebuffPassive2);
            }

            currentTarget.ImplementDamage(PhyDamage, 0, actualArmyCount, 0.3f);
        }




        currentTarget.Shake(0.3f);

        if (levelHero >= 5) {
            addBuff(buffPassive5);
        }

    }

    public override float getMovementSpeed()
    {
        return base.getMovementSpeed() * (1+(bonusDamage/500f));
    }

    public override void moved()
    {
        //base.moved();
        actualMoveToSkill++;

    }

    [PunRPC]
    public void RorateAttackHolder(float angle)
    {
        
        basicAttack.rotation = Quaternion.Euler(0, 0, -angle);
        skillAttack.rotation = basicAttack.rotation;
    }

    public override void Attack(Pion p)
    {
        //base.Attack(p);

        if (p == null)
        {
            return;
        }
        //base.Attack(p);

        currentTarget = p;
        Vector2 dirr = p.transform.position - transform.position;
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("RorateAttackHolder", RpcTarget.All, angle);
        }
        else {
            RorateAttackHolder(angle);
        }




        if (actualMoveToSkill >= skillReqirement)
        {
            actualMoveToSkill = 0;
            anim.SetBool("skill", true);
            
        }
        else {

            basicAttackSprite.flipX = (Random.RandomRange(0,2) == 1? true:false);
            anim.SetBool("attack", true);
            
        }
        

    }

    public void playBasicSound() {
        audios.clip = basicAttackSound;
        audios.Play();
    }
    public void playSkillSound() {
        audios.clip = skillAttackSound;
        audios.Play();
    }

}
