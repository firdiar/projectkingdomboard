﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BasicInfantryHeroScript : Pion
{
    [Header("Heroes Addon")]
    Pion currentTarget = null;
    [SerializeField] Transform basicAttackHolder = null;
    [SerializeField] AudioClip attackBasic;
    [SerializeField] GameObject slashAfter = null;

    [PunRPC]
    public void RotateAttackHolder(float angle) {
        basicAttackHolder.rotation = Quaternion.Euler(0, 0, -angle);
    }

    public void EnemyAttacked() {

        audios.clip = attackBasic;
        audios.Play();

        if (!isMine)
            return;

        //anim.SetBool("attack", false);


        if (currentTarget == null)
        {
            return;
        }
        int PhyDamage = Mathf.RoundToInt(getPhyDamage());
        int MagDamage = Mathf.RoundToInt(getMagDamage());

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            currentTarget.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount, 0.2f);

#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
            }
#endif
        }
        else {
            currentTarget.ImplementDamage(PhyDamage, MagDamage, actualArmyCount, 0.2f);
        }

        currentTarget.Shake(0.2f);

        //isAttacking = false;
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("instantiateEffect", RpcTarget.All, currentTarget.transform.position, currentTarget.pv.ViewID);
        }
        else {
            instantiateEffect(currentTarget.transform.position, currentTarget.transform);
        }
    }

    public override void Attack(Pion p)
    {
        if (!isMine)
            return;

        if (!canAttack())
        {
            isAttacking = false;
            Debug.Log("Cant attack player due to capturing");
            return;
        }
        if (p == null)
        {
            return;
        }

        currentTarget = p;
        Vector2 dirr = p.getCurrentTile() - getCurrentTile();
        float angle = Mathf.Atan2(dirr.x, dirr.y) * 180 / Mathf.PI;

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("RotateAttackHolder", RpcTarget.All, angle);
        }
        else {
            RotateAttackHolder(angle);
        }

        anim.SetBool("attack", true);

    }

    [PunRPC]
    public void instantiateEffect( Vector3 pos, int viewID)
    {
        PhotonView v = PhotonView.Find(viewID);
        if (v != null)
        {
            instantiateEffect(pos, v.transform);
            //GameObject temp = Instantiate(slashAfter, pos, Quaternion.Euler(0, 0, (PhotonNetwork.IsMasterClient ? 0 : 180)), v.transform);
            //Destroy(temp, 0.9f);
        }
    }

    void instantiateEffect(Vector3 pos, Transform t)
    {
        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;

        GameObject temp = Instantiate(slashAfter, pos, Quaternion.Euler(0, 0, ( isMaster ? 0 : 180)), t);
        Destroy(temp, 0.9f);
        
    }
}
