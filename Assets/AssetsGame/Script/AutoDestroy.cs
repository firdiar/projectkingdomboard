﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    public float DestroyAfter = 0;

    public void Start()
    {
        Destroy(gameObject, DestroyAfter);
    }
}
