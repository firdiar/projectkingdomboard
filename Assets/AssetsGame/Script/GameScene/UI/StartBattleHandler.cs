﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBattleHandler : MonoBehaviour
{
    [SerializeField] UnityEngine.UI.Text playerName;
    [SerializeField] UnityEngine.UI.Text enemyName;

    [SerializeField]AudioSource src;
    [SerializeField]Animation anim;

    [SerializeField] RectTransform centerRiple;

    UnityEngine.Events.UnityAction action;

    public void init(string playerName , string enemyName) {
        this.playerName.text = playerName;
        this.enemyName.text = enemyName;
    }

    public void startAnim(UnityEngine.Events.UnityAction ack) {
        src.Play();
        anim.Play();
        //Invoke("playRiple", 1.8f);

        action = ack;
    }
    public void playRiple(int custom) {
        Camera.main.GetComponent<RipplePospProcessor>().RippleCamScreenCustom(centerRiple.transform.position , custom) ;
    }

    public void finishAnim() {
        action.Invoke();
        gameObject.SetActive(false);
    }

}
