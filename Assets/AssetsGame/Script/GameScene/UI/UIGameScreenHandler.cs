﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ResultGame { WIN , LOSE , DRAW}

public class UIGameScreenHandler : MonoBehaviour
{
    public static UIGameScreenHandler MAIN = null;

    [Header("UI Reference")]
    [SerializeField] Text myTextName = null;
    [SerializeField] Image myFillPoin = null;
    [SerializeField] Text myTextPoin = null;
    [SerializeField] Text enemyTextName = null;
    [SerializeField] Image enemyFillPoin = null;
    [SerializeField] Text enemyTextPoin = null;
    [SerializeField] Text timeLeft = null;
    [SerializeField] GameObject hintHero = null;
    [SerializeField] GameObject hintBuilding = null;
    [SerializeField] GameObject objectSurrender = null;
    [SerializeField] Animation maps;

    [Header("PopUp Status")]
    [SerializeField] Image heroIdle;
    [SerializeField] Image fillAmountArmy;
    [SerializeField] Text armyCount;
    [SerializeField] Text groupName;

    [SerializeField] Text currentTab;
    [SerializeField] Text health;
    [SerializeField] Text phyAtk;
    [SerializeField] Text magAtk;
    [SerializeField] Text phyDeff;
    [SerializeField] Text magDeff;
    [SerializeField] Text marchSpd;
    [SerializeField] Text counterAtk;
    [SerializeField] Text range;
    [SerializeField] Text attackSpd;
    [SerializeField] Text buffTextArea;
    [SerializeField] Text skillTextArea;

    [Header("PopUp Building")]
    [SerializeField] Image buildingImg;
    [SerializeField] Text BuildingTitle;
    [SerializeField] Text BuildingDesc;

    [Header("Menu Pop Up")]
    [SerializeField] GameObject popUPStatus;
    [SerializeField] GameObject popUpBuilding;
    [SerializeField]GameObject status;
    [SerializeField] GameObject skill;
    [SerializeField] GameObject buff;

    [Header("Finish Game")]
    [SerializeField] Animation layerFinish = null;
    [SerializeField] Animation resultWinLose = null;
    [SerializeField] Animation claimGold = null;
    [SerializeField] Text playerName1 = null;
    [SerializeField] Text playerName2 = null;
    [SerializeField] Text playerName3 = null;
    [SerializeField] Text battleGold = null;
    [SerializeField] Text captureGold = null;
    [SerializeField] Text exploreGold = null;
    [SerializeField] Text totalGold = null;
    int targetTotalGold = 0;

    private void Start()
    {
        //openPopUpStatus();
        UIGameScreenHandler.MAIN = this;
    }

    public void showHintChara(Pion p) {
        initPopUp(p);
        hintHero.SetActive(true);
    }
    public void hideHintChara() {
        hintHero.SetActive(false);
    }
    public void showHintBuilding(MapTile tile)
    {
        initPopUpBuilding( MapGenerator.MAIN.convertTileToDescription(tile) );
        hintBuilding.SetActive(true);
    }
    public void hideHintBuilding()
    {
        hintBuilding.SetActive(false);
    }

    public void InitTop( string myName , string enemyName , float max , int time)
    {
        myTextName.text = myName;
        enemyTextName.text = enemyName;

        UpdatePoin(0, max, false);
        UpdatePoin(0, max, true);

        UpdateTime(time);
    }

    bool mapIsOpened = false;
    public void openCloseMap() {
        if (!mapIsOpened)
        {
            openMaps();
        }
        else {
            closeMaps();
        }
        mapIsOpened = !mapIsOpened;
    }

    void openMaps() {
        maps.Play("OpenMapAnim");
    }
    void closeMaps()
    {
        maps.Play("CloseMapAnim");
    }

    public void openLayerGame() {
        RoyaleHeroesLoading.hideLayerLoading();
    }

    public void Surrender() {
        objectSurrender.SetActive(true);
    }

    [SerializeField] GameObject disconnectLayer = null;

    public void disconnected() {
        disconnectLayer.SetActive(true);
    }

    public void closeLayerGame(ResultGame isWin , int battleGold , int captureCount , int exploreCount)
    {
        this.battleGold.text = battleGold.ToString();
        if (isWin == ResultGame.WIN )
        {
            this.battleGold.text += "x2 (Win)";
            targetTotalGold = battleGold * 2;
        }
        else if (isWin == ResultGame.LOSE)
        {
            this.battleGold.text += " (Lose)";
            targetTotalGold = battleGold;
        }
        else {
            this.battleGold.text += " (Draw)";
            targetTotalGold = 0;
        }

        captureGold.text = captureCount.ToString() + " x " + "100";
        exploreGold.text = exploreCount.ToString() + " x " + "10";

        totalGold.text = "+ 0";
        targetTotalGold += captureCount * 100 + exploreCount * 10 ;

        Account.CurrentAccountData.resources.gold += targetTotalGold;

        //RoyaleHeroesLoading.openLayerLoading();
        layerFinish.gameObject.SetActive(true);
        layerFinish.Play();


        if (isWin == ResultGame.WIN)
        {
            resultWinLose.Play("WinAnim");
            MapGenerator.MAIN.playSoundVictory();
            playerName1.text = Account.CurrentAccountData.nickName;
        }
        else if (isWin == ResultGame.LOSE)
        {
            resultWinLose.Play("LoseAnim");
            MapGenerator.MAIN.playSoundLose();
            playerName2.text = Account.CurrentAccountData.nickName;
        }
        else
        {
            resultWinLose.Play("DrawAnim");
            MapGenerator.MAIN.playSoundDraw();
            playerName3.text = Account.CurrentAccountData.nickName;
        }

        Invoke("playClaimGold", 1.5f);
        Invoke("voidToTargetCoin" , 3);
    }

    void playClaimGold() {
        claimGold.Play();
    }
    float currentTargetValue = -1;
    void voidToTargetCoin() {
        currentTargetValue = 0;
    }
    private void Update()
    {
        if (currentTargetValue != -1) {
            currentTargetValue = Mathf.Min(currentTargetValue + Time.deltaTime / 3f , 1);

            totalGold.text = "+ "+Mathf.RoundToInt( Mathf.Lerp(0 , targetTotalGold , currentTargetValue) ).ToString();
            if (currentTargetValue == 1) {
                currentTargetValue = -1;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Surrender();
        }
    }

    public void gotoResultScene() {
        RoyaleHeroesLoading.openLayerLoading(()=> UnityEngine.SceneManagement.SceneManager.LoadScene("ReportScene"));
    }

    public void initPopUpBuilding(MapGenerator.TileDescription tile) {
        buildingImg.sprite = tile.sprite;
        BuildingTitle.text = tile.title;
        BuildingDesc.text = tile.desc;
        //BuildingDesc.transform.parent.GetComponent<VerticalLayoutGroup>().spacing += 0.01f;
    }

    public void initPopUp(Pion p) {

        groupName.text = p.groupName;
        heroIdle.sprite = p.heroData.idle;
        armyCount.text = p.actualArmyCount + " / " + p.totalArmyCount;
        fillAmountArmy.fillAmount = p.actualArmyCount / (p.totalArmyCount * 1f);

        

        health.text = RoyaleHeroesLibs.intToStringK(p.getHealth());
        phyAtk.text = RoyaleHeroesLibs.intToStringK(p.getPhyAttack());
        magAtk.text = RoyaleHeroesLibs.intToStringK(p.getMagAttack());

        phyDeff.text = RoyaleHeroesLibs.intToStringK(p.getPhyDeff());
        magDeff.text = RoyaleHeroesLibs.intToStringK(p.getMagDeff());

        marchSpd.text = p.getMovementSpeed().ToString();
        attackSpd.text = (Mathf.Floor(p.getAttackSpeed()*100)/100).ToString() + "/s";
        counterAtk.text = Mathf.Floor( p.getCounterAttackRate() * 100 ) .ToString() + "%";
        range.text = p.getRange().ToString();


        string buffTxt = "";

        if (p.buffHero.Count == 0)
        {
            buffTxt = "Hero have no buff\n\nTip: Goto center strategy to get buff";
        }
        else {
            foreach (BuffStatusScriptableObject buff in p.buffHero)
            {
                buffTxt += buff.buffName + "\n";
                buffTxt += "\t" + buff.desc + "\n\n";
            }
        }
        

        buffTextArea.text = buffTxt;

        string skillTxt = "";
        foreach(HeroesSkill skill in p.heroData.skill)
        {
            skillTxt += skill.name + "\n";
            skillTxt += "\t" + skill.description + "\n\n";
        }

        skillTextArea.text = skillTxt;


        changeTabStatus();

        buffTextArea.transform.parent.GetComponent<VerticalLayoutGroup>().spacing += 0.01f;
        skillTextArea.transform.parent.GetComponent<VerticalLayoutGroup>().spacing += 0.01f;
        buffTextArea.transform.parent.GetComponent<VerticalLayoutGroup>().spacing += 0.01f;
        skillTextArea.transform.parent.GetComponent<VerticalLayoutGroup>().spacing += 0.01f;
    }

    public void changeTabStatus() {

        currentTab.text = "Status";

        buff.SetActive(false);
        skill.SetActive(false);
        status.SetActive(true);
    }
    public void changeTabSkill() {

        currentTab.text = "Skills";

        buff.SetActive(false);
        skill.SetActive(true);
        status.SetActive(false);
    }
    public void changeTabBuff() {

        currentTab.text = "Buff";

        buff.SetActive(true);
        skill.SetActive(false);
        status.SetActive(false);
    }

    public void NextTabPopUp()
    {
        if (currentTab.text == "Status")
        {
            changeTabSkill();
        }
        else if (currentTab.text == "Skills")
        {
            changeTabBuff();
        }
        else if (currentTab.text == "Buff")
        {
            changeTabStatus();
        }
    }

    public void PrevTabPopUp()
    {
        if (currentTab.text == "Status")
        {
            changeTabBuff();
        }
        else if (currentTab.text == "Skills")
        {
            changeTabStatus();
        }
        else if (currentTab.text == "Buff")
        {
            changeTabSkill();
        }
    }
    public void openPopUpStatus() {

        hideHintChara();
        hideHintBuilding();

        popUPStatus.SetActive(true);
        changeTabStatus();

        Animation animation = popUPStatus.GetComponent<Animation>();

        animation.Play("PopUpOpenAnim");

    }

    public void closePopUpStatus() {
        Animation animation = popUPStatus.GetComponent<Animation>();
        //animation.clip = animation.;
        animation.Play("PopUpCloseAnim");
        Invoke("setInAcvtivePopUp", (4f/6f));
    }

    public void openPopUpBuilding()
    {

        hideHintChara();
        hideHintBuilding();
        popUpBuilding.SetActive(true);
        changeTabStatus();

        Animation animation = popUpBuilding.GetComponent<Animation>();

        animation.Play("PopUpOpenBuildingAnim");

    }

    public void closePopUpBuilding()
    {
        Animation animation = popUpBuilding.GetComponent<Animation>();
        //animation.clip = animation.;
        animation.Play("PopUpCloseBuildingAnim");
        Invoke("setInAcvtivePopUpBuilding", (5f/6f));
    }


    void setInAcvtivePopUp()
    {
        popUPStatus.SetActive(false);
    }
    void setInAcvtivePopUpBuilding()
    {
        popUpBuilding.SetActive(false);
    }



    public void UpdateTime(int timeleft) {

        if (timeleft < 60)
        {
            int sec2 = timeleft % 60;
            if (sec2 >= 10)
            {
                this.timeLeft.text = "00:" + sec2.ToString();
            }
            else
            {
                this.timeLeft.text = "00:0" + sec2.ToString();
            }
            return;
        }

        int minute = timeleft / 60;
        int sec = timeleft % 60;

       

        if (sec >= 10) {
            this.timeLeft.text = "0" + minute.ToString() + ":" + sec.ToString();
        }
        else {
            this.timeLeft.text = "0" + minute.ToString() + ":0"+sec.ToString();
        }
    }

    public void UpdatePoin(float current , float max , bool mine)
    {
        if (mine)
        {
            myFillPoin.fillAmount = Mathf.Min(current/max , 1);
            myTextPoin.text = current.ToString() + " / " + max.ToString();
        }
        else {
            enemyFillPoin.fillAmount = Mathf.Min(current / max, 1);
            enemyTextPoin.text = current.ToString() + " / " + max.ToString();
        }
    }




}
