﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    [Header("Autonomous Movement")]
    public bool automovement = true;
    [SerializeField] GameObject autonomousMachineHolder = null;
    IMoverCharacter autonomousMachine = null;

    [Header("Other")]
    public List<Pion> playerPion;
    public Color colorSelected;
    public Color colorNotSelected;
    List<Pion> _selectedPion = new List<Pion>();
    public List<Pion> selectedPion {
        get {
            return _selectedPion;
        }
    }

    public int selectedCount() {
        return selectedPion.Count;
    }

    public void clearSelection() {
        foreach (Pion p in selectedPion)
        {
            p.setNotSelected();
        }
        selectedPion.Clear();
    }
    public void addSelected(Pion p) {
        if(selectedPion.Contains(p)){
            return;
        }
        p.setSelected();
        selectedPion.Add(p);
    }

    private void Start()
    {
        if (automovement) {
            autonomousMachine = autonomousMachineHolder.GetComponent<IMoverCharacter>();
            InvokeRepeating("updateHeroesMovement", 7 , 1);
        }
    }

    public void updateHeroesMovement() {
        foreach (Pion p in playerPion) {
            if (p == null) {
                continue;
            }


            Debug.Log("Moving "+p.name+" : "+ (!p.automovement || p.isMoving || p.isExistTargetAround || p.isCapturing));
            if (!p.automovement || p.isMoving || p.isExistTargetAround || p.isCapturing) {
                continue;
            }

            Vector2 location = autonomousMachine.getNextMovementHero(p);
            //Debug.Log(location)
            if (!(location.x == -1 && location.y == -1))
            {
                if (p.heroData.heroesClass == HeroesClass.Archer || p.heroData.heroesClass == HeroesClass.Magician)
                {
                    p.MovePionTo(location , 1);
                }
                else {
                    p.MovePionTo(location);
                }
            }

        }
    }



}

public interface IMoverCharacter {
    Vector2 getNextMovementHero(Pion p);
}
