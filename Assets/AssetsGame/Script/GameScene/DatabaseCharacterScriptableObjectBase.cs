﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Database", menuName = "Character/List", order = 1)]
public class DatabaseCharacterScriptableObjectBase : ScriptableObject
{
    public GameObject prefabsPion;
    public List<PionData> infantry;

    public void setArmy(ref GameObject obj , ArmyType type, int index) {

        List<PionData> PD;
        //Pion temp;
        switch (type)
        {
            case ArmyType.Infantry:
                PD = infantry;
                //temp = new InfantryBasic();
                obj.AddComponent<InfantryBasic>();
                break;
            default:
                //temp = new Pion();
                PD = new List<PionData>();
                break;
        }


        if (index >= 0 && index < PD.Count)
        {  
            //obj.GetComponent<Pion>().ImplementPionData(infantry[index]);
            //Debug.Log(obj.GetComponent<Pion>().armyName);
        }
    }

    public int getIndexFromPrimary(ArmyType type , string name) {

        List<PionData> PD;
        switch (type) {
            case ArmyType.Infantry:
                PD = infantry;
                break;
            default:
                PD = new List<PionData>();
                break;
        }

        for (int i = 0; i < PD.Count; i++) {
            if (name == PD[i].armyPrimary) {
                return i;
            }
        }
        return -1;
    }

}

[System.Serializable]
public class PionData {
    public string armyPrimary;
    public string armyName;
    public ArmyType armyType;

    public int power;
    public float speed;
    public int range;

    public float attack;
    public float armor;
    public float magicArmor;

    public Sprite gameSprite;
    public UnityEngine.UI.Image displayImage;


}
