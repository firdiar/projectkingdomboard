﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public enum Gender { Male , Female }

public class MapGenerator : MonoBehaviourPunCallbacks {

    public static MapGenerator MAIN;

    [System.Serializable]
    public class TileDescription {
        public TileType type;
        public Sprite sprite;
        public string title;
        [TextArea]
        public string desc;

        public TileDescription cloneWithSprite(Sprite s) {
            TileDescription t = new TileDescription();
            t.type = type;
            t.sprite = s;
            t.title = title;
            t.desc = desc;
            return t;
        }
    }

    [SerializeField] public bool isTestMode = false;

    [Header("Network")]
    [SerializeField]public bool isOnline = true;
    [SerializeField]public PhotonView pv;

    [Header("Manager")]
    [SerializeField] public GameSceneManager manager;

    [Header("Map Data")]
    [SerializeField] int width = 0;
    [SerializeField] int height = 0;
    [SerializeField] int obstacleCount = 5;
    [SerializeField] float tileSize = 1;
    [SerializeField] float tileOffsetX = 0;
    [SerializeField] float tileOffsetY = 0;
    [SerializeField] float extraSizeCam = 0;
    [SerializeField] MapTile[,] maps = null;
    [SerializeField] GameObject fogPrefab = null;
    public DamagePopup textPopUpPrefabs = null;
    public PlayerManager armyParent = null;
    public Transform armyEnemyParent = null;
    [SerializeField] FogHandler fogParent = null;
    [SerializeField] int groundTileCount = 0;
    [SerializeField] Transform decoration = null;
    [SerializeField] List<TileDescription> tileDescription = new List<TileDescription>();


    [Header("ParticleDie")]
    [SerializeField] GameObject particleEffectDie = null;
    [SerializeField] AudioClip clipManDie = null;
    [SerializeField] AudioClip clipWomanDie = null;


    [Header("Color")]
    public Color colorNeutral = new Color();
    public Color colorEnemy = new Color();
    public Color colorMine = new Color();

    [Header("Audio")]
    public AudioSource audio;
    public AudioClip landmarkCaptured;
    public AudioClip gameWin;
    public AudioClip gameLose;
    public AudioClip gameDraw;

    [Header("Sprite Asset")]
    [SerializeField] Sprite groundOdd = null;
    [SerializeField] Sprite groundEven = null;
    [SerializeField] Sprite lake = null;
    [SerializeField] Sprite crack = null;
    [SerializeField] Sprite landmark = null;
    [SerializeField] Sprite landmarkEnemies = null;
    [SerializeField] Sprite landmarkOwned = null;
    [SerializeField] Sprite[] wall = null;
    [SerializeField] Sprite hospital = null;
    [SerializeField] Sprite centerStrategy = null;
    [SerializeField] Sprite embassy = null;
    float startX;
    float startY;

    float heights ;
    float widths ;

    List<MapTile> landmarkTile = new List<MapTile>();

    #region limitMap
    public float minX() {
        float heightCam = 2f * Camera.main.orthographicSize;
        float widthCam = heightCam * Camera.main.aspect;
        return -(startX * tileSize) + widthCam / 2 - tileOffsetX - extraSizeCam ;
    }
    public float maksX() {
        float heightCam = 2f * Camera.main.orthographicSize;
        float widthCam = heightCam * Camera.main.aspect;
        //Debug.Log(startX + " - " + widthCam+" - "+ heightCam + " - "+ Camera.main.aspect);
        return (startX* tileSize) - widthCam / 2 + tileOffsetX + extraSizeCam;
    }

    public float minY()
    {
        float heightCam = 2f * Camera.main.orthographicSize;
        return -(startY*tileSize) + heightCam/2 - tileOffsetY - extraSizeCam;
    }

    public float maksY()
    {
        float heightCam = 2f * Camera.main.orthographicSize;
        return (startY* tileSize) - heightCam / 2 + tileOffsetY + extraSizeCam;
    }
    #endregion


    public void spawnParticleDie(Gender gender , Vector2 pos) {
        GameObject particle = Instantiate(particleEffectDie, pos, Quaternion.identity);
        AudioSource aus = particle.GetComponent<AudioSource>();
        if (gender == Gender.Male)
        {
            aus.clip = clipManDie;
        }
        else {
            aus.clip = clipWomanDie;
        }
        aus.Play();
        
    }

    public Sprite getGroundSprite(int x , int y) {
        return (((x + y) % 2 == 0) ? groundEven : groundOdd);
    }

    public TileDescription convertTileToDescription(MapTile tile) {

        foreach (TileDescription td in tileDescription) {
            if (td.type == tile.type) {
                if (td.type == TileType.WALL)
                {
                    return td.cloneWithSprite(tile.getAddonSprite());
                }
                else if ( td.sprite == tile.getAddonSprite())
                {
                    return td;
                }
                
            }
        }

        return null;
    }

    public void playSoundLandmarkCaptured() {
        audio.clip = landmarkCaptured;
        audio.Play();
    }

    public void playSoundVictory()
    {
        audio.clip = gameWin;
        audio.Play();
    }

    public void playSoundLose()
    {
        audio.clip = gameLose;
        audio.Play();
    }

    public void playSoundDraw()
    {
        audio.clip = gameDraw;
        audio.Play();
    }

    // Use this for initialization
    public void GenerateMap() {



        MAIN = this;


        
        heights = Camera.main.orthographicSize * 2.0f;
        widths = heights * Screen.width / Screen.height;

        startX = width / (2 * 1f) - tileOffsetX;
        startY = height / (2 * 1f) - tileOffsetY;

        maps = new MapTile[width, height];
        spawnFogParticle();

        if (!isOnline) {
            generateTileMap();
            return;
        }

        if (!PhotonNetwork.IsMasterClient)
        {
            pv.RPC("clientReady", RpcTarget.MasterClient);
            decoration.Rotate(new Vector3(0, 0, 180));
            //generateTileMap();
            //GenerateMapAddons();
        }
#if UNITY_EDITOR
        //Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        else if(PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            generateTileMap();
        }
#endif
    }

    void generateTileMap()
    {

        for (int i = 0; i < width; i++)
        {
            //maps.Add(new List<MapTile>());

            for (int j = 0; j < height; j++)
            {
                float posX = (i - startX) * tileSize;
                float posY = (j - startY) * tileSize;
                string path = "GamePrefabs/";

                if (isOnline)
                {
                    PhotonNetwork.Instantiate(path + "TileMap", new Vector3(posX, posY, 0), Quaternion.identity);
                }
                else {
                    GameObject tileTemp = Instantiate(Resources.Load<GameObject>(path + "TileMap"), new Vector3(posX, posY, 0), Quaternion.identity);
                    MapTile tile = tileTemp.GetComponent<MapTile>();
                    Vector2 index = getMapTileIndexFromWorld(tileTemp.transform.position);
                    tile.x = (int)index.x;
                    tile.y = (int)index.y;

                    

                    addMapTile((int)index.x, (int)index.y, tile);

                    tileTemp.name += " (" + (int)index.x + "," + (int)index.y + ")";

                    tile.setBaseGroundSprite((int)index.x, (int)index.y);

                    tile.rotateAddon(new Vector3(0, 0, 180));

                }
               
                
            }
        }

        InvokeRepeating("GenerateBuilding", 10, 30);

    }

    void GenerateBuilding() {
        // 0 == hospital  , 1 == center strategy , 2 == embassy

        Debug.Log("Generate Building");

        int spawn = 0;
        if (manager.timePassed > 50)
        {
            spawn = Random.RandomRange(0, 3);
        }
        else {
            spawn = Random.RandomRange(0, 2);
        }
        TileType type = TileType.HOSPITAL;
        switch (spawn) {
            case 0:
                
                type = TileType.HOSPITAL;
                break;
            case 1:
               
                type = TileType.CENTER_STRATEGY;
                break;
            case 2:
                
                type = TileType.EMBASSY;
                break;
        }
        int x = Random.Range(0, width);
        int y = Random.Range(4, height-4);
        if (maps[x, y].type != TileType.GROUND)
        {
            return;
        }
        if (isOnline)
        {
            pv.RPC("setAsBuilding", RpcTarget.All, x, y, type);
        }
        else {
            setAsBuilding(x, y, type);
        }

    }

    void GenerateMapAddons() {

        //Generate Landmark
        int xTemp = Random.RandomRange(1 , width-1);
        Vector2 mirror = getMirrorPosition(xTemp, 4);
        if (isOnline)
        {
            pv.RPC("setLandmark", RpcTarget.Others, xTemp, 4);
            pv.RPC("setLandmark", RpcTarget.Others, (int)mirror.x, (int)mirror.y);
            pv.RPC("setLandmark", RpcTarget.Others, 6, 10);
        }
        setLandmark(xTemp, 4);
        setLandmark((int)mirror.x, (int)mirror.y);
        setLandmark(6, 10);

        //Generate Obstacle
        for (int i = 0; i < obstacleCount; i++)
        {
            int x = Random.Range(0, width);
            int y = 3 + Random.Range(0, (height / 2) - 3);
            Debug.Log("set obstacle " + x + " - " + y);
            if (maps[x,y].type != TileType.GROUND)
            {
                continue;
            }
            bool iscrack = System.Convert.ToBoolean(Random.Range(0, 2));
            if (isOnline)
            {
                pv.RPC("setAsObstacle", RpcTarget.Others, x, y, iscrack);
            }
            setAsObstacle(x, y , iscrack);
        }
        if (isOnline)
        {
            pv.RPC("GenerateArmy", RpcTarget.Others);
        }
        GenerateArmy();

    }







    int mapCount = 0;

    public void addMapTile(int x , int y , MapTile tile) {
        mapCount++;
        maps[x, y] = tile;
        tile.transform.SetParent(this.transform);
        
        if (mapCount == maps.Length) {

            if (!isOnline) {
                GenerateMapAddons();
                return;
            }

            Debug.Log("Map Ready");
            if (!PhotonNetwork.IsMasterClient)
            {
                pv.RPC("clientReady2", RpcTarget.MasterClient);
            }
#if UNITY_EDITOR
            else if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                GenerateMapAddons();
            }
#endif
        }

    }

    void spawnFogParticle() {
        /*int posX = 1;
        int posY = 1;
        for (int i = 0; i < width/2; i ++)
        {
            for (int j = 0; j < height/4; j ++)
            {
                

                //Debug.Log(posX + " - "+posY + " - "+ width / 2f);
                float X = (posX - startX) * tileSize;
                float Y = (posY - startY) * tileSize;

                GameObject temp = Instantiate(fogPrefab, new Vector2((int)X, (int)Y), Quaternion.identity, fogParent.transform);
                fogParent.particle.Add(temp.GetComponent<ParticleSystem>());

                if (posX == width - 2)
                {
                    posX = 1;
                    posY += 3;
                }
                else {
                    posX += 3;
                }
                if (posX == Mathf.Ceil(width / 2f)) {
                    posX++;
                }

                if (posY == Mathf.Ceil(height / 2f)) {
                    posY++;
                }
                Debug.Log("Posx : " + posX + " - " + Mathf.Ceil(width / 2f));

            }
        }*/
    }


    public MapTile getMapTile(int x, int y) {
        return maps[x, y];
    }

    #region PunRPC
    [PunRPC]
    public void clientReady() {
        generateTileMap();
    }
    [PunRPC]
    public void clientReady2()
    {
        GenerateMapAddons();
    }

    [PunRPC]
    public void setAsObstacle(int x , int y , bool isLake) {

        maps[x,y].setCanMove((isLake ? lake : crack), TileType.OBSTACLE);
        Vector2 mirror = getMirrorPosition(x, y);
        maps[(int)mirror.x,(int)mirror.y].setCanMove((isLake ? lake : crack), TileType.OBSTACLE);
    }

    [PunRPC]
    public void setAsBuilding(int x, int y, TileType type )
    {
        switch (type) {
            case TileType.CENTER_STRATEGY:
                maps[x, y].setCanMove(centerStrategy, type);
                break;
            case TileType.HOSPITAL:
                maps[x, y].setCanMove(hospital, type);
                break;
            case TileType.EMBASSY:
                maps[x, y].setCanMove(embassy, type);
                break;
        }

        
    }

    [PunRPC]
    public void setLandmarkOwnerSprite(int x, int y , TileOwner owner)
    {
        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (owner == TileOwner.MASTER)
        {
            if (isMaster)
            {
                maps[x,y].setSprite(landmarkOwned);
            }
            else
            {
                maps[x,y].setSprite(landmarkEnemies);
            }

        }
        else if (owner == TileOwner.CLIENT)
        {
            if (isMaster)
            {
                maps[x,y].setSprite(landmarkEnemies);
            }
            else
            {
                maps[x,y].setSprite(landmarkOwned);
            }
        }
        else if (owner == TileOwner.NEUTRAL)
        {
            maps[x,y].setSprite(landmark);
        }
    }
    [PunRPC]
    public void setLandmark(int x  , int y)
    {
        
        maps[x, y].setCanMove(landmark, TileType.LANDMARK);
        landmarkTile.Add(maps[x, y]);
        generateWall(x, y);
        
    }

    [PunRPC]
    public void destroyAddonMapTile(int x, int y)
    {
        maps[x,y].setCanMove( null, TileType.GROUND);
    }


    DeputyHero getDeputy()
    {

        List<DeputyHero> heroes = new List<DeputyHero>();

        string path = "deputyData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        string json = RoyaleHeroesLibs.loadFileFromName(path);
        if (json != null)
        {
            heroes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DeputyHero>>(json);
        }
        else
        {
            heroes = new List<DeputyHero>();
        }

        foreach (DeputyHero dh in heroes) {
            if (dh.isCurrentDeputy) {
                return dh;
            }
        }
        return null;

    }


    [PunRPC]
    public void GenerateArmy()
    {
        Debug.Log("Generating army");
        Debug.Log("Generating army");
        BlockData[,] data = loadLineUp();

        DeputyHero currentDeputy = getDeputy();

        for (int x = 0; x < 13; x++) {
            for (int y = 0; y < 3; y++) {
                if (data[x, y].hero != "") {

                    Vector3 position;

                    bool isMaster = isOnline ? PhotonNetwork.IsMasterClient : true;

                    if (!isMaster)
                    {
                        Vector2 mirror = getMirrorPosition(x, y);
                        position = maps[(int)mirror.x,(int)mirror.y].transform.position;
                    }
                    else {
                        position = maps[x,y].transform.position;
                    }

                    //maps[x][y].setCanMove(true, landmarkOwned, TileType.LANDMARK);
                    GameObject obj =null;
                    if (isOnline)
                    {
                        obj = PhotonNetwork.Instantiate("HeroPrefabs/" + data[x, y].hero, position, Quaternion.identity);
                    }
                    else {
                        obj = Instantiate( Resources.Load<GameObject>( "HeroPrefabs/" + data[x, y].hero) , position, Quaternion.identity, armyParent.transform);

                        Pion tempP = obj.GetComponent<Pion>();
                        armyParent.playerPion.Add(tempP);
                        tempP.setMapColorIndicator(  new Color(0f, 0.2f, 0.8f));
                        tempP.InvokeRepeating("checkDeactiveBuff", 0, 0.5f);
                        getMapTileFromWorld(obj.transform.position).setCurrentPion(tempP);




                    }
                    


                    Pion p = obj.GetComponent<Pion>();
                    int count = data[x, y].getCountArmy();

                    if (isOnline)
                    {
                        p.pv.RPC("setTotalArmy", RpcTarget.Others, count);
                    }

                    p.setTotalArmy(count);
                    Debug.Log(data[x, y].hero+"  -  "+data[x, y].heroClass + " - "+count);

                    if (currentDeputy.heroName == data[x, y].hero)
                    {
                        p.initHero(data[x, y].getTotalStatus(), 5);
                    }
                    else {
                        p.initHero(data[x, y].getTotalStatus(), Account.CurrentAccountData.getHeroes(data[x, y].hero, data[x, y].heroClass).level);
                    }

                    
                    //RoyaleHeroesLibs.getHeroesData(data[x, y].hero);



                }
            }
        }

        Debug.Log("Army generated");

        if (!isOnline) {
            GameStart();
            return;
        }

        if (!PhotonNetwork.IsMasterClient) {
            pv.RPC("GameStart", RpcTarget.All);
        }
#if UNITY_EDITOR
        //Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        else if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            GameStart();
        }
#endif

        
    }

    int getHeroesLevelFromList(HeroesLevelStoryData[] heroesLevel , string heroName) {
        foreach (HeroesLevelStoryData hlsd in heroesLevel) {
            if (hlsd.heroName == heroName) {
                return hlsd.heroLevel;
            }
        }
        return 1;
    }

    public List<Pion> generateEnemyArmy(BlockData[,] dataLineUp , HeroesLevelStoryData[] heroesLevel) {

        List<Pion> result = new List<Pion>();

        for (int x = 0; x < 13; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                if (dataLineUp[x, y].hero == ""){ continue; }

                Vector3 position;

                
                // Mirror Line UP
                Vector2 mirror = getMirrorPosition(x, y);
                position = maps[(int)mirror.x, (int)mirror.y].transform.position;

                
                GameObject obj = null;
                Pion p = null;
                if (!isOnline)
                {
                    obj = Instantiate(Resources.Load<GameObject>("HeroPrefabs/" + dataLineUp[x, y].hero), position, Quaternion.identity,armyEnemyParent);

                    p = obj.GetComponent<Pion>();
                    result.Add(p);
                    p.setMapColorIndicator(new Color(0.8f, 0.1f, 0.1f));
                    p.InvokeRepeating("checkDeactiveBuff", 0, 0.5f);
                    getMapTileFromWorld(obj.transform.position).setCurrentPion(p);

                }



                //Pion p = obj.GetComponent<Pion>();
                int count = dataLineUp[x, y].getCountArmy();

                p.setTotalArmy(count);
                p.initHero(dataLineUp[x, y].getTotalStatus(), getHeroesLevelFromList(heroesLevel , dataLineUp[x, y].hero) , false);


            }
        }

        return result;
    }



    [PunRPC]
    public void moveReport(Vector2 position, int heroID)
    {
        PhotonView v = PhotonView.Find(heroID);
        if (v == null)
        {
            return;
        }

        Debug.Log("Hero id : " + heroID + " ismoving to " + position);
        moveReport(position, v.GetComponent<Pion>());
        /*
        Pion temp = v.GetComponent<Pion>();
        foreach (Pion p in armyParent.GetComponent<PlayerManager>().playerPion) {
            if (p.isInRange((int)position.x, (int)position.y)) {
                p.addTargetAttack(temp);
            }
        }*/

    }

    public void moveReport(Vector2 position, Pion pion) {
        //Pion temp = v.GetComponent<Pion>();
        if (isOnline)
        {
            foreach (Pion p in armyParent.GetComponent<PlayerManager>().playerPion)
            {
                if (p.isInRange((int)position.x, (int)position.y) && p != pion)
                {
                    p.addTargetAttack(pion);
                }
            }
        }
        else {

            if (pion.transform.parent == armyEnemyParent || isTestMode) // if the pion is Enemy
            {
                foreach (Pion p in armyParent.GetComponent<PlayerManager>().playerPion)
                {
                    if (p.isInRange((int)position.x, (int)position.y) && p != pion)
                    {
                        p.addTargetAttack(pion);
                    }
                }
            }
            if(pion.transform.parent != armyEnemyParent) { // if the pion is mine
                foreach (Transform t in armyEnemyParent) {
                    Pion p = t.GetComponent<Pion>();
                    if (p.isInRange((int)position.x, (int)position.y) && p != pion)
                    {
                        p.addTargetAttack(pion);
                    }
                }
            }

        }
    }

    [PunRPC]
    public void GameStart()
    {
        Debug.Log("Game Started!");
        
        //Camera.main.GetComponent<CameraFollowTarget>().addList(armyParent.playerPion , ()=> {
        //    manager.pv.RPC("setPlayerReady", RpcTarget.All);
        //});
        manager.GameStart();

    }

    #endregion

    public Pion getPlayerOnPosition(int x, int y) {

        foreach (Pion p in armyParent.playerPion) {
            Vector2 pos = p.getCurrentTile();
            if (x == pos.x && y == pos.y) {
                return p;
            }
        }
        return null;
    }

    public int getPlayerLandmarkCount() {
        int count = 0;
        foreach (MapTile tile in landmarkTile) {
            
            if (tile.owner == TileOwner.NEUTRAL) {
                continue;
            }
            bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
            
            if ((tile.owner == TileOwner.MASTER) == isMaster)
            {
                count++;
            }
            
        }
        return count;
    }
    public int getEnemyLandmarkCount()
    {
        int count = 0;
        foreach (MapTile tile in landmarkTile)
        {
            if (tile.owner == TileOwner.NEUTRAL)
            {
                continue;
            }

            bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
            if ((tile.owner == TileOwner.MASTER) != isMaster)
            {
                count++;
            }
        }
        return count;
    }


    public Transform getEnemyArmyParent(Transform playerParent) {
        if (playerParent == armyEnemyParent)
        {
            return armyParent.transform;
        }
        else {
            return armyEnemyParent;
        }
    }

    public List<MapTile> getLandmarkPosition() {
        return landmarkTile;
    }

    public List<Vector2> getPlayerLandmarkPosition()
    {
        List<Vector2> pos = new List<Vector2>();
        foreach (MapTile tile in landmarkTile)
        {
            if (tile.owner == TileOwner.NEUTRAL)
            {
                continue;
            }
            else
            if ((tile.owner == TileOwner.MASTER) == PhotonNetwork.IsMasterClient)
            {
                pos.Add(tile.transform.position);
            }
        }
        return pos;
    }

    public Pion getPionFromID(Transform parent, int id) {

        foreach (Transform t in parent) {
            if (id == t.GetComponent<Pion>().pv.ViewID) {
                return t.GetComponent<Pion>();
            }
        }

        return null;
    }

    public BlockData[,] loadLineUp()
    {
        string path = "lineUpData-" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId + ".json";

        string json = RoyaleHeroesLibs.loadFileFromName(path);
        Debug.Log(json);
        if (json == null)
        {
            return null;
        }
        else
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<BlockData[,]>(json);
        }

    }

    public void generateWall(int centerX, int centerY) {

        maps[centerX + 1, centerY].setCanMove(wall[0], TileType.WALL);
        maps[centerX - 1, centerY].setCanMove(wall[0], TileType.WALL);

        maps[centerX, centerY + 1].setCanMove(wall[3], TileType.WALL);
        maps[centerX, centerY - 1].setCanMove(wall[3], TileType.WALL);

        bool isMaster = isOnline ? PhotonNetwork.IsMasterClient : true;
        if (!isMaster)
        {
            maps[centerX + 1, centerY + 1].setCanMove(wall[1], TileType.WALL);
            maps[centerX - 1, centerY - 1].setCanMove(wall[4], TileType.WALL);

            maps[centerX + 1, centerY - 1].setCanMove(wall[5], TileType.WALL);
            maps[centerX - 1, centerY + 1].setCanMove(wall[2], TileType.WALL);
        }
        else {

            maps[centerX + 1, centerY + 1].setCanMove(wall[4], TileType.WALL);
            maps[centerX - 1, centerY - 1].setCanMove(wall[1], TileType.WALL);

            maps[centerX + 1, centerY - 1].setCanMove(wall[2], TileType.WALL);
            maps[centerX - 1, centerY + 1].setCanMove(wall[5], TileType.WALL);
        }

        

    }
    public Vector2 getMirrorPosition(int x , int y) {
        float newX = width - x;
        float newY = height - y;
        return new Vector2(newX-1, newY-1);
    }



    public List<Vector2> getAvailableAround(int x , int y , int minX , int maxX , int minY , int maxY) {
        List<Vector2> around = new List<Vector2>();

        for(int i=-1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (Mathf.Abs( j )== Mathf.Abs( i )) {
                    continue;
                }
                Vector2 v = new Vector2(x + i, y + j);
                if (!(v.x < minX || v.x > maxX || v.y < minY || v.y > maxY ))
                {
                    if (!maps[(int)v.x,(int)v.y].canMove)
                    {
                        around.Add(v);
                    }
                }
            }
        }

        return around;
    }
    


    public Vector2 getMapTileIndexFromWorld(Vector2 pos) {
        int x = (int)Mathf.Round((pos.x / tileSize) + startX);
        int y = (int)Mathf.Round((pos.y / tileSize) + startY);
        
        return new Vector2(x, y);
    }
    public MapTile getMapTileFromWorld(Vector2 pos) {
        //Vector2 index = getMapTileIndexFromWorld(pos);
        //Debug.Log(index);
        return getMapTileFromIndex(getMapTileIndexFromWorld(pos));
    }
    public MapTile getMapTileFromIndex(Vector2 pos)
    {
        if ((pos.x < 0 || pos.x >= width) || (pos.y < 0 || pos.y >= height))
        {

            //Debug.Log("overflow selection tile "+pos);
            return null;
        }
        return maps[(int)pos.x,(int)pos.y];
    }


    public Vector2 getMapWorldPosition(int x, int y) {
        if ((x < 0 || x >= width) || (y < 0 || y >= height))
        {

            Debug.Log("overflow selection tile " + x+" - "+y);
            return Vector2.one*-1;
        }
        return maps[x,y].transform.position;
    }
    public Vector2 getMapWorldPosition(Vector2 pos)
    {
        return maps[(int)pos.x,(int)pos.y].transform.position;
    }
    

    //Mendapatkan step step dari start menuju ke target
    public List<Vector2> findPath(Vector2 start, Vector2 target) {
        List<Vector2> result = new List<Vector2>();
        Debug.Log("finding Path");
        //result.Add(start);
        //BacktrackMap[,] bMap = new BacktrackMap[width,height];
        /*for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                bMap[i, j] = new BacktrackMap();
                bMap[i, j].isVisited = maps[i][ j].isOccupied || !maps[i][j].canMove;
            }
        }*/
        string idSearch = RoyaleHeroesLibs.RandomString(6);
        searchBacktrack(result, start, target, maps, idSearch);
        result.Reverse();
        //for (int i = 0; i < result.Count; i++)
        //{
        //    Debug.Log(result[i]);
        //}

        return result;
    }
    //fungsi backtracking mencari path menuju target
    public bool searchBacktrack(List<Vector2> result , Vector2 current  , Vector2 target , MapTile[,] bMap , string idSearch) {
        
        //int idx = (result.Count - 1);
        if (current == target) {
            result.Add(current);
            return true;
        }
        bool canMove = false;

        List<Vector2> around = aroundNearest(current, target);

        for (int i = 0; i < around.Count; i++) {
            //searchBacktrack(arou)

            if (  (around[i].x < 0 || around[i].x >= width)   ||   (around[i].y < 0 || around[i].y >= height)   ||   bMap[(int)around[i].x, (int)around[i].y].isVisited(idSearch) || !bMap[(int)around[i].x, (int)around[i].y].canMove || bMap[(int)around[i].x, (int)around[i].y].isOccupied) 
            {
                continue;
            } 

            bMap[(int)around[i].x,(int)around[i].y].idSearch = idSearch;
            canMove = searchBacktrack(result , around[i] , target , bMap , idSearch);
            if (canMove)
            {
                result.Add(current);
                break;
            }
        }

        return canMove;

    }
    // mendapatkan sekeliling
    public List<Vector2> aroundNearest(Vector2 start, Vector2 target) {

        List<Vector2> around = new List<Vector2>();
        around.Add(start + new Vector2(0, 1));//atas
        around.Add(start + new Vector2(0, -1));//bawah
        around.Add(start+new Vector2(1,0));//kanan
        around.Add(start + new Vector2(-1, 0));//kiri

        //around.Add(start + new Vector2(1, 1));//kanan atas
        //around.Add(start + new Vector2(-1, 1));//kiri atas
        //around.Add(start + new Vector2(-1, -1));//kiri bawah
        //around.Add(start + new Vector2(1, -1));//kanan bawah

        around.Sort(delegate (Vector2 a, Vector2 b) {
            float val1 = Vector2.Distance(a, target);
            float val2 = Vector2.Distance(b, target);
            return val1.CompareTo(val2);
        });

        return around;
    }

    public bool isLake(MapTile tile) {
        if (tile != null && tile.type == TileType.OBSTACLE && tile.getAddonSprite() == lake) {
            return true;
        }
        return false;
    }

    // mendapatkan sekeliling
    public Vector2 aroundNearestNotOccupied(Vector2 start , int prefer=1)
    {
        for (int z = 0; z < height; z++)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {

                    Vector2 around = start + (new Vector2(i * -1, j * prefer) * z);
                    Vector2 around2 = start + (new Vector2(i, j * prefer) * z);
                    MapTile m1 = getMapTileFromIndex(around);
                    MapTile m2 = getMapTileFromIndex(around2);
                    if (m1 == null || m1.isOccupied || !m1.canMove)
                    {
                        if (m2 == null)
                        {
                            break;
                        }
                        else if (m2.isOccupied || !m2.canMove)
                        {
                            continue;
                        }
                        else
                        {
                            return around2;
                        }
                    }
                    else
                    {
                        return around;
                    }
                }
            }
        }


        return start;
    }

    
    
}

public class BacktrackMap {
    public bool isVisited = false;
}



