﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogHandler : MonoBehaviour
{

    public List<ParticleSystem> particle = new List<ParticleSystem>();
    public List<bool> isNear = new List<bool>();
    [SerializeField] PlayerManager pm = null;
    [SerializeField] float minDist = 17;

    public void Start()
    {
        InvokeRepeating("detect", 1, 1.5f);
    }

    void detect() {
        if (MapGenerator.MAIN != null && MapGenerator.MAIN.getPlayerLandmarkCount() > 0)
        {
            
            checkWithLandmark();
        }
        else {
            checkWithoutLandmark();
        }
            
    }
    void checkWithLandmark() {
        foreach (ParticleSystem par in particle)
        {
            bool action = false;
            foreach (Pion p in pm.playerPion)
            {
                action = isStopParticle(p.transform.position, par.transform.position, par);
                if (action)
                {
                    if (par.isPlaying) {
                        par.Stop();
                    }
                    break;
                }
            }
            if (action) {
                continue;
            }
            List<Vector2> lan = MapGenerator.MAIN.getPlayerLandmarkPosition();
            foreach (Vector2 landmark in lan)
            {
                action = isStopParticle(landmark, par.transform.position, par);
                if (action)
                {
                    if (par.isPlaying)
                    {
                        par.Stop();
                    }
                    break;
                }
            }
            //Debug.Log("Action : "+action+" isplaying : "+par.isPlaying);
            if (!action && !par.isPlaying)
            {
                par.Play();
            }
        }
    }

    void checkWithoutLandmark() {
        foreach (ParticleSystem par in particle)
        {
            bool action = false;
            foreach (Pion p in pm.playerPion)
            {
                if (p == null) {
                    Debug.LogError("PION NULL!!");
                    continue;
                }
                //float dist = Vector2.Distance(p.transform.position, par.transform.position);
                action = isStopParticle(p.transform.position, par.transform.position, par);
                if (action)
                {
                    if (par.isPlaying)
                    {
                        par.Stop();
                    }
                    break;
                }
            }
            if (!action && !par.isPlaying) {
                par.Play();
            }
        }
    }
    bool isStopParticle(Vector2 a, Vector2 b, ParticleSystem par)
    {

        float dist = Vector2.Distance(a, b);
        //float x = Mathf.Abs(a.x - b.x);
        //float y = Mathf.Abs(a.y - b.y);
        //Debug.Log("x : " + x + " y : " + y);
        if (dist <= minDist)
        {
            //stop particle
            //par.Stop();
            return true;
        }
        else {
            return false;
        }

    }
    
}
