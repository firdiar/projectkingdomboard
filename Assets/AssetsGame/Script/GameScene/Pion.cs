﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(PhotonView))]
[RequireComponent(typeof(Animator))]
public class Pion : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback, IPunObservable
{

    public static float NORMALIZE_BATTLE_FACTOR = 1.18f;
    public static float NORMALIZE_ARMY_DIE_PHY = 0.2f;
    public static float NORMALIZE_ARMY_DIE_MAG = 0.95f;

    public bool automovement = true;

    public Gender gender;
    public Animator anim;
    public HeroesDataScriptableObject heroData;

    public Sprite border {
        set {
            ImageBorder.sprite = value;
        }
    }

    public int levelHero = 0;
    CharacterStatus status = null;
    public int totalArmyCount = 0;
    int _actualArmyCount = 0;

    public int exploaration = 0;

    public int actualArmyCount {
        get {
            return _actualArmyCount;
        }
        set {

            _actualArmyCount = value;

            if (isMine) {
                existChange = true;
            }
            
        }
    }
    float attackCD = 0;


    [SerializeField] SpriteRenderer ImageBorder;
    [SerializeField] SpriteRenderer heroIcon;
    [SerializeField] Transform hpBar;
    float maxFloatHp = 3.7f;

    [SerializeField] GameObject selectedSprite;
    [SerializeField]public PhotonView pv;
    [SerializeField] SpriteRenderer mapIndicator;
    bool existChange = true;

    protected List<Pion> _targetAttack = new List<Pion>();
    public List<BuffStatusScriptableObject> buffHero = new List<BuffStatusScriptableObject>();

    bool _isMoving = false;
    public bool isMoving {
        get { return _isMoving; }
        set {
            _isMoving = value;
            Debug.Log("is MOving "+value);
            if (value == true) {
                buffHero.RemoveAll(item => (item.caseDeactive == BuffStatusScriptableObject.DeactiveCase.MOVING));
            }
        }
    }
    List<Vector2> moveStep;

    public void addTargetAttack(Pion p) {
        if (_targetAttack.Contains(p))
        {
            return;
        }
        else {
            _targetAttack.Add(p);
        }
    }
    bool isShaking = false;
    float shakeDuration = 0;
    public void Shake(float duration) {
        isShaking = true;
        shakeDuration = duration;
    }

    public string groupName {
        get {
            return heroData.nick+"'s "+RoyaleHeroesLibs.getGroupName(totalArmyCount);
        }
    }

    Coroutine movingCoroutine = null;

    [Header("Audio")]
    [SerializeField] protected AudioSource audios = null;
    [SerializeField] AudioClip movementClip = null;
    [SerializeField] AudioClip buffedClip = null;
    [SerializeField] AudioClip debuffedClip = null;

    [PunRPC]
    public void setTotalArmy(int total) {
        totalArmyCount = total;
        actualArmyCount = total;
        setBarHP();
        
    }

    bool isStopped = false;
    public void StopGame() {
        isStopped = true;
    }

    public void initHero(CharacterStatus status , int level , bool changeBorder = true) {
        this.status = status;
        this.levelHero = level;

        if (changeBorder)
        {
            ImageBorder.sprite = RoyaleHeroesLibs.getLevelBorder(level);
        }

        if (isMine)
        {
            FirstFunction();
        }

    }

    public void setMapColorIndicator(Color col) {
        mapIndicator.color = col;
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("Hero Sparned");
        if (!pv.IsMine)
        {
            transform.SetParent(MapGenerator.MAIN.armyEnemyParent);
            mapIndicator.color = new Color(0.8f , 0.1f , 0.1f);
        }
        else {
            transform.SetParent(MapGenerator.MAIN.armyParent.transform);
            MapGenerator.MAIN.armyParent.playerPion.Add(this);

            InvokeRepeating("checkDeactiveBuff", 0, 0.5f);
            mapIndicator.color = new Color(0f , 0.2f , 0.8f );
        }

        pv.ObservedComponents.Add(this);
        pv.ObservedComponents.Add(GetComponent<PhotonAnimatorView>());
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, (!PhotonNetwork.IsMasterClient ? 180 : 0)));
        MapGenerator.MAIN.getMapTileFromWorld(transform.position).setCurrentPion(this);
        

    }

    public virtual void FirstFunction() {

        updateFogOfWarAround();
    }

    public void spawn() {
        Debug.Log("spawn!");
    }

    public Vector2 getPos() {
        //Debug.Log("Getting Position");
        if (MapGenerator.MAIN == null) {
            return Vector2.zero;
        }
        return MapGenerator.MAIN.getMapTileIndexFromWorld(transform.position);
    }

    public void setSelected() {
        selectedSprite.SetActive(true);
    }
    public void setNotSelected()
    {
        selectedSprite.SetActive(false);
    }

    public Vector2 getCurrentTile() {

        if (transform == null)
            return Vector2.zero;

        if (MapGenerator.MAIN == null)
            return Vector2.zero;

        return MapGenerator.MAIN.getMapTileIndexFromWorld(transform.position);
    }

    

    public bool isCapturing {
        get {
            return anim.GetBool("isCapturing");
        }
    }

    public void occupiedCurrentTile() {

        if (MapGenerator.MAIN == null)
            return;


        Vector2 v = getCurrentTile();
        
        MapTile m = MapGenerator.MAIN.getMapTileFromIndex(v);
        if (m != null)
        {
            

            m.setCurrentPion(this);

            if (m.isCapturAble)
            {
                if (MapGenerator.MAIN.isOnline)
                {
                    m.startCapturing((PhotonNetwork.IsMasterClient ? TileOwner.MASTER : TileOwner.CLIENT));
                    anim.SetBool("isCapturing", true);
                }
                else {
                    m.startCapturing((transform.parent == MapGenerator.MAIN.armyParent.transform ? TileOwner.MASTER : TileOwner.CLIENT));
                    anim.SetBool("isCapturing", true);
                }
            }
            else if (m.type == TileType.WALL) {
                BuffStatusScriptableObject buff = RoyaleHeroesLibs.getBuffData("Wall Buff").clone();
                addBuff(buff);
            }

        }
        else {
            Debug.LogError("Error current tile is null");
        }
    }
    
    public void unOccupiedCurrentTile()
    {
        if (MapGenerator.MAIN == null)
            return;

        Vector2 v = getCurrentTile();
        Debug.Log(v);
        MapTile m = MapGenerator.MAIN.getMapTileFromIndex(v);
        if (m != null)
        {
            m.isOccupied = false;
        }
        else
        {
            Debug.LogError("Error current tile is null");
        }
    }

    public void SuccessCapture() {
        
        anim.SetBool("isCapturing", false);

        if (isMine && MapGenerator.MAIN != null) {
            MapGenerator.MAIN.manager.captureCount++;
        }
    }


    public void MovePionTo(Vector2 targetVector , int minus = 0) {
        if (MapGenerator.MAIN == null)
            return;

        List<Vector2> path = MapGenerator.MAIN.findPath(getPos(), targetVector);

        if (minus != 0 && path.Count > minus+1)
        {
            path.RemoveRange(path.Count - minus , minus);
        }

        Move(path);
    }

    public void Move(List<Vector2> moveStep)
    {
        
        unOccupiedCurrentTile();
        this.moveStep = moveStep;
        isMoving = true;
        if (movingCoroutine != null) {
            StopCoroutine(movingCoroutine);
        }

        audios.clip = movementClip;
        audios.Play();

        movingCoroutine = StartCoroutine(Moving());
    }


    public void StopMove()
    {
        isMoving = false;
        this.moveStep.Clear();
    }

    void updateFogOfWarAround(bool force = false) {

        Vector2 current = getCurrentTile();


        for (int i = 0; i <= 7; i++) {
            for (int j = 0; j <= 7; j++) {
                Vector2 temp = new Vector2(current.x-3+i, current.y-3+j);
                MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(temp);
                if (tile != null) {
                    tile.recalculateFog(force);
                }
            }
        }

        

    }

    IEnumerator Moving() {
        anim.SetBool("isCapturing", false);

        if (moveStep.Count > 1)
        {
            moveStep.RemoveAt(0);
        }
        else if (moveStep.Count == 0)
        {
            StopMove();
            Debug.Log("move step not found");
            yield return null;
        }

        Vector2 first = getCurrentTile();
        Vector2 currentPoint = first;

        if (moveStep.Count >= 1)
        {
            first = moveStep[0];
            currentPoint = first;
        }

        while (isMoving && moveStep.Count > 0 && MapGenerator.MAIN != null) {
            //EDITED
            
            MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(moveStep[0]);

            if (tile.isOccupied && moveStep[0] != getCurrentTile()) {
                if (moveStep.Count == 1)
                {
                    moveStep.Clear();
                    moveStep.Add(getCurrentTile());
                    continue;
                }
                else {
                    moveStep = MapGenerator.MAIN.findPath(getCurrentTile(), moveStep[moveStep.Count - 1]);
                    if (moveStep.Count > 1)
                    {
                        moveStep.RemoveAt(0);
                    }
                    else if (moveStep.Count == 0)
                    {
                        StopMove();
                        Debug.Log("move step not found");
                        yield return null;
                    }
                    continue;
                }
                
            }
            
            Vector2 target = MapGenerator.MAIN.getMapWorldPosition(moveStep[0]);

            //Update opacity of fog of war
            updateFogOfWarAround();

            transform.position = Vector2.MoveTowards(transform.position, target, 0.0004f * getMovementSpeed());
            if (Vector2.Distance(target, transform.position) < 0.01f) {
                currentPoint = moveStep[0];
                moveStep.RemoveAt(0);

                if (MapGenerator.MAIN != null && MapGenerator.MAIN.isOnline)
                {
                    MapGenerator.MAIN.pv.RPC("moveReport", RpcTarget.Others, getCurrentTile(), pv.ViewID);
                }
                else {
                    MapGenerator.MAIN.moveReport(getCurrentTile(), this);
                }
                moved();

                MapGenerator.MAIN.manager.exploredTile.Add(getCurrentTile());
                exploaration++;
            }
            yield return new WaitForSeconds(0.02f);
        }

        Vector2 targetNext = transform.position;
        //bool gotPrever = false;

        while (MapGenerator.MAIN != null && MapGenerator.MAIN.getMapTileFromIndex(getCurrentTile()).isOccupied)
        {

            //if (!isMoving || MapGenerator.MAIN.getMapTileFromIndex(getCurrentTile()).isOccupied)
            //{
            int prefer = (int)((currentPoint.y - first.y) / Mathf.Max(Mathf.Abs(currentPoint.y - first.y), 1)) * -1;
            Debug.Log("PREFER : " + prefer);
            targetNext = MapGenerator.MAIN.getMapWorldPosition(MapGenerator.MAIN.aroundNearestNotOccupied(currentPoint, prefer));
            //isMoving = false;
            //gotPrever = true;
            //}

            while (/*gotPrever && */Vector2.Distance(targetNext, transform.position) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetNext, 1);
                yield return new WaitForSeconds(0.02f);
            }

            updateFogOfWarAround();

            //gotPrever = false;
        }
        


        occupiedCurrentTile();
        if (MapGenerator.MAIN != null && MapGenerator.MAIN.isOnline)
        {
            MapGenerator.MAIN.pv.RPC("moveReport", RpcTarget.Others, getCurrentTile(), pv.ViewID);
        } else if (MapGenerator.MAIN != null) {
            MapGenerator.MAIN.moveReport(getCurrentTile(), this);
        }

        traceEnemyAround();

        isMoving = false;
    }

    public virtual void moved() {

    }


    public virtual void traceEnemyAround() {

        if (MapGenerator.MAIN == null)
        {
            return;
        }

        Vector2 myPos = getCurrentTile();
        //Debug.Log("My pos " + myPos+" range "+ getRange());
        int range = getRange();

        for (int i = -1 * range; i <= range; i++) {
            for (int j = -1 * range; j <= range; j++)
            {
                if ((i == 0 && j == 0) || MapGenerator.MAIN==null) {
                    continue;
                }

                //Debug.Log("Tracking " + i + " - " + j);
                MapTile tile = MapGenerator.MAIN.getMapTileFromIndex(new Vector2(i, j)+myPos);
                if ( tile != null && tile.isOccupied  ) {
                    Pion p = tile.getCurrentPion();
                    if (MapGenerator.MAIN.isOnline)
                    {
#if UNITY_EDITOR
                        //IMPORTANT!!
                        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                        {//&& !p.pv.IsMine) {

                            if (p != null)
                            {
                                _targetAttack.Add(p);
                            }
                        }
                        else
                        {
#endif
                            //IMPORTANT!!
                            if (p != null && !p.pv.IsMine)
                            {
                                _targetAttack.Add(p);
                            }
                            //IMPORTANT!!
#if UNITY_EDITOR
                        }
#endif
                    }else {

                        //IMPORTANT!!
                        if (MapGenerator.MAIN.isTestMode)
                        {//&& !p.pv.IsMine) {

                            if (p != null)
                            {
                                Debug.Log("Target Added : "+p.name);
                                _targetAttack.Add(p);
                            }
                        }
                        else
                        {

                            //IMPORTANT!!
                            if (p != null && p.transform.parent != transform.parent)
                            {
                                _targetAttack.Add(p);
                            }
                            //IMPORTANT!!

                        }
                    }

                }
            }
        }
    }

    public bool isMine {
        get {
            if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline) {
                return pv.IsMine;
            }
            
            return true;
            
        }
    }

    protected bool isAttacking = false;

    public virtual void Update()
    {

        if (isShaking) {
            if (shakeDuration <= 0)
            {
                isShaking = false;
                heroIcon.transform.localPosition = Vector2.zero;
            }
            else {
                shakeDuration -= Time.deltaTime;
                heroIcon.transform.localPosition = Random.insideUnitCircle * 0.05f;
            }
        }

        if (!isMine || isStopped) {
            return;
        }

        //Debug.Log(" Status Attacking : "+(_targetAttack.Count > 0) + " - " + (!isMoving )+ " - " + (!isAttacking) + " - " + (canAttack()));

        if (_targetAttack.Count > 0 && !isMoving && !isAttacking && canAttack())
        {
            updateCDTime(Time.deltaTime);
        }



        //checkDeactiveBuff(Time.deltaTime);

    }

    public virtual void updateCDTime(float deltaTime) {
        attackCD -= deltaTime;
        //Debug.Log("Attack CD : " + attackCD+" - "+ (isAttacking == false));
        if (attackCD < 0 && isAttacking == false)
        {
            
            Pion p = GetandCheckTarget();
            if (p != null)
            {
                //Debug.Log("Attacking Targets");
                isAttacking = true;
                Attack(p);
                attackCD = getAttackSpeed();
            }
            else
            {
                Debug.Log("Target Null");
            }
        }
    }


    //List<BuffStatusScriptableObject> removeBuffList = new List<BuffStatusScriptableObject>();
    public void checkDeactiveBuff() {

       // Debug.Log("Checking buff hero");

        if (buffHero.Count > 0)
        {
            
            foreach (BuffStatusScriptableObject buff in buffHero.ToArray())
            {
                if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.TIME)
                {
                    buff.limit -= 0.5f;
                    if (buff.limit <= 0)
                    {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
                else if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.UNITCOUNT_GREATHER_THAN)
                { // jika army kita lebih banyak dari seharusnya remove
                    if (buff.limit < actualArmyCount)
                    {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
                else if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.UNITCOUNT_LESS_THAN)
                { // jika army kita lebih sedikit dari seharusnya remove
                    if (buff.limit > actualArmyCount)
                    {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
                else if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.POWER_GREATHER_THAN)
                { // jika power kita lebih besar dari seharusnya remove
                    if (buff.limit < (actualArmyCount / (totalArmyCount * 1f)))
                    {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
                else if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.POWER_LESS_THAN)
                { // jika power kita lebih sedikit dari seharusnya remove
                    if (buff.limit > (actualArmyCount / (totalArmyCount * 1f)))
                    {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
                else if (buff.caseDeactive == BuffStatusScriptableObject.DeactiveCase.NEARBY_ENEMY_COUNT_NOT_EQUAL) {
                    if (buff.limit != _targetAttack.Count) {
                        buffHero.Remove(buff);
                        //removeBuffList.Add(buff);
                    }
                }
            }
            /*
            //remove buff
            if (removeBuffList.Count > 0) {
                foreach (BuffStatusScriptableObject buff in removeBuffList) {
                    bool sucess = buffHero.Remove(buff);
                    Debug.Log(name+" Removing buff : " + buff.buffName+" - "+sucess);
                    
                }
            }
            */
        }
    }

    public bool isInRange(int x, int y) {


        return isInRange(x,y, getRange());


    }

    public bool isInRange(int x, int y , int range)
    {

        Vector2 pos = getCurrentTile();

        int rangeX = (int)Mathf.Abs(pos.x - x);
        int rangeY = (int)Mathf.Abs(pos.y - y);
        if (rangeX <= range && rangeY <= range)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public bool isInRange(Vector2 pos)
    {

        return isInRange((int)pos.x, (int)pos.y);

    }


    public Pion GetandCheckTarget() {
        float min = 10000;
        Vector2 me = getCurrentTile();
        Pion result = null;

        _targetAttack.RemoveAll(item => {
            if (item == null) {
                return true;
            }
            else {
                return !isInRange(item.getCurrentTile());
            }
            
        });

        foreach (Pion p in _targetAttack) {
            Vector2 pos = p.getCurrentTile();
            float dist = Vector2.Distance(pos, me);
            if (dist < min) {
                min = dist;
                result = p;
            }
        }

        return result;
    }

    [PunRPC]
    public virtual void ImplementDamage(int PhyDamage , int MagDamage , int armyCount) {

        ImplementDamage(PhyDamage, MagDamage,armyCount ,0.3f);
    }


    [PunRPC]
    public virtual void ImplementDamage(int PhyDamage, int MagDamage, int armyCount, float shakeTime)
    {

        float normEnemy = 1;
        float normMe = 1;

        if (armyCount > actualArmyCount)//musuh lebih banyak dari kita
        {
            float val = 1 - Mathf.Sqrt(Mathf.Sqrt(Mathf.InverseLerp(100, 50000, armyCount) ));

            float mul = val * Pion.NORMALIZE_BATTLE_FACTOR;

            int NormArmy = Mathf.RoundToInt(Mathf.Min(actualArmyCount * (1 + mul), armyCount));

            normEnemy = NormArmy / (float)armyCount;
        }
        else if(armyCount < actualArmyCount){//kita lebih banyak dari musuh

            float val = 1 - Mathf.Sqrt(Mathf.Sqrt( Mathf.InverseLerp(100, 50000, actualArmyCount)));

            float mul = val * Pion.NORMALIZE_BATTLE_FACTOR;

            int NormArmy = Mathf.RoundToInt( Mathf.Min(armyCount * (1+mul), actualArmyCount) );

            normMe = NormArmy/(float)actualArmyCount;
        }

        

        int purePhyDeff = Mathf.RoundToInt( getPhyDeff() * normMe);
        int pureMagDeff = Mathf.RoundToInt(getMagDeff() * normMe);

        if (anim.GetBool("isCapturing")) {
            purePhyDeff = Mathf.RoundToInt(purePhyDeff * 0.5f);
            pureMagDeff = Mathf.RoundToInt(pureMagDeff * 0.5f);
        }

        PhyDamage = Mathf.RoundToInt(PhyDamage * normEnemy);
        MagDamage = Mathf.RoundToInt(MagDamage * normEnemy);

        int dmg1 = Mathf.Max(PhyDamage - purePhyDeff, Mathf.RoundToInt(PhyDamage * 0.05f));
        int dmg2 = Mathf.Max(MagDamage - purePhyDeff, Mathf.RoundToInt(MagDamage * 0.05f));


        int hp = getHealth();
        float percent = 1 - ((hp - (dmg1)) / (hp * 1f));
        float percent2 = 1 - ((hp - (dmg2)) / (hp * 1f));

        int armyDie = Mathf.FloorToInt(percent * totalArmyCount);
        int armyDie2 = Mathf.FloorToInt(percent2 * totalArmyCount);

        int min = 0;
        int min2 = 0;

        if (armyDie == 0 && armyDie2 == 0)
        {
            if (percent > percent2)
            {
                armyDie = 1;
                min = 1;
            }
            else
            {
                armyDie2 = 1;
                min2 = 1;
            }
        }

        armyDie = Mathf.Min( actualArmyCount ,  Mathf.CeilToInt( Mathf.Clamp(actualArmyCount, min, armyDie) * NORMALIZE_ARMY_DIE_PHY ) );
        if (armyDie != 0) {
            armyDie++;
        }
        actualArmyCount -= armyDie;

        armyDie2 = Mathf.Min(actualArmyCount, Mathf.CeilToInt( Mathf.Clamp(actualArmyCount, min2, armyDie2) * NORMALIZE_ARMY_DIE_MAG ) );
        actualArmyCount -= armyDie2;

        counter();
        setBarHP();

        if (armyDie > 0)
        {
            if (percent > 0.15f || (armyDie/(float)actualArmyCount) > 0.1f)
            {
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("showTextDamage", RpcTarget.All, armyDie.ToString(), 0.8f, 0f, 0.1f, true);
                }
                else {
                    showTextDamage(armyDie.ToString(), 0.8f, 0f, 0.1f, true);
                }
            }
            else
            {
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("showTextDamage", RpcTarget.All, armyDie.ToString(), 0.8f, 0f, 0.1f);
                }
                else {
                    showTextDamage(armyDie.ToString(), 0.8f, 0f, 0.1f);
                }
            }
        }

        if (armyDie2 > 0)
        {
            if (percent2 > 0.08f || (armyDie2 / (float)actualArmyCount) > 0.1f)
            {
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("showTextDamage", RpcTarget.All, armyDie2.ToString(), 0.6f, 0.3f, 0.8f, true);
                }
                else {
                    showTextDamage(armyDie2.ToString(), 0.6f, 0.3f, 0.8f , true);
                }
            }
            else
            {
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("showTextDamage", RpcTarget.All, armyDie2.ToString(), 0.6f, 0.3f, 0.8f);
                }
                else {
                    showTextDamage(armyDie2.ToString(), 0.6f, 0.3f, 0.8f);
                }
            }
        }

        if (actualArmyCount <= 0)
        {
            Debug.Log("This Army is Done! (" + pv.ViewID + ")");
            Die();
        }
        else
        {
            Shake(shakeTime);
            //Debug.Log("This Army is left "+actualArmyCount+" (" + pv.ViewID + " Died)");
        }
        
        
    }


    public void setAttackingFalse() {
        isAttacking = false;
    }

    public void counter() {

        if (!isMine)
            return;

        float value = Random.RandomRange(0f, 1f);
        if (value < getCounterAttackRate()) {
            Pion p = GetandCheckTarget();
            if (p != null)
            {
                //isAttacking = true;
                if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("showTextDamage", RpcTarget.All, "Countering!", 1f, 0.9f, 0.2f);
                }
                else {
                    showTextDamage("Countering!", 1f, 0.9f, 0.2f);
                }
                //Attack(p);
                attackCD = -1;
            }
        }
    }

    [PunRPC]
    public void showTextDamage(string text , float r , float g , float b , bool isBig)
    {
        if (MapGenerator.MAIN == null)
            return;

        Color col = new Color(r , g, b);

        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (isMaster) {
            DamagePopup.Create(MapGenerator.MAIN.textPopUpPrefabs, transform.position, text, col, new Vector2(Random.RandomRange(-1f, 1f), 1) * 10 , isBig);
        }
        else
        {
            DamagePopup.Create(MapGenerator.MAIN.textPopUpPrefabs, transform.position, text, col, new Vector2(Random.RandomRange(-1f, 1f), -1) * 10 , isBig);
        }

    }

    [PunRPC]
    public void showTextDamage(string text, float r, float g, float b)
    {
        showTextDamage(text, r, g, b, false);
    }


    public void Die() {

        updateFogOfWarAround(true);

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("SpawnDeathParticle", RpcTarget.All);
        }
        else {
            SpawnDeathParticle();
        }

        if (MapGenerator.MAIN == null)
            return;

        MapGenerator.MAIN.armyParent.playerPion.Remove(this);
        unOccupiedCurrentTile();
        PhotonNetwork.Destroy(pv);
        //PhotonNetwork.Destroy(gameObject);
    }

    [PunRPC]
    public void SpawnDeathParticle() {
        if (MapGenerator.MAIN != null)
        {
            MapGenerator.MAIN.spawnParticleDie(gender, transform.position);
        }
    }

    #region getStatus
    public int getHealth()
    {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getHealth(actualArmyCount);
        }
        float power = (actualArmyCount * 1f) / totalArmyCount;
        return Mathf.RoundToInt(power * status.health) + buffAdd;
    }
    public int getPhyAttack() {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero) {
            buffAdd += buff.getPhyAttack(actualArmyCount);
        }
        float power = (actualArmyCount * 1f) / totalArmyCount;
        //Debug.Log(status.phyAttack + " - " + buffAdd + " - " + power);
        return Mathf.RoundToInt(power * status.phyAttack)  + buffAdd;
    }
    public int getMagAttack()
    {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getMagAttack(actualArmyCount);
        }
        float power = (actualArmyCount * 1f) / totalArmyCount;

        return Mathf.RoundToInt(power * status.magAttack) + buffAdd;
    }
    public int getPhyDeff() {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getPhyDeff(actualArmyCount);
        }
        float power = (actualArmyCount * 1f) / totalArmyCount;

        return Mathf.RoundToInt(power * status.phyDeffense) + buffAdd;
    }
    public int getMagDeff() {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getMagDeff(actualArmyCount);
        }
        float power = (actualArmyCount * 1f) / totalArmyCount;

        return Mathf.RoundToInt(power * status.magDeffense) + buffAdd;
    }
    public float getAttackSpeed() {
        float buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getAttackSpeed();
        }
        buffAdd = Mathf.Max( 1 - buffAdd , 0.4f);
        return status.attackSpeed * buffAdd;
    }
    public virtual float getMovementSpeed() {
        float buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getMovementSpeed();
        }
        buffAdd = Mathf.Max(1+buffAdd , 0.01f);
        return status.movementSpeed * buffAdd;
    }
    public float getCounterAttackRate()
    {
        float buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getCounterAttackRate();
        }
        return Mathf.Min( status.counterAttackRate + buffAdd , 1 );
    }
    public int getRange() {
        int buffAdd = 0;
        foreach (BuffStatusScriptableObject buff in buffHero)
        {
            buffAdd += buff.getRange();
        }

        return status.range + buffAdd;
    }
    #endregion

    public bool isExistTargetAround {
        get {
            return _targetAttack.Count != 0;
        }
    }

    public void showTextSkill(string skillName) {
        //pv.RPC("showTextDamage", RpcTarget.All, skillName, 1, 1, 1);

        showTextDamage(skillName, 1, 1, 1);
    }

    public void addBuff(BuffStatusScriptableObject buff)
    {
        Debug.Log("Buff added");

        BuffStatusScriptableObject b = buff.clone();
        buffHero.Add(b);
        Color r = Color.yellow;

        if (b.type == BuffStatusScriptableObject.BuffType.BUFF)
        {
            audios.clip = buffedClip;
            audios.Play();
            if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
            {
                pv.RPC("showTextDamage", RpcTarget.All, "Buffed !", r.r, r.g, r.b);
            }
            else {
                showTextDamage("Buffed !", r.r, r.g, r.b);
            }
        }
        else {
            audios.clip = debuffedClip;
            audios.Play();
            Debug.Log("Heroes Debuffed!");
            if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
            {
                pv.RPC("showTextDamage", RpcTarget.All, "Debuffed !", 0.8f, 0.1f, 0f);
            }else {
                showTextDamage("Buffed !", r.r, r.g, r.b);
            }
        } 
    }

    [PunRPC]
    public void addBuff(string path)
    {
        Debug.Log("Buff added");

        BuffStatusScriptableObject buff = RoyaleHeroesLibs.getBuffData(path);
        if (buff != null)
        {
            addBuff(buff);
        }
        else {
            Debug.Log("Buff not Found : "+path);
        }
    }

    public void Heal(float heal) {

        Debug.Log("Healed");

        audios.clip = buffedClip;
        audios.Play();

        float percent =  (heal / (status.health * 1f));

        int healedArmy = Mathf.FloorToInt(totalArmyCount * percent);
        Debug.Log(" " + heroData.heroName + " heal " + healedArmy + " army");

        int totalWounded = totalArmyCount - actualArmyCount;

        int totalHealed = 0;
        if (totalWounded > healedArmy)
        {
            totalHealed = healedArmy;
        }
        else {
            totalHealed = totalWounded;
        }

        actualArmyCount = actualArmyCount + totalHealed;//Mathf.Min(actualArmyCount + healedArmy, totalArmyCount);

        Color r = Color.green;
        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            pv.RPC("showTextDamage", RpcTarget.All, totalHealed.ToString() + "+", r.r, r.g, r.b);
        }
        else {
            showTextDamage(totalHealed.ToString() + "+", r.r, r.g, r.b);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();

        if (stream.IsWriting)
        {
            //if (existChange)
            //{
                //Debug.Log("Writing Serialiaze");
                stream.SendNext(actualArmyCount);
                
            //}

        }
        else
        {
            //Debug.Log("accepting Serialiaze");
            this._actualArmyCount = (int)stream.ReceiveNext();

            setBarHP();
            //ownerChanged();
        }
    }

    public int getActualArmy() {
        return _actualArmyCount;
    }

    public void setBarHP() {
        Vector3 res = hpBar.localScale;
        float power = (actualArmyCount / (1f * totalArmyCount));

        if (power < 0.6)
        {
            hpBar.GetComponent<SpriteRenderer>().color = new Color(0.9f,0.9f,0.1f);
        }
        else if (power < 0.1)
        {
            hpBar.GetComponent<SpriteRenderer>().color = new Color(0.9f,0.2f , 0);
        }
        else {
            hpBar.GetComponent<SpriteRenderer>().color = new Color(0.5f , 0.8f , 0.2f);
        }

        res.x = power * maxFloatHp;
        hpBar.localScale = res;
    }

    public bool canAttack() {
        return !anim.GetBool("isCapturing");
    }


    public int getPhyDamage() {
        float power = (actualArmyCount * 1f) / totalArmyCount;

        int purePhyAttack = getPhyAttack();
        //int pureMagAttack = getMagAttack();

        int phyOffset = Mathf.RoundToInt((purePhyAttack * 0.2f) * (totalArmyCount / 50000f));
        //int magOffset = Mathf.RoundToInt((pureMagAttack * 0.2f) * (totalArmyCount / 50000f));

        return Random.RandomRange(purePhyAttack - phyOffset, purePhyAttack);
        //int MagDamage = Random.RandomRange(pureMagAttack - magOffset, pureMagAttack);

    }

    public int getMagDamage() {
        float power = (actualArmyCount * 1f) / totalArmyCount;

        //int purePhyAttack = getPhyAttack();
        int pureMagAttack = getMagAttack();

        //int phyOffset = Mathf.RoundToInt((purePhyAttack * 0.2f) * (totalArmyCount / 50000f));
        int magOffset = Mathf.RoundToInt((pureMagAttack * 0.2f) * (totalArmyCount / 50000f));

        //int PhyDamage = Random.RandomRange(purePhyAttack - phyOffset, purePhyAttack);
        return Random.RandomRange(pureMagAttack - magOffset, pureMagAttack);


    }

    public virtual void Attack(Pion p) {

        if (!canAttack()) {
            Debug.Log("Cant attack player due to capturing");
            return;
        }

        Debug.Log("Attack cloesest enemy! at "+p.getCurrentTile());

        int PhyDamage = getPhyDamage();
        int MagDamage = getMagDamage();

        //transform.Rotate(new Vector3(0, 0, 10));

        if (MapGenerator.MAIN == null || MapGenerator.MAIN.isOnline)
        {
            p.pv.RPC("ImplementDamage", RpcTarget.Others, PhyDamage, MagDamage, actualArmyCount);


#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount);
            }
#endif
        }
        else {
            p.ImplementDamage(PhyDamage, MagDamage, actualArmyCount);
        }
        p.Shake(0.3f);

        isAttacking = false;

    }

    public void OnDestroy()
    {
        

        if (Camera.main != null) {
            RipplePospProcessor riple = Camera.main.GetComponent<RipplePospProcessor>();
            if (riple != null)
            {
                riple.RippleCam(transform.position);
            }
        }

        if (isMine) {
            Vibration.Vibrate(200);
        }
        
        
    }


}
