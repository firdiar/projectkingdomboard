﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PhotonView))]
public class GameSceneManager : MonoBehaviourPunCallbacks, IPunObservable
{

    [Header("Preference")]
    [SerializeField] MapGenerator generator;
    [SerializeField] Transform mapCam;
    [SerializeField] UIGameScreenHandler uiManager;
    [SerializeField] StartBattleHandler start;
    [SerializeField] AudioSource srcSound = null;
    [SerializeField] AudioClip[] clip = new AudioClip[0];
    

    int _masterpoin;
    int _clientpoin;

    public int captureCount = 0;
    public HashSet<Vector2> exploredTile = new HashSet<Vector2>();

    [SerializeField]
    public int masterpoin {
        get {
            return _masterpoin;
        }
        set {
            _masterpoin = value;
            changeExist = true;
        }
    }

    [SerializeField]
    public int clientpoin {
        get {
            return _clientpoin;
        }
        set {
            _clientpoin = value;
            changeExist = true;
        }
    }
    bool changeExist = false;

    public float masterbonus = 1;
    public float clientbonus = 1;

    [SerializeField] int _time = 360;

    [SerializeField]
    public int time {
        get {
            return _time;
        }
        set {
            _time = value;
            changeExist = true;
        }
    }
    public int timePassed = 0;

    public int targetUserPoin = 150;

    public PhotonView pv;

    bool isRush = false;

    // Start is called before the first frame update
    void Start()
    {

        if (!generator.isOnline) {
            generator.GenerateMap();
            return;
        }

        pv = GetComponent<PhotonView>();

        pv.ObservedComponents[0] = this;

#if UNITY_EDITOR
        Debug.Log("Connected : " + PhotonNetwork.IsConnected);
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            return;
        }

#endif
        if (PhotonNetwork.IsMasterClient)
        {
            generator.GenerateMap();
        }
        else
        {
            generator.GenerateMap();
            Camera.main.transform.Rotate(new Vector3(0, 0, 180));
            mapCam.Rotate(new Vector3(0, 0, 180));
        }
    }
#if UNITY_EDITOR
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.CreateRoom("TestRoom");
    }
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("RoomCreated");

        Start();
    }
#endif

    public void GameStart() {

        

        string enemyName = "";

        if (generator.isOnline)
        {
#if UNITY_EDITOR
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                enemyName = "test enemy";
            }
            else
            {
#endif
                enemyName = PhotonNetwork.PlayerListOthers[0].NickName;
#if UNITY_EDITOR
            }
#endif
        }
        else {
            enemyName = "Computer";
        }


        //uiManager.initPopUp(MapGenerator.MAIN.armyParent.playerPion[0]);
        uiManager.InitTop(Account.CurrentAccountData.nickName, enemyName, targetUserPoin, 360);
        //uiManager.openPopUpStatus();
        start.init(Account.CurrentAccountData.nickName, enemyName);

        uiManager.openLayerGame();

        //armyParent.playerPion


        Invoke("startBattleAnim" , 0.5f);


        //startBattleAnim();
        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (isMaster) {
            InvokeRepeating("tickTime", 5, 1);
        }

    }

    void startBattleAnim() {
        srcSound.clip = clip[0];
        srcSound.Play();
        start.startAnim(()=> {
            Camera.main.GetComponent<CameraFollowTarget>().SetTarget(MapGenerator.MAIN.armyParent.playerPion[0].transform.position);
            //srcSound.volume = 0.3f;
            StartCoroutine(volDown(0.2f , null));
        });
        Invoke("playSound2", 29.8f);
    }

    IEnumerator volDown(float to , UnityEngine.Events.UnityAction action) {
        float progress = 0;
        float start = srcSound.volume;
        while (progress < 1) {
            progress += 0.01f;
            srcSound.volume = Mathf.Lerp(start, to, progress);
            yield return new WaitForSeconds(0.05f);
        }

        if (action != null) {
            action.Invoke();
        }
        yield return null;
    }

    void playSound2() {
        srcSound.clip = clip[1];
        srcSound.volume = 0.4f;
        srcSound.loop = true;
        srcSound.Play();
    }

    void playSound3()
    {
        srcSound.clip = clip[2];
        //srcSound.volume = 0.9f;
        srcSound.loop = true;
        srcSound.Play();
    }

    /*
    bool playerReady = false;

    public void startTime() {
        InvokeRepeating("tickTime", 1, 1);
    }

    [PunRPC]
    public void setPlayerReady() {
        if (playerReady)
        {
            startTime();
        }
        else {
            playerReady = true;
#if UNITY_EDITOR
            if(PhotonNetwork.CurrentRoom.PlayerCount == 1)
                startTime();
#endif
        }
    }
    */

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();



        //UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
    }
    bool gameFinish = false;
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        if (gameFinish)
            return;

        GameFinished(ResultGame.WIN);
        CancelInvoke("tickTime");
    }

    bool checkIsRush() {

        if (masterpoin > targetUserPoin * (6.5f / 10f) || clientpoin > targetUserPoin * (6.5f / 10f))
        {
            return true;
        }
        else if (time < 40) {
            return true;
        }

        return false;
    }

    [PunRPC]
    public void inRush() {
        StartCoroutine(volDown(0, () => {
            playSound3();
            StartCoroutine(volDown(1f, null));
        }));
    }

    public void tickTime() {

        timePassed++;
        time = Mathf.Max(time-1 , 0);
        masterpoin = Mathf.Min(masterpoin+ Mathf.CeilToInt(MapGenerator.MAIN.getPlayerLandmarkCount() * masterbonus) , targetUserPoin);
        clientpoin = Mathf.Min(clientpoin+ Mathf.CeilToInt(MapGenerator.MAIN.getEnemyLandmarkCount() * clientbonus) , targetUserPoin);

        //Debug.Log("Master Poin : " + masterpoin);

        uiManager.UpdateTime(_time);
        updatePoin();

        if (isRush == false) {
            if (checkIsRush()) {

                if (MapGenerator.MAIN.isOnline)
                {
                    pv.RPC("inRush", RpcTarget.All);
                }
                else {
                    inRush();
                }
                isRush = true;
            }
        }

        

        if (checkGameFinish()) {

            ResultGame isMasterWin = ResultGame.WIN;

            int ourArmyCount = generator.armyParent.transform.childCount;
            int enemyArmyCount = generator.armyEnemyParent.childCount;



            if ((ourArmyCount == 0 || enemyArmyCount == 0) && (enemyArmyCount != ourArmyCount))
            {
                if (ourArmyCount == 0)
                {//kita kalah
                    isMasterWin = ResultGame.LOSE;
                }

                if (enemyArmyCount == 0)
                {//kita menang
                    isMasterWin = ResultGame.WIN;
                }
            }
            else
            {
                if (masterpoin != clientpoin)
                {

                    // define winner
                    if (masterpoin > clientpoin)
                    {
                        isMasterWin = ResultGame.WIN;
                    }
                    else if (masterpoin < clientpoin)
                    {
                        isMasterWin = ResultGame.LOSE;
                    }
                }
                else
                {
                    isMasterWin = ResultGame.DRAW;
                }
                
            }

            setUIFinishGame(isMasterWin);

        }
    }

    public void setUIFinishGame(ResultGame isMasterWin) {
        //gameFinish = true;

        if (MapGenerator.MAIN.isOnline)
        {
            pv.RPC("setGameFinish", RpcTarget.All);
            pv.RPC("GameFinished", RpcTarget.All, isMasterWin);
        }
        else
        {
            setGameFinish();
            GameFinished(isMasterWin);
        }
        CancelInvoke("tickTime");
        //MapGenerator.MAIN.CancelInvoke("GenerateBuilding");
    }



    [PunRPC]
    public void addMasterBonusPoin(int bonus)
    {
        masterbonus += bonus;
    }

    [PunRPC]
    public void addClientBonusPoin(int bonus)
    {
        clientbonus += bonus;
    }

    [PunRPC]
    public void addMasterPoin(int bonus)
    {
        masterpoin += bonus;
    }

    [PunRPC]
    public void addClientPoin(int bonus)
    {
        clientpoin += bonus;
    }

    public bool checkGameFinish() {
        if (time == 0) {
            return true;
        }
        else
        {
            if (masterpoin == targetUserPoin)
            {

                return true;
            }
            else if (clientpoin == targetUserPoin) {
                return true;
            }

            if ((MapGenerator.MAIN.armyParent.transform.childCount == 0 || MapGenerator.MAIN.armyEnemyParent.childCount == 0) &&!MapGenerator.MAIN.isTestMode) {
                return true;
            }

            return false;
        }
    }

    public class BattleResult {

        static BattleResult _battleResult;
        public static BattleResult battleResult {
            get {
                if (_battleResult == null) {
                    _battleResult = new BattleResult();
                    //Hapus nanti
                    /*_battleResult.infantryLoss = 100;
                    _battleResult.magicianLoss = 150;
                    _battleResult.archerLoss = 50;
                    _battleResult.cavalaryLoss = 190;

                    _battleResult.infantryTotal = 200;
                    _battleResult.magicianTotal = 200;
                    _battleResult.archerTotal = 200;
                    _battleResult.cavalaryTotal = 200;*/

                    _battleResult.isWin = true;

                    _battleResult.resourceLoss.food = 100000;
                    _battleResult.resourceLoss.wood = 100000;
                    _battleResult.resourceLoss.stone = 100000;

                    _battleResult.addArmyLoss("Gajah Mada", HeroesClass.Infantry, 10000);
                    _battleResult.addArmyTotal("Gajah Mada", HeroesClass.Infantry, 20000);

                    _battleResult.addArmyLoss("Prince Dipenogoro", HeroesClass.Cavalary, 10000);
                    _battleResult.addArmyTotal("Prince Dipenogoro", HeroesClass.Cavalary, 20000);


                }
                return _battleResult;
            }
            set {
                _battleResult = value;
            }
        }
        public bool isWin = false;
        public int infantryLoss = 0;
        public int archerLoss = 0;
        public int cavalaryLoss = 0;
        public int magicianLoss = 0;

        public int infantryTotal = 0;
        public int archerTotal = 0;
        public int cavalaryTotal = 0;
        public int magicianTotal = 0;

        public void addArmyLoss(string heroName, HeroesClass data , int loss) {
            switch (data) {
                case HeroesClass.Infantry:
                    infantryLoss += loss;
                    break;
                case HeroesClass.Archer:
                    archerLoss += loss;
                    break;
                case HeroesClass.Cavalary:
                    cavalaryLoss += loss;
                    break;
                case HeroesClass.Magician:
                    magicianLoss += loss;
                    break;
            }
            armyLoss.Add(heroName, loss);
        }
        public void addArmyTotal(string heroName ,HeroesClass data, int loss)
        {
            switch (data)
            {
                case HeroesClass.Infantry:
                    infantryTotal += loss;
                    break;
                case HeroesClass.Archer:
                    archerTotal += loss;
                    break;
                case HeroesClass.Cavalary:
                    cavalaryTotal += loss;
                    break;
                case HeroesClass.Magician:
                    magicianTotal += loss;
                    break;
            }

            armyPrevious.Add(heroName, loss);
        }

        public int getTotalDie() {
            return infantryLoss+archerLoss+cavalaryLoss+magicianLoss ;
        }
        public int getTotalArmy()
        {
            return infantryTotal+archerTotal+cavalaryTotal+magicianTotal;
        }


        public Dictionary<string, int> armyPrevious = new Dictionary<string, int>();
        public Dictionary<string, int> armyLoss = new Dictionary<string, int>();
        public Dictionary<string, int> armyBonusXp = new Dictionary<string, int>();
        public ResourcesMaterial resourceLoss = new ResourcesMaterial();
    }

    [PunRPC]
    public void setGameFinish()
    {
        gameFinish = true;
        foreach (Transform t in generator.armyEnemyParent) {
            t.GetComponent<Pion>().StopGame();
        }
        foreach (Pion t in generator.armyParent.playerPion)
        {
            t.StopGame();
        }

    }

    
    public void setSurrender() {
        CancelInvoke("tickTime");
        

        BattleResult result = new BattleResult();

        result.isWin = false;

        PlayerManager pm = generator.armyParent;
        BlockData[,] data = generator.loadLineUp();
        foreach (BlockData blok in data)
        {
            if (blok.hero != "")
            {
                bool found = false;
                foreach (Pion p in pm.playerPion)
                {
                    if (p.heroData.heroName == blok.hero)
                    {
                        int armyLoss = p.totalArmyCount - p.actualArmyCount;
                        //result.armyLoss.Add(p.heroData.heroName, armyLoss);

                        result.resourceLoss += blok.getTotalResources() * (1 - ( Mathf.Max( p.actualArmyCount - 100 , 0)   / (p.totalArmyCount * 1f))  );

                        result.addArmyLoss(blok.hero, p.heroData.heroesClass, armyLoss);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    int armyLoss = blok.getCountArmy();
                    //result.armyLoss.Add(blok.hero, armyLoss);
                    result.resourceLoss += blok.getTotalResources();

                    result.addArmyLoss(blok.hero, blok.heroClass, armyLoss);

                }

                result.addArmyTotal(blok.hero, blok.heroClass, blok.getCountArmy());
                //result.armyPrevious.Add(blok.hero, blok.getCountArmy());
            }
        }

        int goldGain = 50 + Mathf.RoundToInt(result.getTotalArmy() / 1000f);

        if (time > 300) {
            goldGain = 0;
        }


        uiManager.closeLayerGame(ResultGame.LOSE, goldGain, captureCount, exploredTile.Count);

        //string json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

        //Debug.Log(json);

        BattleResult.battleResult = result;

        if (generator.isOnline)
        {
            PhotonNetwork.LeaveRoom();
        }


        //if (PhotonNetwork.MasterClient) {
        MapGenerator.MAIN.enabled = false;
        MapGenerator.MAIN = null;
        //}

        

        //GameFinished(ResultGame.LOSE);

    }
    


    [PunRPC]
    public void GameFinished(ResultGame winlose) {
        Debug.Log("Game Finished");

        StartCoroutine(volDown(0, null));


        ResultGame isIWin = ResultGame.DRAW;
        BattleResult result = new BattleResult();

        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;

        if (isMaster)
        {
            if (winlose == ResultGame.WIN)
            {
                result.isWin = true;
                isIWin = ResultGame.WIN;
            }
            else if(winlose == ResultGame.LOSE)
            {
                result.isWin = false;
                isIWin = ResultGame.LOSE;
            }
        }
        else {
            if (winlose == ResultGame.WIN)
            {
                result.isWin = false;
                isIWin = ResultGame.LOSE;
            }
            else if (winlose == ResultGame.LOSE)
            {
                result.isWin = true;
                isIWin = ResultGame.WIN;
            }
        }
        


        PlayerManager pm = generator.armyParent;
        BlockData[,] data = generator.loadLineUp();
        foreach (BlockData blok in data) {
            if (blok.hero != "") {
                bool found = false;
                foreach (Pion p in pm.playerPion) {
                    if (p.heroData.heroName == blok.hero) {
                        int armyLoss = p.totalArmyCount - p.actualArmyCount;
                        //result.armyLoss.Add(p.heroData.heroName, armyLoss);

                        result.resourceLoss += blok.getTotalResources() * (1-(p.actualArmyCount / (p.totalArmyCount * 1f)));
                        
                        result.addArmyLoss(blok.hero, p.heroData.heroesClass, armyLoss);

                        result.armyBonusXp.Add(blok.hero, p.exploaration / 10);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    int armyLoss = blok.getCountArmy();
                    //result.armyLoss.Add(blok.hero, armyLoss);
                    result.resourceLoss += blok.getTotalResources();

                    result.addArmyLoss(blok.hero, blok.heroClass, armyLoss);

                }

                result.addArmyTotal(blok.hero, blok.heroClass, blok.getCountArmy());
                //result.armyPrevious.Add(blok.hero, blok.getCountArmy());
            }
        }

        int goldGain = 50 + Mathf.RoundToInt(result.getTotalArmy() / 1000f);
        if (winlose == ResultGame.DRAW) {
            goldGain = 0;
        }

        

        uiManager.closeLayerGame(isIWin, goldGain , captureCount , exploredTile.Count );

        //string json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

        //Debug.Log(json);

        BattleResult.battleResult = result;

        //if (PhotonNetwork.MasterClient) {
        MapGenerator.MAIN.enabled = false;
        //MapGenerator.MAIN = null;
        //}

        if (generator.isOnline)
        {
            PhotonNetwork.LeaveRoom();
        }

    }

    public void updatePoin() {
        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (isMaster) {
            uiManager.UpdatePoin(masterpoin , targetUserPoin , true);
            uiManager.UpdatePoin(clientpoin, targetUserPoin, false);
        }
        else {
            uiManager.UpdatePoin(clientpoin, targetUserPoin, true);
            uiManager.UpdatePoin(masterpoin, targetUserPoin, false);
            
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
        if (stream.IsWriting)
        {
            if (changeExist)
            {
                //Debug.Log("Writing Serialiaze");
                stream.SendNext(_masterpoin);
                stream.SendNext(_clientpoin);
                stream.SendNext(_time);
                changeExist = false;
            }

        }
        else
        {
            //Debug.Log("accepting Serialiaze");
            this._masterpoin = (int)stream.ReceiveNext();
            this._clientpoin = (int)stream.ReceiveNext();
            this._time = (int)stream.ReceiveNext();

            uiManager.UpdateTime(_time);
            updatePoin();
            //ownerChanged();
        }

    }

    public override void OnDisconnected(DisconnectCause cause)
    {

        if (!MapGenerator.MAIN.isOnline)
        {
            
            return;
        }

        base.OnDisconnected(cause);

        if (!PhotonNetwork.ReconnectAndRejoin()) {
            uiManager.disconnected();
        }
    }

    public void moveToLogin() {
        RoyaleHeroesLoading.openLayerLoading(() =>
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LoginScene");
        });
    }
}
