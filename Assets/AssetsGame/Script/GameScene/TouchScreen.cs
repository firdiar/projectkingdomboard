﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class TouchScreen : MonoBehaviour


    ,IPointerDownHandler , IPointerUpHandler , IDragHandler


{

    [SerializeField] PlayerManager pm = null;
    
    float touchesPrevPosDifference, touchesCurPosDifference, zoomModifier;

    Vector2 firstTouchPrevPos, secondTouchPrevPos;

    [SerializeField]
    float zoomModifierSpeed = 0.4f;

    bool isClicking = false;
    Vector2 dragInputPosition;

    bool scrollDisabled = false;

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount == 2)
        {
            scrollDisabled = true;
            isClicking = false;
            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);

            firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
            secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

            zoomModifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * zoomModifierSpeed;

            if (touchesPrevPosDifference > touchesCurPosDifference)
                Camera.main.orthographicSize += zoomModifier;
            if (touchesPrevPosDifference < touchesCurPosDifference)
                Camera.main.orthographicSize -= zoomModifier;

            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 5f, 30f);
            Scroll(Vector2.zero);

        }else if (scrollDisabled) {
            scrollDisabled = false;
        }

        /*
        else if(Input.touchCount == 1){
            Touch firstTouch = Input.GetTouch(0);
            //firstTouch.type
            switch (firstTouch.phase) {
                case TouchPhase.Began:
                    Debug.Log("Inpuut Begin");
                    isClicking = true;
                    dragInputPosition = firstTouch.position;
                    break;
                case TouchPhase.Moved:
                    Debug.Log("Drag");

                    isClicking = false;
                    Scroll(Camera.main.ScreenToWorldPoint(dragInputPosition) - Camera.main.ScreenToWorldPoint(firstTouch.position));
                    dragInputPosition = firstTouch.position;
                    break;
                case TouchPhase.Ended:
                    Debug.Log("Input Up");
                    if (isClicking) {
                        isClicking = false;
                        rayCast(Camera.main.ScreenToWorldPoint(firstTouch.position));

                        //Camera.main.orthographicSize = 10;
                    }
                    break;

            }
            
        }
        */

    }



    public void rayCast(Vector2 TouchPos) {



        if (MapGenerator.MAIN == null) {
            return;
        }

        MapTile m = MapGenerator.MAIN.getMapTileFromWorld(TouchPos);
        //Debug.Log(TouchPos);
        
        if (m == null)
        {
            return;
        }

        UIGameScreenHandler.MAIN.hideHintBuilding();

        

        Pion temp = MapGenerator.MAIN.getPlayerOnPosition(m.x, m.y);

        Debug.Log(m.x + " - " + m.y + " - " + TouchPos);

        if (temp != null)
        {
            Debug.Log("Selected : "+temp.name);
            // m.getCurrentPion;
            //Pion temp = m.getCurrentPion();
            if (MapGenerator.MAIN.isOnline)
            {
                if (!temp.photonView.IsMine)
                {
                    return;
                }
            }
            else {
                if (temp.transform.parent != MapGenerator.MAIN.armyParent.transform) {
                    return;
                }
            }

            
            if (pm.selectedPion.Contains(temp))
            {
                pm.selectedPion.Remove(temp);
                temp.setNotSelected();
            }
            else {
                pm.addSelected(temp);
                Debug.Log("Pion Selected");
            }

            if (pm.selectedCount() == 1)
            {
                UIGameScreenHandler.MAIN.showHintChara(pm.selectedPion[0]);
            }
            else {
                UIGameScreenHandler.MAIN.hideHintChara();
            }

        }
        else if (pm.selectedCount() > 0)
        {
            UIGameScreenHandler.MAIN.hideHintChara();
            Vector2 targetVector = new Vector2(m.x, m.y);

            pm.selectedPion.RemoveAll((pion) => pion == null);

            pm.selectedPion.Sort(delegate (Pion a, Pion b) {
                float val1 = Vector2.Distance(a.getPos(), targetVector);
                float val2 = Vector2.Distance(b.getPos(), targetVector);
                return val1.CompareTo(val2);
            });

            foreach (Pion p in pm.selectedPion)
            {
                p.MovePionTo(targetVector);
                //p.Move(MapGenerator.MAIN.findPath(p.getPos(), targetVector));
            }
            pm.clearSelection();

        } else if(m.type != TileType.GROUND){
            //clicking other tile
            UIGameScreenHandler.MAIN.showHintBuilding(m);

        }


    }

    // Use this for initialization
     void Scroll(Vector2 delta) {
        Camera.main.GetComponent<CameraFollowTarget>().SetTargetingOff();
        Vector3 pos = Camera.main.transform.position;
        pos.x = Mathf.Clamp(pos.x + delta.x , MapGenerator.MAIN.minX() , MapGenerator.MAIN.maksX());
        pos.y = Mathf.Clamp(pos.y + delta.y, MapGenerator.MAIN.minY(), MapGenerator.MAIN.maksY());
        Camera.main.transform.position = pos;
    }


    public void OnPointerDown(PointerEventData eventData)
    {

        if (scrollDisabled)
            return;

        isClicking = true;
        dragInputPosition = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (scrollDisabled)
            return;

        if (isClicking)
        {
            isClicking = false;
            rayCast(Camera.main.ScreenToWorldPoint(eventData.position));
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (scrollDisabled)
            return;

        isClicking = false;
        Scroll(Camera.main.ScreenToWorldPoint(dragInputPosition) - Camera.main.ScreenToWorldPoint(eventData.position));
        dragInputPosition = eventData.position;
    }


}
