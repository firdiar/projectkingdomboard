﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SaveData", menuName = "UserData/Data", order = 1)]
public class UserSaveDataScriptableObject : ScriptableObject
{
    public List<ArmyFormation> formation;
	
}

[System.Serializable]
public class ArmyFormation {
    public Vector2 pos;
    public ArmyType armyType;
    public int index;
}
public enum ArmyType {
    Infantry
}
