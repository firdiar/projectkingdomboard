﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    [SerializeField]float gotoTime = 2;
    Vector3 target = Vector2.zero;
    bool isTargeting = false;
    float progress = 1;

    // Start is called before the first frame update
    public void SetTarget(Vector2 pos)
    {
        target = pos;
        target.z = transform.position.z;
        isTargeting = true;
        progress = 0;
    }
    public void SetTargetingOff() {
        isTargeting = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTargeting) {
            //Debug.Log("moving : " + progress);
            progress = Mathf.Min(progress+ (Time.deltaTime/(gotoTime*10)) , gotoTime);

            transform.position = Vector3.Lerp(transform.position, target, progress / gotoTime);
            if (Vector3.Distance(transform.position, target) <= 0.01f) {
                isTargeting = false;
            }
        }
    }
}
