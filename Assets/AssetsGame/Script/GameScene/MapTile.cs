﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileOwner { MASTER , CLIENT , NEUTRAL }
public enum TileType { WALL , LANDMARK , OBSTACLE , GROUND , CENTER_STRATEGY , EMBASSY , HOSPITAL }

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(Animation))]
public class MapTile : MonoBehaviour, IPunObservable, IPunInstantiateMagicCallback
{
    
    public int x = 0;
    public int y = 0;

    //public aud

    public Animation anim;

    [SerializeField]
    public bool _isOccupied = false;
    public bool isOccupied {
        get {
            return _isOccupied;
        }
        set {
            takeOver();
            if (value == false) {
                pionID = -1;
            }
            
            _isOccupied = value;

            if (isCapturing)
            {
                targetOwner = TileOwner.NEUTRAL;
                //owner = TileOwner.NEUTRAL;
            }

            existChange = true;
        }
    }



    float poinToCapture {
        get {
            switch (type) {
                case TileType.LANDMARK:
                    return 10;
                case TileType.HOSPITAL:
                    return 3;
                case TileType.CENTER_STRATEGY:
                    return 15;
                case TileType.EMBASSY:
                    return 5;
                default:
                    return 10;
            }
        }
    }
    float _capturePoin = 0;
    public TileOwner targetOwner;
    public float capturePoin {
        get { return _capturePoin; }
        set {
            takeOver();
            _capturePoin = value;

            setScaleCapture();

            existChange = true;
        }
    }
    bool isCapturing = false;
    bool toCapturingZero = false;
    public bool isCapturAble
    {
        get
        {

            bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient :(currentPion.transform.parent == MapGenerator.MAIN.armyParent.transform) ;


            switch (type)
            {
                case TileType.LANDMARK:
                    return (owner == TileOwner.NEUTRAL ? true : ((owner == TileOwner.MASTER) != isMaster));
                case TileType.HOSPITAL:
                    return (owner == TileOwner.NEUTRAL ? true : ((owner == TileOwner.MASTER) != isMaster));
                case TileType.CENTER_STRATEGY:
                    return (owner == TileOwner.NEUTRAL ? true : ((owner == TileOwner.MASTER) != isMaster));
                case TileType.EMBASSY:
                    return (owner == TileOwner.NEUTRAL ? true : ((owner == TileOwner.MASTER) != isMaster));
                default:
                    return false;
            }
        }
        
    }


    Color targetColor = Color.white;
    public int limitCapture = -1;

    [SerializeField]
    public TileType _type;

    public TileType type
    {
        get
        {
            return _type;
        }
        set
        {
            takeOver();
            _type = value;
            if (value == TileType.OBSTACLE)
            {
                addon.transform.Rotate(new Vector3(0, 0, 90) * Random.Range(0, 4));
            }
            else
            {
                if (MapGenerator.MAIN != null && !MapGenerator.MAIN.isOnline)
                {
                    addon.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                }
                else
                {
                    addon.transform.rotation = Quaternion.Euler(new Vector3(0, 0, (!PhotonNetwork.IsMasterClient ? 180 : 0)));
                }
            }
            if (value == TileType.HOSPITAL) {
                limitCapture = Random.RandomRange(5, 10);
            }
            setMapIndicator();
            existChange = true;
        }
    }

    [SerializeField]
    public TileOwner _owner;
    public TileOwner owner
    {
        get
        {
            return _owner;
        }
        set
        {
            takeOver();
            _owner = value;
            //ownerChanged();
            existChange = true;
            
        }
    }

    

    public bool canMove {
        get {
            switch (type) {
                case TileType.OBSTACLE:
                    return false;
                default:
                    return true;
            }
        }
    }

    

    [SerializeField] SpriteRenderer baseGround;
    [SerializeField] SpriteRenderer addon;
    [SerializeField] SpriteRenderer mapIndicator;
    [SerializeField] SpriteRenderer fogOfWar;
    [SerializeField] SpriteMask maskOfWar;

    [SerializeField] Transform captureLevel;
    [SerializeField] Transform captureEfectRight;
    [SerializeField] Transform captureEfectLeft;
    

    public string idSearch = "";

    public Sprite getAddonSprite() {
        return addon.sprite;
    }

    public bool isVisited(string idSearch) {
        if (this.idSearch == idSearch)
        {
            return true;
        }
        else {
            return false;
        }
    }

    Pion currentPion = null;
    PhotonView pv = null;
    [SerializeField]public int pionID = -1;

    //Fog of War
    //int fogUpdateCounter = 15;


    public string show() {
        return x + " , " + y;
    }

    public void startCapturing(TileOwner targetOwner) {
        if (owner == targetOwner) {
            return;
        }
        Debug.Log(targetOwner);
        this.targetOwner = targetOwner;
        isCapturing = true;

    }


    
    /*
    public void FixedUpdate()
    {

        if (fogUpdateCounter >= 0)
        {
            if (fogUpdateCounter == 0)
            {
                FogUpdate();
                fogUpdateCounter--;
            }
            
            fogUpdateCounter--;
            
        }

    }
    */

    [Header("Fog of War")]
    [Tooltip("Delayed Point * 0.02f second")]
    [SerializeField]float delayFogUpdate = 0.05f;
    bool updateFogOrdered = false;

    public void recalculateFog(bool force) {
        if (force)
        {
            Invoke("FogUpdate", delayFogUpdate);
        }
        else if (!updateFogOrdered) //fogUpdateCounter < 0)
        {
            Invoke("FogUpdate", delayFogUpdate);
            updateFogOrdered = true;
        }

        
        
    }

    void FogUpdate()
    {
        updateFogOrdered = false;
        if (MapGenerator.MAIN == null)
            return;

        if (MapGenerator.MAIN.armyParent.playerPion.Count == 0) {
            maskOfWar.enabled = true;

            Color colo = fogOfWar.color;
            colo.a = 0.75f;
            fogOfWar.color = colo;

            return;
        }

        float min = -1;
        foreach (Pion p in MapGenerator.MAIN.armyParent.playerPion) {
            
            if (p != null) {
                float dist = Vector2.Distance(p.transform.position, this.transform.position);
                if (dist< min || min == -1) {
                    min = dist;
                }
            }
        }

        float lerp = Mathf.InverseLerp(5, 12, min);
        if (lerp != 1 && maskOfWar.enabled)
        {
            maskOfWar.enabled = false;
        }
        else if(lerp == 1 && !maskOfWar.enabled)
        {
            maskOfWar.enabled = true;
        }

        Color col = fogOfWar.color;
        col.a = 0.75f * lerp;
        fogOfWar.color = col;
    }

    public void Update()
    {
        if (isCapturing)
        {
            if (targetOwner == TileOwner.NEUTRAL)
            {
                capturePoin = Mathf.Max(capturePoin - Time.deltaTime, 0);
                if (capturePoin == 0)
                {
                    owner = TileOwner.NEUTRAL;

                    isCapturing = false;

                    tileNetraled();
                }
            }
            else
            {

                if (owner != TileOwner.NEUTRAL)
                {
                    capturePoin = Mathf.Max(capturePoin - Time.deltaTime, 0);
                }
                else
                { // jika neutral
                    capturePoin = Mathf.Min(capturePoin + Time.deltaTime, poinToCapture);
                }

                if (capturePoin == 0)
                {
                    owner = TileOwner.NEUTRAL;
                    tileNetraled();
                    //MapGenerator.MAIN.pv.RPC("setLandmarkOwnerSprite", RpcTarget.All, x, y, owner);
                }
                else if (capturePoin == poinToCapture)
                {
                    Debug.Log("Captured Successfully");
                    //bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : ;
                    owner = MapGenerator.MAIN.isOnline ? (PhotonNetwork.IsMasterClient ? TileOwner.MASTER : TileOwner.CLIENT) : targetOwner;
                    isCapturing = false;

                    tileCaptured();

                    //MapGenerator.MAIN.pv.RPC("setLandmarkOwnerSprite", RpcTarget.All, x, y, owner);
                }

            }

        }
        else if (toCapturingZero) {
            capturePoin = Mathf.Max(capturePoin - Time.deltaTime*20, 0);
            if (capturePoin == 0)
            {
                toCapturingZero = false;
            }
        }


    }

    [PunRPC]
    public void showText(string text, float r , float g , float b)
    {
        Color col = new Color(r, g, b);
        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (isMaster)//PhotonNetwork.IsMasterClient)
        {
            DamagePopup.Create(MapGenerator.MAIN.textPopUpPrefabs, transform.position, text, col, new Vector2(Random.RandomRange(-1f, 1f), 1) * 10 , false);
        }
        else
        {
            DamagePopup.Create(MapGenerator.MAIN.textPopUpPrefabs, transform.position, text, col, new Vector2(Random.RandomRange(-1f, 1f), -1) * 10, false);
        }

    }

    void tileCaptured() {
        toCapturingZero = false;

        MapGenerator.MAIN.playSoundLandmarkCaptured();

        limitCapture = Mathf.Max(limitCapture - 1, 0);
        switch (type)
        {
            case TileType.LANDMARK:
                capturingLandmark();
                Color col = Color.blue;
                showText("Landmark captured!", col.r, col.g, col.b);
                if (currentPion != null)
                {
                    currentPion.SuccessCapture();
                }
                break;
            case TileType.CENTER_STRATEGY:
                capturingCenterStrategy();
                if (currentPion != null)
                {
                    currentPion.SuccessCapture();
                }
                break;
            case TileType.HOSPITAL:
                capturingHospital();
                break;
            case TileType.EMBASSY:
                capturingEmbasy();
                if (currentPion != null)
                {
                    currentPion.SuccessCapture();
                }
                break;
        }
        setMapIndicator();
        

    }
    void tileNetraled()
    {
        toCapturingZero = false;
        switch (type)
        {
            case TileType.LANDMARK:
                capturingLandmark();
                break;
        }
        setMapIndicator();
    }
#region capturing

    void capturingLandmark() {
        Debug.Log(owner);
        if (MapGenerator.MAIN.isOnline)
        {
            MapGenerator.MAIN.pv.RPC("setLandmarkOwnerSprite", RpcTarget.All, x, y, owner);
        }
        else {
            MapGenerator.MAIN.setLandmarkOwnerSprite(x, y, owner);
        }
    }
    void capturingHospital()
    {
        currentPion.Heal(Random.RandomRange(500000, 7000000));
        owner = TileOwner.NEUTRAL;
        capturePoin = 0;

        if (limitCapture == 0) {
            destroyAddon();
            if (currentPion != null)
            {
                currentPion.SuccessCapture();
            }
        }
        else {
            isCapturing = true;
        }

    }
    void capturingCenterStrategy()
    {
        BuffStatusScriptableObject buff = RoyaleHeroesLibs.getBuffData("Center Strategy Buff");

        foreach (Pion p in MapGenerator.MAIN.armyParent.playerPion) {
            p.addBuff(buff);
        }
        destroyAddon();

        Color col = Color.yellow;
        showText("Buff Added to all army" ,col.r , col.g , col.b );
    }
    void capturingEmbasy()
    {

        bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
        if (isMaster)
        {
            MapGenerator.MAIN.manager.addMasterBonusPoin(1);
            MapGenerator.MAIN.manager.addMasterPoin(10);
        }
        else {
            MapGenerator.MAIN.manager.pv.RPC("addClientBonusPoin",RpcTarget.MasterClient ,1);
            MapGenerator.MAIN.manager.pv.RPC("addClientPoin", RpcTarget.MasterClient, 10);
        }

        destroyAddon();

        Color col = Color.yellow;
        showText("You got 50% bonus poin",  col.r, col.g, col.b);
    }


#endregion

    void destroyAddon() {
        owner = TileOwner.NEUTRAL;
        type = TileType.GROUND;
        limitCapture = -1;
        anim.Play("addonDestroy");

        if (capturePoin > 0) {
            isCapturing = false;
            toCapturingZero = true;

            capturePoin = 10;
        }



    }

    public void spriteToNull() {
        addon.sprite = null;
    }

    void setScaleCapture() {

        if (MapGenerator.MAIN == null)
            return;

        float percent = (capturePoin / poinToCapture);
        float scale = 0;
        //Debug.Log(percent+" - "+capturePoin);
        if (percent > 0.05f)
        {
            scale = Mathf.Lerp(0.7f, 1.4f,  Mathf.InverseLerp( 0.05f , 1f , percent)  );
        }
        else {
            scale = Mathf.Lerp(0f, 0.7f, percent*20);
        }

        if (owner == TileOwner.NEUTRAL)
        { // lagi capture
            if (MapGenerator.MAIN.isOnline)//online condition
            {
                if (isCapturing)
                {// jika kita yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorMine, percent);
                }
                else
                {// jika musuh yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorEnemy, percent);
                }
            }
            else {
                //Debug.Log(targetOwner);
                if (targetOwner == TileOwner.MASTER)
                {// jika kita yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorMine, percent);
                }
                else
                {// jika musuh yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorEnemy, percent);
                }
            }
        }
        else if (percent != 1)
        { // lagi ngelepas capture
            if (MapGenerator.MAIN.isOnline)//online condition
            {
                if (isCapturing)
                {// jika kita yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorEnemy, percent);
                }
                else
                {// jika musuh yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorMine, percent);
                }
            }
            else {
                if (targetOwner == TileOwner.MASTER)
                {// jika kita yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorEnemy, percent);
                }
                else
                {// jika musuh yang capture
                    captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, MapGenerator.MAIN.colorMine, percent);
                }
            }
        }
        else {
            bool isMaster = MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true;
            captureLevel.GetComponent<SpriteRenderer>().color = Color.Lerp(MapGenerator.MAIN.colorNeutral, (isMaster == (owner==TileOwner.MASTER)? MapGenerator.MAIN.colorMine : MapGenerator.MAIN.colorEnemy ), percent);
        }

        captureLevel.localScale = new Vector3(scale, scale, scale);

        captureEfectLeft.Rotate(new Vector3(0, 0, -1));
        captureEfectRight.Rotate(new Vector3(0, 0, 1));

        Color col = Color.white;

        if (percent < 0.4f)
        {
            col.a = Mathf.InverseLerp(0, 0.4f, percent);
            captureEfectLeft.GetComponent<SpriteRenderer>().color = col;
            captureEfectRight.GetComponent<SpriteRenderer>().color = col;
        }
        else if(percent > 0.9f) {
            col.a = Mathf.InverseLerp(1f, 0.9f, percent);
            captureEfectLeft.GetComponent<SpriteRenderer>().color = col;
            captureEfectRight.GetComponent<SpriteRenderer>().color = col;
        }

        

    }


    public Pion getCurrentPion() {
        if (isOccupied)
        {
            if (currentPion != null)
            {
                return currentPion;
            }
            else {
                if (pionID == -1) {
                    return null;
                }
                PhotonView v = PhotonView.Find(pionID);
                if (v == null) {
                    return null;
                }

                return v.GetComponent<Pion>();
            }
        }
        else {
            return null;
        }
    }

    public void setCurrentPion(Pion p)
    {
        isOccupied = true;
        currentPion = p;
        if (MapGenerator.MAIN.isOnline)
        {
            pionID = p.pv.ViewID;
        }
        existChange = true;
    }    

    public void setSprite(Sprite sprite) {
        addon.sprite = sprite;
    }

    public void setCanMove( Sprite sprite) {
        //canMove = isCanMove;
        setSprite(sprite);
    }
    public void setCanMove(Sprite sprite , TileType type)
    {
        this.type = type;
        existChange = true;
        setCanMove(sprite);
    }

    void takeOver()
    {
        if (!MapGenerator.MAIN.isOnline) {
            return;
        }

        if (! (pv.Owner.UserId == PhotonNetwork.LocalPlayer.UserId))
        {
            pv.TransferOwnership(PhotonNetwork.LocalPlayer);
        }
    }

    /*void ownerChanged() {
        if (owner == TileOwner.NEUTRAL)
        {
            captureLevel.GetComponent<SpriteRenderer>().color = MapGenerator.MAIN.colorNeutral;
        }
        else {
            if ((owner == TileOwner.MASTER) == PhotonNetwork.IsMasterClient)
            {//jika milik saya
                captureLevel.GetComponent<SpriteRenderer>().color = MapGenerator.MAIN.colorMine;
            }
            else {
                captureLevel.GetComponent<SpriteRenderer>().color = MapGenerator.MAIN.colorEnemy;
            }
        }
    }
    */


    bool existChange = false;
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
        
        if (stream.IsWriting)
        {
            if (existChange) {
                Debug.Log("Writing Serialiaze");
                stream.SendNext(isOccupied);
                stream.SendNext(type);
                stream.SendNext(owner);
                stream.SendNext(pionID);
                stream.SendNext(capturePoin);
                existChange = false;
            }
            
        }
        else
        {
            //Debug.Log("accepting Serialiaze");
            this._isOccupied = (bool)stream.ReceiveNext();
            TileType typeTemp = (TileType)stream.ReceiveNext();
            this._owner = (TileOwner)stream.ReceiveNext();
            this.pionID = (int)stream.ReceiveNext();
            this._capturePoin = (float)stream.ReceiveNext();

            

            if (_type != TileType.GROUND && typeTemp == TileType.GROUND) {
                destroyAddon();
            }else
            {
                this._type = typeTemp;
            }
            setMapIndicator();
            setScaleCapture();

            //ownerChanged();
        }
    }

    void setMapIndicator() {

        if (type == TileType.GROUND || type == TileType.OBSTACLE ||  type == TileType.WALL)
        {
            mapIndicator.gameObject.SetActive(false);
        }
        else {
            mapIndicator.gameObject.SetActive(true);

            if (type == TileType.LANDMARK)
            {
                Debug.Log("Landmark!");
                mapIndicator.transform.localScale = new Vector3(9, 9, 1);
                if (owner == TileOwner.NEUTRAL)
                {
                    mapIndicator.color = new Color(0.65f, 0.65f, 0.65f);
                }
                else if ((owner == TileOwner.MASTER) == (MapGenerator.MAIN.isOnline ? PhotonNetwork.IsMasterClient : true))
                {
                    mapIndicator.color = new Color(0.2f, 0.5f, 0.9f);
                }
                else
                {
                    mapIndicator.color = new Color(1f, 0.4f, 0.1f);
                }
            }
            else
            {
                mapIndicator.color = new Color(0.5f , 0.9f , 0.3f);
            }

        }

    }

    public void setBaseGroundSprite(int x, int y) {
        baseGround.sprite = MapGenerator.MAIN.getGroundSprite(x, y);
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        pv = GetComponent<PhotonView>();
        pv.ObservedComponents[0] = this;
        PhotonNetwork.AddCallbackTarget(this);

        Vector2 index = MapGenerator.MAIN.getMapTileIndexFromWorld(transform.position);
        x = (int)index.x;
        y = (int)index.y;
        MapGenerator.MAIN.addMapTile((int)index.x, (int)index.y, this);
        name += " (" + (int)index.x + "," + (int)index.y + ")";

        baseGround.sprite = MapGenerator.MAIN.getGroundSprite(x, y);

        


        if (PhotonNetwork.IsMasterClient) {
            rotateAddon(new Vector3(0, 0, 180));
        }

        //_owner = TileOwner.NEUTRAL;

        //throw new System.NotImplementedException();
        //GetComponent<PhotonView>()
    }

    public void rotateAddon(Vector3 vec) {
        addon.transform.Rotate(vec);
    }


    public void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
}
