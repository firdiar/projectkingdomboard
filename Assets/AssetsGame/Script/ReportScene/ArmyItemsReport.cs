﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmyItemsReport : MonoBehaviour
{
    [SerializeField] Text heroName;
    [SerializeField] Image heroIcon;
    [SerializeField] Image amountXp;
    [SerializeField] Image amountDie;

    float percentDie = 0;
    float startXp = 0;
    float targetXp = 0;
    float maxXp = 0;

    float currentVal = -1;

    public void startAnim() {
        currentVal = 0;
    }

    // Start is called before the first frame update
    public void Init(string heroName, int expGain, float percentDie)
    {
        HeroesData data = Account.CurrentAccountData.getHeroes(heroName, RoyaleHeroesLibs.getHeroesData(heroName).heroesClass);

        ExperienceData dateLevel = RoyaleHeroesLibs.getExperienceData("HeroesLevel");
        maxXp = dateLevel.listLevel[data.level - 1].experienceTarget;

        targetXp = Mathf.Min(expGain + data.xp, maxXp);

        this.percentDie = percentDie;

        HeroesDataScriptableObject hData= RoyaleHeroesLibs.getHeroesData(heroName);
        heroIcon.sprite = hData.icon;
        this.heroName.text = heroName;

        updateByValue(0);

        Debug.Log("Hero Exp : " + Account.CurrentAccountData.getHeroes(heroName, RoyaleHeroesLibs.getHeroesData(heroName).heroesClass).xp);
        data.addXP(expGain);
        Debug.Log("Hero Exp : "+Account.CurrentAccountData.getHeroes(heroName, RoyaleHeroesLibs.getHeroesData(heroName).heroesClass).xp);
    }

    void updateByValue( float val) {
        amountXp.fillAmount = Mathf.InverseLerp(0, maxXp, Mathf.Lerp(startXp, targetXp, val));
        amountDie.fillAmount = 1 - (val * percentDie);

    }

    // Update is called once per frame
    void Update()
    {
        if (currentVal != -1)
        {
            currentVal = Mathf.Min(currentVal + Time.deltaTime / 3f, 1);
            updateByValue(currentVal);
            if (currentVal == 1)
            {
                currentVal = -1;
            }
        }
    }
}
