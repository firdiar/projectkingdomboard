﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReportSceneManager : MonoBehaviour
{

    GameSceneManager.BattleResult result;
    [SerializeField] Animation layerAnim = null;
    [SerializeField] Animation reportAnim = null;

    [SerializeField] StatusReportScene status = null;
    [SerializeField] ArmyReportScene army = null;

    private void Start()
    {
        result = GameSceneManager.BattleResult.battleResult;

        int rewardXp = calculateResultToGetExp(result);
        int rewardPoin = calculateResultToGetChPoin(result);

        float infDie = -1;
        float arcDie = -1;
        float cavDie = -1;
        float magDie = -1;

        if (result.infantryTotal != 0) {
            infDie = result.infantryLoss / (float)result.infantryTotal;
        }
        if (result.archerTotal != 0)
        {
            arcDie = result.archerLoss / (float)result.archerTotal;
        }
        if (result.cavalaryTotal != 0)
        {
            cavDie = result.cavalaryLoss / (float)result.cavalaryTotal;
        }
        if (result.magicianTotal != 0)
        {
            magDie = result.magicianLoss / (float)result.magicianTotal;
        }


        float totalDie = result.getTotalDie() / (float)result.getTotalArmy();



        //init status tab
        status.init(result.resourceLoss , rewardXp , rewardPoin , infDie , arcDie , cavDie , magDie , totalDie );

        


        //init army tab

        Dictionary<string, Vector2> heroData = new Dictionary<string, Vector2>();
        //ExperienceData expHero =  RoyaleHeroesLibs.getExperienceData("");
        foreach (KeyValuePair<string , int> val in result.armyLoss) {
            int total = result.armyPrevious[val.Key];

            int bonus = 0;
            if (result.armyBonusXp.ContainsKey(val.Key)) {
                bonus = result.armyBonusXp[val.Key];
            }
            int xpHero = calculateXpGainHero(total, val.Value) + bonus;

            //Account.CurrentAccountData.getHeroes();

            heroData.Add(val.Key, new Vector2(xpHero, val.Value / (float)total));

            
        }

        army.Init(heroData);


        ExperienceData dateLevel = RoyaleHeroesLibs.getExperienceData("PlayerLevel");
        ExperienceData chLevel = RoyaleHeroesLibs.getExperienceData("ChapterLevel");

        int maxXp = dateLevel.listLevel[Account.CurrentAccountData.level - 1].experienceTarget;
        int maxPoin = chLevel.listLevel[Account.CurrentAccountData.tierLevel - 1].experienceTarget;

        Account.CurrentAccountData.exp += rewardXp;
        

        if (Account.CurrentAccountData.tierLevel == 1)
        {
            Account.CurrentAccountData.tierPoin = Mathf.Clamp(Account.CurrentAccountData.tierPoin + rewardPoin , 1 , 999999);
        }
        else {
            Account.CurrentAccountData.tierPoin += rewardPoin;
        }

        if (maxXp <= Account.CurrentAccountData.exp) {
            Account.playerLevelUP();
        }


        if (maxPoin <= Account.CurrentAccountData.tierPoin)
        {
            Account.playerTierUp();
        }
        else if (Account.CurrentAccountData.tierPoin <= 0) {
            Account.playerTierDown();
        }


        RoyaleHeroesLibs.SaveAccountData();
    }

    public void LayerOpened() {
        status.startAnim();
    }

    bool volDown = false;

    [SerializeField] new AudioSource audios;

    private void Update()
    {
        if (volDown) {
            audios.volume -= Time.deltaTime;
            if (audios.volume == 0) {
                volDown = false;
            }
        }
    }

    public void LayerClosed()
    {
        volDown = true;
        RoyaleHeroesLoading.openLayerLoading(()=> {
            //move to mainmenu scene here
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");

        });
    }

    bool isOpened = false;

    public void openLayer() {
        if (isOpened)
            return;

        isOpened = true;
        layerAnim.Play("openLayer");
    }

    bool currentIsStatus = true;
    public void flip() {

        if (currentIsStatus)
        {
            reportAnim.Play("FlipArmy");
            army.Invoke("startAnim", 1.2f);
        }
        else {
            reportAnim.Play("FlipStatus");
        }

        currentIsStatus = !currentIsStatus;
    }

    int calculateXpGainHero(int totalArmy , int totalDeath) {

        float percent = totalDeath / (float)totalArmy;

        float xp = 0;
        if (percent > 0.8f)
        {
            xp = Mathf.Sqrt((1 - percent))*10;
        }
        else
        {
            xp = Mathf.Sqrt(percent) * 10;
        }


        return Mathf.RoundToInt( xp ) + 1;
    }


    public void gotoMainMenu() {
        layerAnim.Play("closeLayerReport");
    }

    int calculateResultToGetExp(GameSceneManager.BattleResult res)
    {
        float percent = 0;
        if (res.isWin)
        {
            percent = (res.getTotalDie() / (float)res.getTotalArmy() ) + 0.5f;
        }
        else {
            percent = res.getTotalDie() / (float)res.getTotalArmy();
        }

        int xp = Mathf.CeilToInt( 10 * percent );

        ExperienceData dateLevel = RoyaleHeroesLibs.getExperienceData("PlayerLevel");
        int maxXp = dateLevel.listLevel[Account.CurrentAccountData.level - 1].experienceTarget;

        maxXp = maxXp - Account.CurrentAccountData.exp;

        xp = Mathf.Min( xp , maxXp) ;

        return xp;
    }

    int calculateResultToGetChPoin(GameSceneManager.BattleResult res)
    {
        if (res.isWin)
        {
            return 10 - Account.CurrentAccountData.tierLevel;
        }
        else {
            int poinminus =  -Mathf.RoundToInt(Account.CurrentAccountData.tierLevel*10/2f);

            return poinminus;
        }
    }

    float getPercentArmyDie(GameSceneManager.BattleResult res) {

        return  res.getTotalDie()/(float)res.getTotalArmy();
    }

    public void opanReport() {

    }
    
}
