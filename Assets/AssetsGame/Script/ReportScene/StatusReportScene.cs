﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusReportScene : MonoBehaviour
{

    [Header("UI")]
    [SerializeField] Text xpPlus;
    [SerializeField] RectTransform parentXp;
    [SerializeField] RectTransform fillAmountXP;
    [SerializeField] Text poinPlus;
    [SerializeField] RectTransform parentPoin;
    [SerializeField] RectTransform fillAmountPoin;
    [SerializeField] Text food;
    [SerializeField] Text wood;
    [SerializeField] Text stone;

    [SerializeField] Image percentDie;
    [SerializeField] Text textPercentDie;
    [SerializeField] Image infDie;
    [SerializeField] Image archDie;
    [SerializeField] Image cavDie;
    [SerializeField] Image magDie;
    [SerializeField] Color green;
    [SerializeField] Color yellow;
    [SerializeField] Color red;



    public int targetFood = 100000;
    public int targetWood = 100000;
    public int targetStone = 100000;

    float startXp = 0;
    float startPoin = 0;

    float targetXp = 100;
    float targetPoin = 100;


    float maxXp = 0;
    float maxPoin = 0;

    float percentInf = 0;
    float percentArc = 0;
    float percentCav = 0;
    float percentMag = 0;

    float percentTotal = 0;


    bool animStarted = false;

    public void startAnim() {

        if (animStarted)
            return;

        animStarted = true;
        currentVal = 0;
    }

    public void init(ResourcesMaterial cost , float rewardXp , float rewardPoin , float percentInf , float percentArc , float percentCav , float percentMag , float percentTotal ) {
        ExperienceData dateLevel = RoyaleHeroesLibs.getExperienceData("PlayerLevel");
        ExperienceData chLevel = RoyaleHeroesLibs.getExperienceData("ChapterLevel");
        maxXp = dateLevel.listLevel[Account.CurrentAccountData.level - 1].experienceTarget;
        maxPoin = chLevel.listLevel[Account.CurrentAccountData.tierLevel - 1].experienceTarget;

        targetFood = cost.food;
        targetWood = cost.wood;
        targetStone = cost.stone;

        Account.CurrentAccountData.resources.food = Mathf.Max(Account.CurrentAccountData.resources.food - cost.food, 0);
        Account.CurrentAccountData.resources.wood = Mathf.Max(Account.CurrentAccountData.resources.wood-cost.wood , 0);
        Account.CurrentAccountData.resources.stone = Mathf.Max(Account.CurrentAccountData.resources.stone - cost.stone, 0);

        setXpAmount(Account.CurrentAccountData.exp / maxXp);
        setPoinAmount(Account.CurrentAccountData.tierPoin / maxPoin);

        startXp = Account.CurrentAccountData.exp;
        startPoin = Account.CurrentAccountData.tierPoin;

        targetXp = Mathf.Min( Account.CurrentAccountData.exp + rewardXp , maxXp);
        targetPoin = Mathf.Min(Account.CurrentAccountData.tierPoin + rewardPoin , maxPoin);

        this.percentInf = percentInf;
        this.percentArc = percentArc;
        this.percentCav = percentCav;
        this.percentMag = percentMag;
        this.percentTotal = percentTotal;

        xpPlus.text = "( + " + rewardXp + " )";
        if (rewardPoin >= 0)
        {
            poinPlus.text = "( + " + rewardPoin + " )";
        }
        else {
            poinPlus.text = "( " + rewardPoin + " )";
        }
        

        food.text = "0";
        wood.text = "0";
        stone.text = "0";

        setInfAmount(1);
        setArcAmount(1);
        setCavAmount(1);
        setMagAmount(1);


        //updateByValue(0.001f);
        RoyaleHeroesLoading.hideLayerLoading();

    }

    void setXpAmount(float val) {

        Debug.Log(val);
        float minWidth = -parentXp.rect.width;
        Vector2 size = fillAmountXP.sizeDelta;
        size.x = Mathf.Lerp(minWidth, 0, val);
        fillAmountXP.sizeDelta = size;
    }

    void setPoinAmount(float val)
    {
        float minWidth = -parentPoin.rect.width;
        Vector2 size = fillAmountPoin.sizeDelta;
        size.x = Mathf.Lerp(minWidth, 0, val);
        fillAmountPoin.sizeDelta = size;
    }

    void setFoodTxt(float val) {
        food.text = RoyaleHeroesLibs.intToStringK(Mathf.RoundToInt(Mathf.Lerp(0, targetFood, val)));
    }
    void setWoodTxt(float val)
    {
        wood.text = RoyaleHeroesLibs.intToStringK(Mathf.RoundToInt(Mathf.Lerp(0, targetWood, val)));
    }
    void setStoneTxt(float val)
    {
        stone.text = RoyaleHeroesLibs.intToStringK(Mathf.RoundToInt(Mathf.Lerp(0, targetStone, val)));
    }

    void setInfAmount(float val) {
        if (percentInf == -1) {
            infDie.fillAmount = 1;
            infDie.color = Color.gray;
            return;
        }

        infDie.fillAmount = 1-val;
        if (1 - val < 0.2f)
        {
            infDie.color = red;
        }
        else if (1 - val < 0.6f)
        {
            infDie.color = yellow;
        }
        else {
            infDie.color = green;
        }
    }
    void setArcAmount(float val)
    {
        if (percentArc == -1)
        {
            archDie.fillAmount = 1;
            archDie.color = Color.gray;
            return;
        }
        archDie.fillAmount = 1-val;
        if (1 - val < 0.2f)
        {
            archDie.color = red;
        }
        else if (1 - val < 0.6f)
        {
            archDie.color = yellow;
        }
        else
        {
            archDie.color = green;
        }
    }
    void setCavAmount(float val)
    {
        if (percentCav == -1)
        {
            cavDie.fillAmount = 1;
            cavDie.color = Color.gray;
            return;
        }
        cavDie.fillAmount = 1-val;
        if (1 - val < 0.2f)
        {
            cavDie.color = red;
        }
        else if (1 - val < 0.6f)
        {
            cavDie.color = yellow;
        }
        else
        {
            cavDie.color = green;
        }
    }
    void setMagAmount(float val)
    {
        if (percentMag == -1)
        {
            magDie.fillAmount = 1;
            magDie.color = Color.gray;
            return;
        }
        magDie.fillAmount = 1-val;
        if (1 - val < 0.2f)
        {
            magDie.color = red;
        }
        else if (1 - val < 0.6f)
        {
            magDie.color = yellow;
        }
        else
        {
            magDie.color = green;
        }
    }

    void setPercentDieAmount(float val) {
        percentDie.fillAmount = val;
        float percent = Mathf.Floor(val*1000) / 10f;
        textPercentDie.text = percent.ToString()+" %";
    }




    float currentVal = -1;

    void updateByValue(float val) {
        setFoodTxt(val);
        setWoodTxt(val);
        setStoneTxt(val);

        float amountXP = Mathf.InverseLerp(0, maxXp, Mathf.Lerp(startXp, targetXp, val));
        Debug.Log(Account.CurrentAccountData.exp+" - "+targetXp+" - "+amountXP);
        setXpAmount(amountXP);

        float amountPoin = Mathf.InverseLerp(0, maxPoin, Mathf.Lerp(startPoin, targetPoin, val));
        setPoinAmount(amountPoin);

        setInfAmount(percentInf * val);
        setArcAmount(percentArc * val);
        setCavAmount(percentCav * val);
        setMagAmount(percentMag * val);
        setPercentDieAmount(percentTotal * val);

    }


    // Update is called once per frame
    void Update()
    {
        if (currentVal != -1)
        {
            currentVal = Mathf.Min( currentVal + Time.deltaTime / 3f  , 1) ;
            updateByValue(currentVal);
            if (currentVal == 1)
            {
                currentVal = -1;
            }
        }

    }
}
