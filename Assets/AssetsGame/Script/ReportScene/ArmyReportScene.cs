﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmyReportScene : MonoBehaviour
{
    [SerializeField] Transform parentContent;
    [SerializeField] ArmyItemsReport prefabs;


    
    List<ArmyItemsReport> items = new List<ArmyItemsReport>();

    // Start is called before the first frame update
    public void Init(Dictionary<string, Vector2> heroData)
    {

        // x for xp , y for percenDeath
        foreach (KeyValuePair<string, Vector2> val in heroData)
        {
            ArmyItemsReport report = Instantiate(prefabs, parentContent);
            report.Init(val.Key, (int)val.Value.x, val.Value.y);
            items.Add(report);
        }

    }

    bool animStarted = false;

    public void startAnim()
    {
        if (animStarted)
            return;

        animStarted = true;
        foreach (ArmyItemsReport temp in items) {
            temp.startAnim();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
