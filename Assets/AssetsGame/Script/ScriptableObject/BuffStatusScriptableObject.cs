﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Buff", menuName = "BuffData/New")]
public class BuffStatusScriptableObject : ScriptableObject
{


    public string buffName;

    [TextArea]
    public string desc;

    [System.Serializable]
    public enum BuffType { BUFF , DEBUFF }

    [System.Serializable]
    public enum DeactiveCase{ MOVING , TIME , UNITCOUNT_GREATHER_THAN , UNITCOUNT_LESS_THAN , POWER_GREATHER_THAN , POWER_LESS_THAN  , NEARBY_ENEMY_COUNT_NOT_EQUAL , INFINITE }

    [System.Serializable]
    public enum ActiveCase { NONE , UNITCOUNT_GREATHER_THAN, UNITCOUNT_LESS_THAN, POWER_GREATHER_THAN, POWER_LESS_THAN}

    public BuffType type;

    public DeactiveCase caseDeactive;
    public float limit;
    public ActiveCase caseActive;
    public float requirement;

    public CharacterStatus status;
    public CharacterStatus getBuff(int troopsCount)
    {
        CharacterStatus c = new CharacterStatus();
        c.health = status.health * troopsCount;
        c.phyAttack = status.phyAttack * troopsCount;
        c.phyDeffense = status.phyDeffense * troopsCount;
        c.magAttack = status.magAttack * troopsCount;
        c.magDeffense = status.magDeffense * troopsCount;

        c.counterAttackRate = status.counterAttackRate;
        c.attackSpeed = status.attackSpeed;
        c.movementSpeed = status.movementSpeed;
        c.range = status.range;
        return c;
    }

    #region getStatus
    public int getHealth(int actualArmyCount)
    {
        return status.health * actualArmyCount;
    }
    public int getPhyAttack(int actualArmyCount)
    {
        return status.phyAttack * actualArmyCount;
    }
    public int getMagAttack(int actualArmyCount)
    {
        return status.magAttack * actualArmyCount;
    }
    public int getPhyDeff(int actualArmyCount)
    {
        return status.phyDeffense * actualArmyCount;
    }
    public int getMagDeff(int actualArmyCount)
    {
        return status.magDeffense * actualArmyCount;
    }
    public float getAttackSpeed()
    {
        return status.attackSpeed;
    }
    public float getMovementSpeed()
    {
        return status.movementSpeed;
    }
    public float getCounterAttackRate()
    {
        return status.counterAttackRate;
    }
    public int getRange()
    {
        return status.range;
    }
    #endregion

    public BuffStatusScriptableObject clone()
    {
        BuffStatusScriptableObject b = ScriptableObject.CreateInstance<BuffStatusScriptableObject>();

        b.status = new CharacterStatus();

        b.type = type;
        b.buffName = buffName;
        b.desc = desc;
        b.caseDeactive = caseDeactive;
        b.caseActive = caseActive;
        b.limit = limit;
        b.requirement = requirement;

        b.status.health = status.health;
        b.status.phyAttack = status.phyAttack;
        b.status.phyDeffense = status.phyDeffense;
        b.status.magAttack = status.magAttack;
        b.status.magDeffense = status.magDeffense;

        b.status.counterAttackRate = status.counterAttackRate;
        b.status.attackSpeed = status.attackSpeed;
        b.status.movementSpeed = status.movementSpeed;

        return b;
    }


}
