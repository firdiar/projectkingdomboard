﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tutorial_1", menuName = "TutorialData/New")]
public class TutorialSectionScriptableObject : ScriptableObject
{
    public string title = "";
    public string speakerName = "Gajah Mada";
    public Sprite speakerImage = null;
    public List<ImageAndDialog> imageAndDialogue = new List<ImageAndDialog>();
}

[System.Serializable]
public class ImageAndDialog {
    public Sprite image = null;
    [TextArea]
    public List<string> dialogue = new List<string>();
}
