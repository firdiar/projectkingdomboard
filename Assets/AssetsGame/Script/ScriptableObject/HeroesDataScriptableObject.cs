﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "HeroName", menuName = "HeroesData/New")]
public class HeroesDataScriptableObject : ScriptableObject
{
    
    public string heroName;
    public string nick;

    [TextArea]
    public string desc;

    public HeroesClass heroesClass;

    public Sprite icon;
    public Sprite idle;

    public HeroesStatus status;

    public HeroesSkill[] skill;

    public GameObject prefabsHeroes;

}

[System.Serializable]
public class CharacterStatus {
    public int health = 0;
    public int phyAttack = 0;
    public int phyDeffense = 0;
    public int magAttack = 0;
    public int magDeffense = 0;
    public float counterAttackRate = 0;
    public int range = 0;
    public float attackSpeed = 0;
    public float movementSpeed = 0;

    public static CharacterStatus operator+ (CharacterStatus a, CharacterStatus b) {
        CharacterStatus c = new CharacterStatus();
        c.health = a.health + b.health;
        c.phyAttack = a.phyAttack + b.phyAttack;
        c.phyDeffense = a.phyDeffense + b.phyDeffense;
        c.magAttack = a.magAttack + b.magAttack;
        c.magDeffense = a.magDeffense + b.magDeffense;
        c.counterAttackRate = a.counterAttackRate + b.counterAttackRate;
        c.range = a.range + b.range;
        c.attackSpeed = a.attackSpeed + b.attackSpeed;
        c.movementSpeed = a.movementSpeed + b.movementSpeed;

        return c;
    }

    public static CharacterStatus operator/ (CharacterStatus a, float b)
    {
        CharacterStatus c = new CharacterStatus();
        c.health = a.health;
        c.phyAttack = a.phyAttack;
        c.phyDeffense = a.phyDeffense;
        c.magAttack = a.magAttack;
        c.magDeffense = a.magDeffense;
        c.counterAttackRate = a.counterAttackRate/b;
        c.range = (int)Mathf.Round( a.range/b );
        c.attackSpeed = a.attackSpeed / b;
        c.movementSpeed = Mathf.Round( a.movementSpeed / b );

        return c;
    }
    public static CharacterStatus operator* (CharacterStatus a, int b)
    {
        CharacterStatus c = new CharacterStatus();
        c.health = a.health*b;
        c.phyAttack = a.phyAttack*b;
        c.phyDeffense = a.phyDeffense*b;
        c.magAttack = a.magAttack*b;
        c.magDeffense = a.magDeffense*b;
        c.counterAttackRate = a.counterAttackRate * b;
        c.range = (int)Mathf.Round(a.range * b);
        c.attackSpeed = a.attackSpeed * b;
        c.movementSpeed = a.movementSpeed * b;

        return c;
    }
}



[System.Serializable]
public class HeroesStatus : CharacterStatus{

    public int maxTroops(int level) {
        return level * 10000;
    }

}

[System.Serializable]
public class HeroesSkill
{
    public string name;
    public Sprite icon;
    [TextArea]
    public string description;
    public bool isActiveSkill;
}

[System.Serializable]
public enum HeroesClass
{
    Infantry , Archer , Cavalary , Magician
}


