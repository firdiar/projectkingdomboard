﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Class", menuName = "ArmyData/New")]
public class ArmyDataScriptableObject : ScriptableObject
{
    public string armyName;
    public HeroesClass classArmy;
    public CharacterStatus status;
    public string description;
    public ResourcesMaterial requirement;
}

