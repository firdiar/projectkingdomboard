﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Class", menuName = "ArmyData/Holder")]
public class ArmyDataHolderScriptableObject : ScriptableObject
{
    public ArmyDataScriptableObject[] infantry;
    //public Sprite iconInfantry;
    public ArmyDataScriptableObject[] archer;
    //public Sprite iconArcher;
    public ArmyDataScriptableObject[] cavalary;
    //public Sprite iconCavalary;
    public ArmyDataScriptableObject[] magician;
    //public Sprite iconMagician;

}
