﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Account {
    private static Account _currentAccountData;
    public static Account CurrentAccountData {
        get {
            if (_currentAccountData != null)
            {
                return _currentAccountData;
            }
            else {
                _currentAccountData = generateNewPlayerData("UserTest");
                return _currentAccountData;
            }

           
        }
        set {
            _currentAccountData = value;
        }
    }
    public string token { get; set; }
    public Heroes heroes { get; set; }
    public string nickName { get; set; }
    public int exp { get; set; }
    public int level { get; set; }
    public int tierLevel { get; set; }
    public int tierPoin { get; set; }
    public bool connected { get; set; }
    public ResourcesMaterial resources { get; set; }
    public ResourcesProductionData production { get; set; }

    public HeroesData getHeroes(string name , HeroesClass heroesClass) {

        switch (heroesClass) {
            case HeroesClass.Infantry:
                foreach (HeroesData h in heroes.Infantry) {
                    if (h.name == name) {
                        return h;
                    }
                }
                break;
            case HeroesClass.Cavalary:
                foreach (HeroesData h in heroes.Cavalary)
                {
                    if (h.name == name)
                    {
                        return h;
                    }
                }
                break;
            case HeroesClass.Archer:
                foreach (HeroesData h in heroes.Archer)
                {
                    if (h.name == name)
                    {
                        return h;
                    }
                }
                break;
            case HeroesClass.Magician:
                foreach (HeroesData h in heroes.Magician)
                {
                    if (h.name == name)
                    {
                        return h;
                    }
                }
                break;
        }
        return null;
    }
    

    public static void playerLevelUP() {
        CurrentAccountData.production.idleWork++;
        CurrentAccountData.level++;
        CurrentAccountData.exp = 0;
    }
    public static void playerTierUp() {
        CurrentAccountData.tierLevel++;
        CurrentAccountData.tierPoin = 0;
    }
    public static void playerTierDown()
    {
        CurrentAccountData.tierLevel--;
        CurrentAccountData.tierPoin = (40*(2^CurrentAccountData.tierLevel))-21;
    }
    public static void addResource(int value , ResourcesMaterialType type ) {
        switch (type) {
            case ResourcesMaterialType.Food:
                CurrentAccountData.resources.food = Mathf.Clamp(CurrentAccountData.resources.food + value, 0, 1000000000);
                break;
            case ResourcesMaterialType.Wood:
                CurrentAccountData.resources.wood = Mathf.Clamp(CurrentAccountData.resources.wood + value, 0, 1000000000);
                break;
            case ResourcesMaterialType.Stone:
                CurrentAccountData.resources.stone = Mathf.Clamp(CurrentAccountData.resources.stone + value, 0, 1000000000);

                break;
            case ResourcesMaterialType.Gold:
                CurrentAccountData.resources.gold = Mathf.Clamp(CurrentAccountData.resources.gold + value, 0, 1000000000);
                break;
        }
    }
    public static void addHeroesCard(string hName , HeroesClass hClass)
    {
        HeroesData result = CurrentAccountData.getHeroes(hName, hClass);
        if (result == null)
        {
            HeroesData newHeroes = new HeroesData(hName);
            CurrentAccountData.heroes.addHeroes(newHeroes, hClass);
            result = newHeroes;

            Debug.Log("New Heroes");
        }
        else {
            result.addCard(1);
        }
        
    }
    public static void addHeroesXP(string hName, HeroesClass hClass, int xp)
    {
        HeroesData result = CurrentAccountData.getHeroes(hName, hClass);
        if (result == null) {
            return;
        }
        result.addXP(xp);
    }
    public static Account generateNewPlayerData(string nickName)
    {
        Account acc = new Account();

        acc.token = PlayerPrefs.GetString("userToken", "");

        acc.resources = new ResourcesMaterial();
        acc.resources.wood = 250000;
        acc.resources.stone = 250000;
        acc.resources.food = 250000;
        acc.resources.gold = 50000;

        acc.production = new ResourcesProductionData();
        acc.production.idleWork = 0;
        acc.production.foodWork = 1;
        acc.production.woodWork = 1;
        acc.production.stoneWork = 1;
        acc.production.resFood = 0;
        acc.production.resWood = 0;
        acc.production.resStone = 0;
        acc.production.lastCheckpoint = new System.DateTime(System.DateTime.Now.AddDays(-1).Year , System.DateTime.Now.AddDays(-1).Month, System.DateTime.Now.AddDays(-1).Day );

        acc.nickName = nickName;

        acc.exp = 0;

        acc.level = 1;

        acc.tierLevel = 1;

        acc.tierPoin = 0;

        acc.connected = true;

        acc.heroes = new Heroes();

        //Dihapus nanti
        //acc.heroes.Archer.Add(new HeroesData("Emiya Shiro"));
        //acc.heroes.Infantry.Add(new HeroesData("Charles Martel"));
        //acc.heroes.Cavalary.Add(new HeroesData("Jeane D Arc"));
        //acc.heroes.Magician.Add(new HeroesData("Merlin Ambrosinus"));


        //StarterHero
        acc.heroes.Magician.Add(new HeroesData("Commander Shahnaz"));
        acc.heroes.Magician.Add(new HeroesData("Commander Clara"));//Del Later

        acc.heroes.Infantry.Add(new HeroesData("Commander Noverio"));
        acc.heroes.Infantry.Add(new HeroesData("Commander Julius"));

        acc.heroes.Cavalary.Add(new HeroesData("Commander Alicia"));
        acc.heroes.Cavalary.Add(new HeroesData("Commander Reina"));//Del Later

        acc.heroes.Archer.Add(new HeroesData("Commander Ben"));
        acc.heroes.Archer.Add(new HeroesData("Commander Axel"));

        //del later

        acc.heroes.Archer.Add(new HeroesData("Keumalahayati"));
        acc.heroes.Infantry.Add(new HeroesData("Gajah Mada"));
        acc.heroes.Magician.Add(new HeroesData("Prabu Siliwangi"));
        acc.heroes.Cavalary.Add(new HeroesData("Prince Dipenogoro"));
        return acc;
    }

    public static void SaveAccount() {
        RoyaleHeroesLibs.SaveAccountData();
    }

}

public class Heroes
{
    public List<HeroesData> Archer = new List<HeroesData>();
    public List<HeroesData> Cavalary = new List<HeroesData>();
    public List<HeroesData> Infantry = new List<HeroesData>();
    public List<HeroesData> Magician = new List<HeroesData>();
    public void addHeroes(HeroesData data , HeroesClass heroesClass) {
        switch (heroesClass)
        {
            case HeroesClass.Infantry:
                Infantry.Add(data);
                break;
            case HeroesClass.Cavalary:
                Cavalary.Add(data);
                break;
            case HeroesClass.Archer:
                Archer.Add(data);
                break;
            case HeroesClass.Magician:
                Magician.Add(data);
                break;
        }
    }
}

public class HeroesData
{

    /// <summary>
    /// Generate new Heroes with 0 for all stat
    /// </summary>
    /// <param name="name">Name of Heroes</param>
    public HeroesData(string name) {
        this.name = name;
        this.card = 1;
        this.level = 1;
        this.xp = 0;
    }

    /// <summary>
    /// Load Heroes Data
    /// </summary>
    /// <param name="name">Name of heroes</param>
    /// <param name="card">String count of card that player have</param>
    /// <param name="xp">experience of this heroes</param>
    /// <param name="level">level of this heroes</param>
    public HeroesData(string name,int card , int xp, int level)
    {
        this.name = name;
        this.card = card;
        this.level = level;
        this.xp = xp;
    }



    public int card { get; set; }
    public int level { get; set; }
    public string name { get; set; }
    public int xp { get; set; }

    public void checkLevelUp() {
        ExperienceData xpData = RoyaleHeroesLibs.getExperienceData("HeroesLevel");
        ExperienceData cardData = RoyaleHeroesLibs.getExperienceData("HeroesCard");
        if (xpData.listLevel[level - 1].experienceTarget <= xp && cardData.listLevel[level - 1].experienceTarget <= card) {
            xp = 0;
            card -= cardData.listLevel[level - 1].experienceTarget;
            level++;
            Debug.Log("HeroesLevelUp! "+level);
            
        }
    }
    public void addXP(int value) {
        ExperienceData xpData = RoyaleHeroesLibs.getExperienceData("HeroesLevel");
        xp = Mathf.Clamp(xp + value, 0, xpData.listLevel[level - 1].experienceTarget);
        checkLevelUp();
    }
    public void addCard(int value)
    {
        card += value;
        checkLevelUp();
    }
}

[System.Serializable]
public class ResourcesMaterial
{
    public int food;
    public int gold;
    public int stone;
    public int wood;
    
    public static ResourcesMaterial operator +(ResourcesMaterial a , ResourcesMaterial b) {
        ResourcesMaterial c = new ResourcesMaterial();
        c.food = a.food + b.food;
        c.gold = a.gold + b.gold;
        c.stone = a.stone + b.stone;
        c.wood = a.wood + b.wood;
        return c;

    }

    public static ResourcesMaterial operator *(ResourcesMaterial a, float b)
    {
        ResourcesMaterial c = new ResourcesMaterial();
        c.food = Mathf.RoundToInt( a.food *b );
        c.gold = Mathf.RoundToInt(a.gold * b);
        c.stone = Mathf.RoundToInt(a.stone * b);
        c.wood = Mathf.RoundToInt(a.wood * b);
        return c;

    }

}


public enum ResourcesMaterialType
{
    Food , Wood , Stone , Gold
}

public class ResourcesProductionData
{
    public System.DateTime lastCheckpoint { get; set; }
    public int resFood { get; set; }
    public int resStone { get; set; }
    public int resWood { get; set; }
    public int foodWork { get; set; }
    public int woodWork { get; set; }
    public int stoneWork { get; set; }
    public int idleWork { get; set; }
}


