﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Experience", menuName = "UserData/Experience", order = 1)]
public class ExperienceData : ScriptableObject
{

    public List<LevelAndExperience> listLevel;
}

[System.Serializable]
public class LevelAndExperience {
    public int level;
    public int experienceTarget;
}
