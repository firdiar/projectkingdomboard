﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoyaleHeroesNestedScrollHorizontal : ScrollRect , IPointerUpHandler , IPointerDownHandler
{
    public ScrollRect mOutterScroll;
    public ScorllViewSnap mOutterSnap;

    bool draggingSide = false;
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        base.OnInitializePotentialDrag(eventData);
        mOutterScroll.OnInitializePotentialDrag(eventData);
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            //Vector2 newdelta = new Vector2(eventData.delta.x, 0);
            //eventData.delta = newdelta;
            mOutterScroll.OnBeginDrag(eventData);
            draggingSide = true;
        }
        else base.OnBeginDrag(eventData);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (draggingSide)
        {
            Vector2 newdelta = new Vector2(eventData.delta.x, 0);
            eventData.delta = newdelta;
            mOutterScroll.OnDrag(eventData);
        }
        else base.OnDrag(eventData);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (draggingSide)
        {
            mOutterScroll.OnEndDrag(eventData);
            draggingSide = false;
        }
        else base.OnEndDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        mOutterSnap.OnPointerUp(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        mOutterSnap.OnPointerDown(eventData);
    }
}
