﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoyaleHeroesNestedScrollOuter : MonoBehaviour
{
    [SerializeField]
    RoyaleHeroesNestedScroll[] mScrollTest;

    [SerializeField]
    RoyaleHeroesNestedScrollHorizontal[] mScrollTest2;

    private void Start()
    {

        foreach (RoyaleHeroesNestedScroll scroll in mScrollTest) {
                scroll.mOutterScroll = GetComponent<UnityEngine.UI.ScrollRect>();
        }

        foreach (RoyaleHeroesNestedScrollHorizontal scroll in mScrollTest2)
        {
            scroll.mOutterScroll = GetComponent<UnityEngine.UI.ScrollRect>();
        }

    }
}
