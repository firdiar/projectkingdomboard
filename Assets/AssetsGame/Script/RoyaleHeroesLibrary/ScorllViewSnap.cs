﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ScrollRect))]
public class ScorllViewSnap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    [SerializeField] int defaultItem = 0;
    [SerializeField] bool isHorizontal = true;
    [SerializeField] bool reversed = false;
    [Header("Clamp Free")]
    [SerializeField] bool clampFree = false;
    [SerializeField] float clampFreeDeaccelerationRate = 0.3f;
    [Header("Scale Middle")]
    [SerializeField] bool scaleMidle = false;
    [SerializeField] float scaleFactor = 0.3f;
    [SerializeField] float scaleDist = 1f;
    [Header("Scale Not Middle")]
    [SerializeField] bool scaleNotMidle = false;
    [SerializeField] float scaleFactorMin = 0.2f;
    [SerializeField] float scaleDistMin = 1f;
    [SerializeField] [Range(0, 1)] float acceleration = 0.15f;
    [SerializeField] HorizontalLayoutGroup contentH = null;
    [SerializeField] VerticalLayoutGroup contentV = null;
    ScrollRect scrollView;
    bool isInContent = false;
    bool inputFromUser = false;
    int currentIndex;


    float lengthView = 640;
    float lengthTotal = 1920;
    float lengthPerItem = 320;
    float padding = 0;

    public delegate void OnSelectedNewItemDelegate(int index);
    public OnSelectedNewItemDelegate onSelectedNewItemDelegate;

    private void Start()
    {
        Init();
        currentIndex = defaultItem;
    }
    public void Init()
    {
        //scrollView = GetComponent<ScrollRect>();
        //Debug.Log(GetComponent<RectTransform>().sizeDelta);
        currentIndex = -1;
        //Debug.Log(gameObject.name);
        scrollView = GetComponent<ScrollRect>();

        if (isHorizontal)
        {
            lengthView = GetComponent<RectTransform>().rect.width;
            if (contentH.transform.childCount != 0)
            {
                lengthPerItem = contentH.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + (contentH.spacing / 2.0f);
            }
            else
            {
                lengthPerItem = 0;
            }
            contentH.padding.left = (int)(lengthView/2);
            contentH.padding.right = (int)(lengthView / 2);
            padding = (int)(lengthView / 2);
            lengthTotal = lengthPerItem * contentH.transform.childCount + 2*padding;
        }
        else {

            lengthView = GetComponent<RectTransform>().rect.height;
            if (contentV.transform.childCount != 0)
            {
                lengthPerItem = contentV.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y + (contentV.spacing / 2.0f);
            }
            else
            {
                lengthPerItem = 0;
            }
        contentV.padding.top = (int)(lengthView / 2);
            contentV.padding.bottom = (int)(lengthView / 2);
            padding = (int)(lengthView / 2);
            lengthTotal = lengthPerItem * contentV.transform.childCount + 2 * padding;
            //Debug.Log(contentV.transform.childCount);
        }




        Invoke("setDefaultPosition", 0.2f);

        //setPosition(defaultItem);

        //Debug.Log(convertValueToPosition(640, 1920, 480));
    }

    //indexStartFrom 0
    float getTargetItemPosition(int index) {
        return index * lengthPerItem + (0.5f * lengthPerItem) + (padding);
    }
    public int getCurrentPosition() {
        return (currentIndex < 0 ? 0 : currentIndex);
    }

    public void setDefaultPosition() {
        setPosition(defaultItem);
    }
    public void setPosition(int index) {
        float targetPos = getTargetItemPosition(index);
        float percentage = convertValueToPosition(lengthView, lengthTotal, targetPos);

        //Debug.Log(percentage+" - "+lengthView+" - "+lengthTotal+" - "+targetPos);
        if (snapCoroutine != null) {
            StopCoroutine(snapCoroutine);
        }
        if (isActiveAndEnabled)
        {
            snapCoroutine = StartCoroutine(snap(percentage , index));
        }
    }

    Coroutine snapCoroutine = null;

    IEnumerator snap(float target , int index) {

        float progress = 0;
        if (isHorizontal)
        {
            while (Mathf.Abs(scrollView.horizontalNormalizedPosition - target) > 0.001f)
            {
                progress = Mathf.Min(progress + 0.15f, 1);
                scrollView.horizontalNormalizedPosition = Mathf.Lerp(scrollView.horizontalNormalizedPosition, target, progress);
                //Debug.Log(Mathf.Abs(scrollView.horizontalNormalizedPosition - target));
                yield return new WaitForSeconds(0.02f);
            }
        }
        else {
            while (Mathf.Abs(scrollView.verticalNormalizedPosition - target) > 0.001f)
            {
                progress = Mathf.Min(progress + acceleration, 1);
                scrollView.verticalNormalizedPosition = Mathf.Lerp(scrollView.verticalNormalizedPosition, target, progress);
                //Debug.Log(Mathf.Abs(scrollView.horizontalNormalizedPosition - target));
                yield return new WaitForSeconds(0.02f);
            }

        }

        if (currentIndex != index && onSelectedNewItemDelegate!=null) {
            onSelectedNewItemDelegate(index);
            
        }
        currentIndex = index;

        yield return null;
    }



    float convertValueToPosition(float lengthView , float lengthTotal , float targetPosition) {

        float lengthVelocity = lengthTotal - lengthView;

        float unconvertValue = targetPosition - (lengthView / 2.0f);

        return (unconvertValue / lengthVelocity);

    }
    float lastValue = 0;
    public void valueChange(Vector2 velocity) {
        //Debug.Log(velocity.x);
        if (isHorizontal)
        {
            lastValue = velocity.x;
        }
        else {
            lastValue = velocity.y;
        }

        if (tapped) {
            float speed = scrollView.velocity.magnitude;
            float minSpeed = Screen.height * clampFreeDeaccelerationRate;
            
            if (speed < minSpeed) {
                int near = searchNearestItem();
                setPosition(near);
                tapped = false;
            }
        }

        if (scaleMidle) {
            updateScale();
        }
        if (scaleNotMidle)
        {
            updateScaleMin();
        }

    }

    public void updateScale()
    {
        Transform content = null;
        if (isHorizontal)
        {
            content = contentH.transform;
        }
        else {
            content = contentV.transform;
        }

        foreach (Transform t in content) {
            float dist = Vector2.Distance(transform.position, t.position);

            float lerpValue = Mathf.InverseLerp(scaleDist, 0, Mathf.Min(dist, scaleDist));
            t.localScale = Vector3.one * (1 + Mathf.Lerp(0, scaleFactor, lerpValue));

        }
        
    }
    public void updateScaleMin()
    {
        Transform content = null;
        if (isHorizontal)
        {
            content = contentH.transform;
        }
        else
        {
            content = contentV.transform;
        }

        foreach (Transform t in content)
        {
            float dist = Vector2.Distance(transform.position, t.position);

            float lerpValue = Mathf.InverseLerp(scaleDist, 0, Mathf.Min(dist, scaleDist));
            t.localScale = Vector3.one * (1 - Mathf.Lerp(scaleFactorMin , 0, lerpValue));

        }

    }

    public float offsetItem = 1f;

    int searchNearestItem() {

        /*
        float lengthVelocity = lengthTotal - lengthView;

        float unconvertValue = offsetItem * ((lastValue * lengthVelocity)+(lengthPerItem));

        int totalItem = Mathf.RoundToInt((lengthTotal - lengthView) / lengthPerItem);

        float minimalDist = -1;
        int targetITem = -1;
        Debug.Log(lengthTotal + " - " + lengthView + " - " + lengthPerItem + " - ");

        
        for (int i = 0; i < totalItem ; i ++) {
            float pos = getTargetItemPosition(i);
            float tempDist = Mathf.Abs(pos - unconvertValue);
            Debug.Log(tempDist + " - " + i + " - "+ unconvertValue+" - "+pos);
            if (tempDist < minimalDist || targetITem == -1) {
                targetITem = i;
                minimalDist = tempDist;
            }
        }
        */
        int targetITem = -1;
        float mindist = -1;

        Transform content = null;
        if (isHorizontal)
        {
            content = contentH.transform;
        }
        else
        {
            content = contentV.transform;
        }
        
        for(int i= 0; i < content.childCount; i++)
        {
            float dist = Vector2.Distance(transform.position, content.GetChild(i).position);

            Debug.Log(dist + " - " + i);
            if (dist < mindist || targetITem == -1) {
                targetITem = i;
                mindist = dist;
                Debug.Log("Result : "+i);
            }

        }

        if (reversed)
        {
            targetITem = (content.childCount-1) - targetITem;
        }

        return targetITem;
    }
    
    bool tapped = false;
    public void OnPointerUp(PointerEventData eventData)
   {


        //Debug.Log("Tapped Up - "+near);
        if (!clampFree)
        {
            int near = searchNearestItem();
            setPosition(near);
        }
        else {
            tapped = true;
        }
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Tapped");
        tapped = false;
    }
}
