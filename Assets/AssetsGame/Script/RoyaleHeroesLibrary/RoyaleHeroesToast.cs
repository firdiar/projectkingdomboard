﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoyaleHeroesToast : MonoBehaviour {

    static RoyaleHeroesToast _toast;
    public static RoyaleHeroesToast toast {
        get {
            if (_toast == null)
            {
                GameObject prefab = Resources.Load<GameObject>("General/CanvasEkstra");

                //_toast 
                GameObject temp = Instantiate(prefab);
                _toast = temp.GetComponent<RoyaleHeroesToast>();

                RoyaleHeroesLoading.loading = temp.GetComponent<RoyaleHeroesLoading>();

                DontDestroyOnLoad(_toast.gameObject);
                return _toast;
            }
            else {
                return _toast;
            }
        }
        set {
            _toast = value;
            //DontDestroyOnLoad(value.gameObject);
        }
    }



    public Text txt;
    public Image holder;
    public Image borderLeft;
    public Image borderRight;
    

    Coroutine activeCoroutine;

    public void Start()
    {
        //toast = this;
    }

    public static void ShowToast(string text, float duration) {
        
        RoyaleHeroesToast.toast.showToast(text, duration);
    }

    public void showToast(string text, float duration)
    {
        //holder.gameObject.SetActive(true);
        if (activeCoroutine != null) {
            StopCoroutine(activeCoroutine);
        }
        activeCoroutine = StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text,float duration)
    {
        Color orginalColor = txt.color;

        txt.text = text;
        txt.enabled = true;

        //Fade in
        yield return fadeInAndOut( true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut(false, 0.5f);

        txt.enabled = false;
        txt.color = orginalColor;
        //holder.gameObject.SetActive(false);
    }

    IEnumerator fadeInAndOut( bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, alpha);
            holder.color = new Color(holder.color.r, holder.color.g, holder.color.b, alpha);
            borderLeft.color = new Color(borderLeft.color.r, borderLeft.color.g, borderLeft.color.b, alpha);
            borderRight.color = new Color(borderRight.color.r, borderRight.color.g, borderRight.color.b, alpha);
            yield return null;
        }
    }

    internal static void ShowToast(string v)
    {
        throw new NotImplementedException();
    }
}
