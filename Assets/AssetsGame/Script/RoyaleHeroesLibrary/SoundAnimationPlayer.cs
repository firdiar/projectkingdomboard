﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class SoundAnimationPlayer : MonoBehaviour
{
    public AudioClip[] audioList;


    public UnityEvent OnEvent;

    public void playAudio(int index) {
        AudioSource aus = GetComponent<AudioSource>();
        aus.clip = audioList[index];
        aus.Play();
    }
    public void setBooleanAnimatorTrue(string name) {
        GetComponent<Animator>().SetBool(name, true);
    }
    public void setBooleanAnimatorFalse(string name)
    {
        GetComponent<Animator>().SetBool(name, false);
    }

    public void runEventOnFinish() {
        OnEvent.Invoke();
    }
}
