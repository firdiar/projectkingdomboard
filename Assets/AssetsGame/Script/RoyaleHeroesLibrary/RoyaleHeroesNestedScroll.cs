﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoyaleHeroesNestedScroll : ScrollRect
{
    public ScrollRect mOutterScroll;

    bool draggingSide = false;
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        base.OnInitializePotentialDrag(eventData);
        mOutterScroll.OnInitializePotentialDrag(eventData);
    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.y) > Mathf.Abs(eventData.delta.x))
        {
            Vector2 newdelta = new Vector2(0, eventData.delta.y);
            eventData.delta = newdelta;
            mOutterScroll.OnBeginDrag(eventData);
            draggingSide = true;
        }
        else base.OnBeginDrag(eventData);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (draggingSide)
        {
            Vector2 newdelta = new Vector2(0, eventData.delta.y);
            eventData.delta = newdelta;
            mOutterScroll.OnDrag(eventData);
        }
        else base.OnDrag(eventData);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (draggingSide)
        {
            mOutterScroll.OnEndDrag(eventData);
            draggingSide = false;
        }
        else base.OnEndDrag(eventData);
    }
    
}
