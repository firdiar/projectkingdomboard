﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoyaleHeroesLoading : MonoBehaviour
{

    static RoyaleHeroesLoading _loading;
    public static RoyaleHeroesLoading loading
    {
        get
        {
            if (_loading == null)
            {
                GameObject prefab = Resources.Load<GameObject>("General/CanvasEkstra");

                GameObject temp = Instantiate(prefab);
                _loading = temp.GetComponent<RoyaleHeroesLoading>();

                RoyaleHeroesToast.toast = temp.GetComponent<RoyaleHeroesToast>();
                
                DontDestroyOnLoad(_loading.gameObject);
                return _loading;
            }
            else
            {
                return _loading;
            }
        }
        set
        {
            _loading = value;
            //DontDestroyOnLoad(_loading.gameObject);
        }
    }

    [System.Serializable]
    public class ContentLoading
    {
        public string title;
        [TextArea]
        public string content;
    }

    public Text title = null;
    public Text content = null;
    public Text loadingText = null;
    public Image background = null;
    public Animation anim = null;
    public List<ContentLoading> _contents = new List<ContentLoading>();

    UnityEngine.Events.UnityAction responBack = null;

    public static void openLayerLoading(UnityEngine.Events.UnityAction responBack = null) {

        loading.randomContent();
        loading.anim.gameObject.SetActive(true);
        loading.anim.Play("OpenLoading");
        
        if (responBack != null) {
            loading.InvokeResponBack(responBack, 0.7f);
        }

    }

    public static void hideLayerLoading(UnityEngine.Events.UnityAction responBack = null)
    {
        loading.anim.Play("HideLoading");
        
        loading.setInactiveLoadingObject();

        if (responBack != null)
        {
            loading.InvokeResponBack(responBack, 0.7f);
        }

    }

    public void randomContent() {
        ContentLoading temp = _contents[Random.RandomRange(0, _contents.Count)];

        title.text = temp.title;
        content.text = temp.content;
    }

    public void InvokeResponBack(UnityEngine.Events.UnityAction responBack , float time) {
        this.responBack = responBack;
        Invoke("runResponBack", time);
    }

    void runResponBack() {

        if (responBack != null)
        {
            responBack.Invoke();
            responBack = null;
        }
    }

    public void setInactiveLoadingObject() {
        Invoke("setInactive" , 0.7f);
    }

    void setInactive() {
        anim.gameObject.SetActive(false);
    }
    public void Start()
    {
        //loading = this;
        InvokeRepeating("LoadingTextUpdate" , 0, 0.5f);
    }

    int count = 0;
    void LoadingTextUpdate() {
        count++;
        count = count % 3;

        switch (count) {
            case 0:
                loadingText.text = "Loading .";
                break;
            case 1:
                loadingText.text = "Loading . .";
                break;
            case 2:
                loadingText.text = "Loading . . .";
                break;
        }
    }


}
