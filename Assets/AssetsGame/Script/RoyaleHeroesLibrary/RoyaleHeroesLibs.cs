﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RoyaleHeroesLibs {

    /// <summary>
    /// Convert int to String and cut it (ex : 1000 > 1.0K , 1000000 > 1.0M , etc)
    /// </summary>
    /// <param name="num">number to convert</param>
    /// <returns></returns>
    public static string intToStringK(int num)
    {
        if (num >= 100000000)
            return (num / 1000000).ToString("#,0M");

        if (num >= 10000000)
            return (num / 1000000).ToString("0.#") + "M";

        if (num >= 100000)
            return (num / 1000).ToString("#,0K");

        if (num >= 10000)
            return (num / 1000).ToString("0.#") + "K";

        return num.ToString("#,0");
    }
    public static string intToStringK(long num)
    {

        if (num >= 100000000000)
            return (num / 10000000000).ToString("#,0B");

        if (num >= 10000000000)
            return (num / 1000000000).ToString("0.#") + "B";

        if (num >= 100000000)
            return (num / 1000000).ToString("#,0M");

        if (num >= 10000000)
            return (num / 1000000).ToString("0.#") + "M";

        if (num >= 100000)
            return (num / 1000).ToString("#,0K");

        if (num >= 10000)
            return (num / 1000).ToString("0.#") + "K";

        return num.ToString("#,0");
    }

    public static void SaveAccountData()
    {
        
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(Account.CurrentAccountData);
        Debug.Log(json);
        DatabaseReference reff = FirebaseDatabase.DefaultInstance.GetReference("/account/" + Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId);
        reff.SetRawJsonValueAsync(json).ContinueWith(task => {
            //Debug.Log(task.IsCompleted && !task.IsFaulted);
            if (task.IsFaulted)
            {
                Debug.Log("Data fault");
            }
            else if (task.IsCompleted)
            {
                Debug.Log("Data saved");
            }


        });
    }


    public static string RandomString(int length)
    {
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        string stringChars = "";
        var random = new Random();

        for (int i = 0; i < length; i++)
        {
            stringChars += chars[Random.Range(0, chars.Length)];
        }

        return stringChars;
    }

    public static HeroesDataScriptableObject getHeroesData(string Name)
    {
        return Resources.Load<HeroesDataScriptableObject>("HeroesData/" + Name);
    }
    public static BuffStatusScriptableObject getBuffData(string Name)
    {
        return Resources.Load<BuffStatusScriptableObject>("BuffData/" + Name);
    }

    public static Sprite getLevelBorder(int level) {

        switch (level) {
            case 1:
                return Resources.Load<Sprite>("GlobalImage/CommonBorder");
                
            case 2:
                return Resources.Load<Sprite>("GlobalImage/AdvanceBorder");
                
            case 3:
                return Resources.Load<Sprite>("GlobalImage/RareBorder");
                 
            case 4:
                return Resources.Load<Sprite>("GlobalImage/EpicBorder");
                 
            case 5:
                return Resources.Load<Sprite>("GlobalImage/LegendBorder");
                

        }

        return null;
    }

    public static ExperienceData getExperienceData(string nameFile) {
        return Resources.Load<ExperienceData>("Experience/"+nameFile);
    }
    public static int mod(int k, int n) { return ((k %= n) < 0) ? k + n : k; }
    public static string getGroupName(int countUnit) {

        if (countUnit <= 100)
        {
            return "Unit";
        }
        else if (countUnit <= 500) {
            return "Fireteam";
        }
        else if (countUnit <= 1000)
        {
            return "Squad";
        }
        else if (countUnit <= 5000)
        {
            return "Platoon";
        }
        else if (countUnit <= 10000)
        {
            return "Squadron";
        }
        else if (countUnit <= 15000)
        {
            return "Battalion";
        }
        else if (countUnit <= 22500)
        {
            return "Regiment";
        }
        else if (countUnit <= 30000)
        {
            return "Division";
        }
        else if (countUnit <= 40000)
        {
            return "Corps";
        }
        else
        {
            return "Field Army";
        }
    }

    public static string AddDotToStringNumber(string number)
    {
        int loop = (number.Length - 1) / 3;
        int offset = (3 - number.Length % 3) % 3;
        for (int i = 0; i < loop; i++)
        {
            int index = loop - i;
            number = number.Insert((index * 3) - offset, ".");
        }
        return number;
    }

    public static string loadFileFromPath(string path) {
        if (File.Exists(path)) {
            return File.ReadAllText(path);
        }
        return null;
    }

    public static string loadFileFromName(string name)
    {
        string path = Path.Combine(Application.persistentDataPath, name);
        return loadFileFromPath(path);
    }

    public static void saveFileToName(string name, string value)
    {
        string path = Path.Combine(Application.persistentDataPath, name);
        saveFileToPath(path, value);
    }
    public static void saveFileToPath(string path, string value)
    {
        File.WriteAllText(path, value);
    }

    public class Wrapper<T> where T : struct
    {
        public static implicit operator T(Wrapper<T> w)
        {
            return w.Value;
        }

        public Wrapper(T t)
        {
            _t = t;
        }

        public T Value
        {
            get
            {
                return _t;
            }

            set
            {
                _t = value;
            }
        }

        public override string ToString()
        {
            return _t.ToString();
        }

        private T _t;
    }

    public static System.Globalization.CultureInfo getCultureInfo() {
        return new System.Globalization.CultureInfo("en-US", true);
    }
}
