﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoutSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(PlayerPrefs.GetString("userToken"));
        RoyaleHeroesLoading.hideLayerLoading();
        Photon.Pun.PhotonNetwork.Disconnect();
        Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();
    }

    public void moveToLoginScreen() {
        RoyaleHeroesLoading.openLayerLoading(() => {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LoginScene");
        });
    }
}
