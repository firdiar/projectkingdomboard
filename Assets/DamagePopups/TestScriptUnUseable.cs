﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScriptUnUseable : MonoBehaviour
{
    [SerializeField] DamagePopup prefabs;
    [SerializeField] Transform spawnObject;
    public void spawn() {
        DamagePopup.Create(prefabs, spawnObject.position, "599", Color.green , new Vector2(Random.RandomRange(-1f , 1f) , 1)*15 , false);
    }
}
